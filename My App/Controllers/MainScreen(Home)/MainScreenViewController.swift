//
//  MainScreenViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/17/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import ImageSlideshow
import SwiftyJSON
import CoreData

class MainScreenViewController: UIViewController {
    
    /*
        This Controller have Main Table View
        this table contain 4 cells
        1 - Slider Cell index 0
        2 - Icons Cell index 1
        3 - Posts Cell index 2
        4 - Products cell index 3
     all delegates and data sourse Handeled in "ExtensionMainScreenViewController"
     */
    
    var settings: Settings!
    var iconList = [Icons]()
    var sliderList = [Icons]()
    var ItemList = [Items]()
    var homeModelResponse: HomeModelResponse!
    
    @IBOutlet weak var MaintableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settings = MyAppSettings.data!.settings![0]
        let nibName = UINib(nibName: "SliderMainScreenTableViewCell", bundle: nil)
        MaintableView.register(nibName, forCellReuseIdentifier: "sliderCell")
        getHomeJson()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: settings.menu_name!)
        CheckIfRequiredLoginInAppOrNop()
        self.MaintableView.reloadData()
    }
    
    //MARK:- Helper Methods
    /* This Method ask if this application has required login or optional
     if rquired we present login screen
     if optional we leave user can browes application but he must login in cart when end purchase
     */
    func CheckIfRequiredLoginInAppOrNop() {
        if !MyAppHandller.shared.appLogin() {
            if settings.member_app == "1" {
                let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
                vc.modalPresentationStyle = .overFullScreen
                self.navigationController?.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    //MARK:- requests online
    func getHomeJson() {
        let param = ["method":"main_page" ,
                     "language":"\(MyAppHandller.shared.getMyAppCurruntLang())" ,
                     "post_active": "\(settings.post_active!)" ,
                     "limit":"\(settings.item_num_app!)"]
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    print(JSON(response.value!))
                    self.homeModelResponse = try JSONDecoder().decode(HomeModelResponse.self, from: response.data!)
                    self.homeModelResponse.data!.icons!.forEach({ (object) in
                        if object.kind == "0" {
                            self.sliderList.append(object)
                        } else if object.kind == "1" {
                            self.iconList.append(object)
                        }
                        self.ItemList = self.homeModelResponse.data!.items!
                    })
                    self.MaintableView.reloadData()
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
        }
    }
}
 // TableView Delegate and datasourse in ExtensionMainScreenViewController
// collectionView Delegate and datasourse in ExtensionMainScreenViewController
