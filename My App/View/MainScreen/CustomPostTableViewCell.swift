//
//  CustomPostTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/20/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import CoreData

class CustomPostTableViewCell: UITableViewCell {

    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var PostTitle: UILabel!
    @IBOutlet weak var postDate: UILabel!
    @IBOutlet weak var postLike: UIButton!
    @IBOutlet weak var descriptionInPanorama: UILabel!
    var PostID: String = "-1"
    
    let fetchRequest  : NSFetchRequest<FavEntityMo> = FavEntityMo.fetchRequest()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print("#Cell ==> CustomPostTableViewCell")
    }

    @IBAction func postLike(_ sender: Any) {
        
        let predicateID = NSPredicate(format: "id == %@", PostID)
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicateID])
        print(predicateCompound)
        fetchRequest.predicate = predicateCompound
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequest)
            if test.count > 0
            {
                PresistanceServce.context.delete(test[0])
                do {
                    try PresistanceServce.context.save() // <- remember to put this :)
                    self.postLike.tintColor = UIColor(hexString: "#CCCCCC")
                    
                } catch {
                    // Do something... fatalerror
                }
                
            } else {
                let subject = FavEntityMo(context: PresistanceServce.context)
                subject.id = PostID
                subject.idForFav = true
                subject.cartQ = 1
                PresistanceServce.saveContext()
                self.postLike.tintColor = UIColor(hexString: SharedData.AppColor)
                
            }
        }
        catch
        {
            print(error)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
