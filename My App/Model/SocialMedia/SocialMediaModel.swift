

import Foundation
struct SocialMediaModel : Codable {
	let state : String?
	let data : [SocialData]?

	enum CodingKeys: String, CodingKey {

		case state = "state"
		case data = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		state = try values.decodeIfPresent(String.self, forKey: .state)
		data = try values.decodeIfPresent([SocialData].self, forKey: .data)
	}

}
