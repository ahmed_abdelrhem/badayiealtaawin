//
//  AppDelegate.swift
//  My App
//
//  Created by Kirollos Maged on 11/5/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import CoreData
import MOLH
import IQKeyboardManagerSwift
import GoogleMaps
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import Firebase
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    let chatFetchRequest  : NSFetchRequest<ChatEntityMo> = ChatEntityMo.fetchRequest()
   
    
    override init() {
        super.init()
        UIFont.overrideInitialize()
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey(URLHandller.MapKey)
        MOLH.shared.activate(true)
        IQKeyboardManager.shared.enable = true
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self

        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshToken(notification:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        return true
    }
    
    func fbHandler(){
//        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    @objc func refreshToken(notification: NSNotification){
        fbHandler()
    }
    func applicationWillTerminate(_ application: UIApplication) {
        PresistanceServce.saveContext()
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
//        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        fbHandler()
    }
    
   
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        let data = JSON(userInfo)
        print("data didReceiveRemoteNotification: \(data)")
        //        if let notificationID = data["gcm.notification.notification_type"].string {
        //            if let target = data["gcm.notification.target_id"].string {
        //                if notificationID == "1"{
        //                    SharedHandller.setValue(target, forKey: "TARGET_ID")
        //                    NotificationCenter.default.post(name: TO_IMAGE, object: nil)
        //
        //                }else if notificationID == "3"{
        //                    SharedHandller.setValue(target, forKey: "TARGET_ID")
        //                    NotificationCenter.default.post(name: TO_VIDEO, object: nil)
        //                }else if notificationID == "4"{
        //                    SharedHandller.setValue(target, forKey: "TARGET_ID")
        //                    NotificationCenter.default.post(name: TO_BOOK, object: nil)
        //                }else if notificationID == "5"{
        //                    SharedHandller.setValue(target, forKey: "TARGET_ID")
        //                    NotificationCenter.default.post(name: TO_ARTICLE, object: nil)
        //                }
        
        //            }
        //        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//            print("Unable to register for remote notifications: \(error.localizedDescription)") 
//           print("APNs registration failed: \(error)")
       }
    
    
    
      // Push notification received
      func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
          print("DidReceiveRemoteNotification")
          
      }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        print("APNs device token: \(deviceTokenString)")
        
        
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("FCM token: \(result.token)")
              let refreshedToken = result.token
             UserDefaults.standard.setValue(refreshedToken, forKey: DEVICE_TOKEN)
             UserDefaults.standard.synchronize()

            }
        }

        
        
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "My_App")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}



//MARK: forgorund
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // when app is opend
        
        let userInfo = notification.request.content.userInfo as! [String : Any]
        let json = JSON(userInfo)
        print(json)
        if let NT = json["notification_type"].string {
            if NT == "2" {
                guard let voice = json["voice"].string else { return }
                guard let image = json["image"].string else { return }
                guard let location = json["location"].string else { return }
                guard let receiver_id = json["receiver_id"].string else { return }
                guard let sender_id = json["sender_id"].string else { return }
                guard let message = json["message"].string else { return }
                saveMessage(reciver_id: receiver_id, sender_id: sender_id,
                            message_id: lastMessageID() + 1,
                            notification_type: NT,
                            message: message,
                            location: location,
                            voice: voice, image: image, time: getTime())
                NotificationCenter.default.post(name: MESSAGE_NOTIFY, object: nil)
                
            } else {
                completionHandler([.alert, .badge, .sound])
            }
        }
       
        
    }
    
    //MARK: ON Click Local Notification Forground
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        // didselect Notifications
        let userInfo = response.notification.request.content.userInfo
        
        print(userInfo)
        
        let json = JSON(userInfo)
        
        if let NT = json["notification_type"].string {
            let state = UIApplication.shared.applicationState
            if state == .background || state == .active {
                print("App in Background")
                if NT == "2" {
                    guard let voice = json["voice"].string else { return }
                    
                    guard let image = json["image"].string else { return }
                    guard let location = json["location"].string else { return }
                    guard let receiver_id = json["receiver_id"].string else { return }
                    guard let sender_id = json["sender_id"].string else { return }
                    guard let message = json["message"].string else { return }
                    saveMessage(reciver_id: receiver_id,
                                sender_id: sender_id,
                                message_id: lastMessageID() + 1,
                                notification_type: NT, message: message, location: location, voice: voice, image: image, time: getTime())
                    NotificationCenter.default.post(name: MESSAGE_NOTIFY, object: nil)
                    
                }
            } else {
                if NT == "2" {
                    guard let voice = json["voice"].string else { return }
                    
                    guard let image = json["image"].string else { return }
                    guard let location = json["location"].string else { return }
                    guard let receiver_id = json["receiver_id"].string else { return }
                    guard let sender_id = json["sender_id"].string else { return }
                    guard let message = json["message"].string else { return }
                    saveMessage(reciver_id: receiver_id, sender_id: sender_id,
                                message_id: lastMessageID() + 1,
                                notification_type: NT,
                                message: message,
                                location: location,
                                voice: voice,
                                image: image,
                                time: getTime())
                    NotificationCenter.default.post(name: MESSAGE_NOTIFY, object: nil)
                    
                }
            }
           
        }                                                                                                                                                  
        
        completionHandler()
    }
}

//MARK:backgorund
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.setValue(fcmToken, forKey: DEVICE_TOKEN)
        UserDefaults.standard.synchronize()
        
    }
    
    
  
    
    func saveMessage(reciver_id: String,sender_id:String,message_id: Int64,notification_type: String,message: String,location:String,voice:String,image:String,time: String) {
        let subject = ChatEntityMo(context: PresistanceServce.context)
        print("New MEssage ID: ", message_id)
        subject.reciver_id = reciver_id
        subject.sender_id = sender_id
        subject.message_id = message_id
        subject.notification_type = notification_type
        subject.message = message
        subject.location = location
        subject.voice = voice
        subject.image = image
        subject.time = time
        subject.messageSent = 0
        PresistanceServce.saveContext()
        
    }
    
    func getTime() -> String {
        let timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .medium, timeStyle: .short)
        print(timestamp)
        return timestamp
    }
    
    
    func lastMessageID() -> Int64 {
        do
        {
            let test = try PresistanceServce.context.fetch(chatFetchRequest)
            if test.count > 0
            {
                print((test.last?.message_id)!)
                return (test.last?.message_id)!
            }
        }
        catch {
            print(error)
            return 0
        }
        return 0
    }
    
}


extension UIApplication {
    var statusBarView : UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
