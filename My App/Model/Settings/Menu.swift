
//  My App
//
//  Created by Kirollos Maged on 12/6/18.
//  Copyright © 2018 apple. All rights reserved.
//
import Foundation
struct Menu : Codable {
    let id : String?
    let name : String?
    let type : String?
    let type_id : String?
    let icon : String?
    let style_type : String?
    let menu_id : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case type = "type"
        case type_id = "type_id"
        case icon = "icon"
        case style_type = "style_type"
        case menu_id = "menu_id"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        type_id = try values.decodeIfPresent(String.self, forKey: .type_id)
        icon = try values.decodeIfPresent(String.self, forKey: .icon)
        style_type = try values.decodeIfPresent(String.self, forKey: .style_type)
        menu_id = try values.decodeIfPresent(String.self, forKey: .menu_id)
    }

}
