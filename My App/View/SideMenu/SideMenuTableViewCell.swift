//
//  SideMenuTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/11/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Kingfisher
import ExpyTableView

class SideMenuTableViewCell: UITableViewCell ,ExpyTableViewHeaderCell{

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellText: UILabel!
    @IBOutlet weak var arrowBTN: UIButton!
    @IBOutlet weak var normalSeparator: UIView!
    
    var MyAppModel = MyAppHandller.shared.getMyAppSettings()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCellView()
    }
    
    func setImage(url : String, offlineUrl: Bool) {
        guard let setting = MyAppModel.data?.settings?[0] else { return }
        if !offlineUrl {
            DispatchQueue.global().async {
                DispatchQueue.main.async {
                    self.cellImage.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "myappIcon"), options: nil, progressBlock: nil) { (image, error, cashe, url) in
                        if error != nil {
                            self.cellImage.image = UIImage(named: "myappIcon")
                        }
                    }
                }
            }
        } else {
            self.cellImage.image = UIImage(named: url)
            self.cellImage.tintColor = UIColor(hexString:  "#" + setting.menu_text_color!)
        }
       
    }

    func setupCellView() {
        guard let settings = MyAppModel.data?.settings?[0] else { return }
        if let arrowC = settings.arrow_color {
            arrowBTN.tintColor = UIColor(hexString: "#" + arrowC)
        }
        if let menuTextColor = settings.menu_text_color {
            cellText.textColor = UIColor(hexString: "#" + menuTextColor)
        }
        if AppDelegate.Static.isThereSparatorInSideMenu {
            normalSeparator.isHidden = false
            normalSeparator.backgroundColor = UIColor(hexString: "#\(settings.menu_line_color!)")
        }
    }
    
    func haveArrow(MainCell:Bool) {
        arrowBTN.isHidden = !MainCell
    }
    
//    func rotateArrow() {
//        let image = arrowBTN.currentBackgroundImage
//         let newArrow = ImageTools.imageRotatedByDegrees(oldImage: image!, deg: 180.0)
//        
//        arrowBTN.setBackgroundImage(newArrow, for: .normal)
//    }

    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            
        case .willCollapse:
            print("WILL COLLAPSE")
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
    
}

class SideMenuTableViewCell2: UITableViewCell ,ExpyTableViewHeaderCell{
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellText: UILabel!
    @IBOutlet weak var arrowBTN: UIButton!
    
    var MyAppModel = MyAppHandller.shared.getMyAppSettings()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCellView()
    }
    
    func setImage(url : String, offlineUrl: Bool) {
        guard let setting = MyAppModel.data?.settings?[0] else { return }
        if !offlineUrl {
            DispatchQueue.global().async {
                DispatchQueue.main.async {
                    self.cellImage.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "myappIcon"), options: nil, progressBlock: nil) { (image, error, cashe, url) in
                        if error != nil {
                            self.cellImage.image = UIImage(named: "myappIcon")
                        }
                    }
                }
            }
        } else {
            self.cellImage.image = UIImage(named: url)
            self.cellImage.tintColor = UIColor(hexString:  "#" + setting.menu_text_color!)
        }
        
    }
    
    func setupCellView() {
        guard let settings = MyAppModel.data?.settings?[0] else { return }
        if let arrowC = settings.arrow_color {
            arrowBTN.tintColor = UIColor(hexString: "#" + arrowC)
        }
        if let menuTextColor = settings.menu_text_color {
            cellText.textColor = UIColor(hexString: "#" + menuTextColor)
        }
    }
    
    func haveArrow(MainCell:Bool) {
        arrowBTN.isHidden = !MainCell
    }
    
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            
        case .willCollapse:
            print("WILL COLLAPSE")
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
    
}



extension UITableViewCell {
    
    func showSeparator() {
        DispatchQueue.main.async {
            self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func hideSeparator() {
        DispatchQueue.main.async {
            self.separatorInset = UIEdgeInsets(top: 0, left: self.bounds.size.width, bottom: 0, right: 0)
        }
    }
    
}
