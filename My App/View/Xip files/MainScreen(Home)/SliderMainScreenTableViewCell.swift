//
//  SliderMainScreenTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/17/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import ImageSlideshow

class SliderMainScreenTableViewCell: UITableViewCell {

    @IBOutlet weak var sliderShow: ImageSlideshow!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        sliderShow.contentScaleMode = .scaleToFill
        sliderShow.slideshowInterval = 2.5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
