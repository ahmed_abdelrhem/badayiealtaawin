
import Foundation
struct HomeData : Codable {
	let icons : [Icons]?
	let items : [Items]?

	enum CodingKeys: String, CodingKey {

		case icons = "icons"
		case items = "items"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		icons = try values.decodeIfPresent([Icons].self, forKey: .icons)
		items = try values.decodeIfPresent([Items].self, forKey: .items)
	}

}
