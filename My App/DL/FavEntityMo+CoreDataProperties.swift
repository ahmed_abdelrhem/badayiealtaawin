//
//  FavEntityMo+CoreDataProperties.swift
//  
//
//  Created by Kirollos Maged on 1/21/19.
//
//

import Foundation
import CoreData


extension FavEntityMo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FavEntityMo> {
        return NSFetchRequest<FavEntityMo>(entityName: "FavEntity")
    }

    @NSManaged public var id: String?
    @NSManaged public var idForFav: Bool
    @NSManaged public var cartQ: Int16

}
