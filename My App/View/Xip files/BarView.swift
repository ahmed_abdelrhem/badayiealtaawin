////
////  BarView.swift
////  My App
////
////  Created by Kirollos Maged on 12/5/18.
////  Copyright © 2018 apple. All rights reserved.
////
//
//import UIKit
//import MOLH
//
//class BarView: UIView {
//    
//    @IBOutlet weak var btnCart: UIButton!
//    @IBOutlet weak var btnSearch: UIButton!
//    @IBOutlet weak var btnNotif: UIButton!
//    @IBOutlet weak var btnLanguage: UIButton!
//    @IBOutlet weak var btnFavourit: UIButton!
//    @IBOutlet weak var btnChat: UIButton!
//    @IBOutlet weak var bskView: UIView!
//    @IBOutlet weak var btnMenu: UIButton!
//    @IBOutlet weak var labelHeader: UILabel!
//    @IBOutlet weak var labelHeaderBasket: UILabel!
//    
//    var backBtn = false
//    var view:UIView!
//    var index = 0
//    var vc:UIViewController!
//    
//    var appColor:UIColor {
//        get{
//            return Colours.hexStringToUIColor((AppDelegate.Static.appColor))
//        }
//    }
//    
//    var itemsInBasket:String?{
//        get{
//            return labelHeaderBasket.text!
//        }
//        set{
//            labelHeaderBasket.text = newValue
//        }
//    }
//    var headerName:String{
//        get{
//            return ""
//        }
//        set{
//            labelHeader.text = newValue
//            
//        }
//    }
//    
//    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setup()
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setup()
//    }
//    
//    func setup() {
//        view = loadViewFromNib()
//        view.backgroundColor = appColor
//        view.clipsToBounds = true
//        bskView.circularView()
//        btnMenu.tintColorToButton(UIColor.white)
//        btnCart.isHidden = !AppDelegate.Static.isTherecart
//        bskView.isHidden = !AppDelegate.Static.isTherecart
//        btnLanguage.isHidden = !AppDelegate.Static.isThereLangues
//        btnNotif.isHidden = !AppDelegate.Static.isThereNotification
//        btnSearch.isHidden = !AppDelegate.Static.isThereSearch
//        btnFavourit.isHidden = !AppDelegate.Static.isThereFavorate
//        
//        
//        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]; addSubview(view)
//    }
//    
//    func loadViewFromNib() -> UIView {
//        let bundle = Bundle(for:type(of: self))
//        let nib = UINib(nibName: "BarView", bundle: bundle)
//        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
//        
//        return view
//    }
//    
//    override func willRemoveSubview(_ subview: UIView) {
//        self.removeFromSuperview()
//    }
//    @IBAction func favoritAction(_ sender: AnyObject) {
//        
//    }
//    
//    var firstTime=false;
//    @IBAction func menuAction(_ sender: UIButton) {
////        if AppDelegate.Static.isFromNotif{
////            NavigationTools.dismissView(vc)
////            AppDelegate.Static.isFromNotif = false
////        }else{
////            print("in  menu button")
////            if !backBtn{
////                if AppDelegate.Static.isArabic{
////                    print("menu button pressed")
////
////                    if AppDelegate.Static.isFirstTime{
////                        NavigationTools.popBackRVC(vc)
////                        AppDelegate.Static.isFirstTime = false
////                    }
////                    vc.revealViewController().rightRevealToggle(sender)
////                }else{
////                    vc.revealViewController().revealToggle(sender)
////                }
////                AppDelegate.Static.isBackbtn = false
////            }else{
////                print("in")
////                NavigationTools.popBackRVC(vc)
////            }
////        }
//    }
//    
//    @IBAction func changeLangAction(_ sender: AnyObject) {
//        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
//        MOLH.reset()
//    }
//    
//    @IBAction func gotoCartAction(_ sender: AnyObject) {
////        NavigationTools.pushToNavigateViewController(vc, tovc: PushViewController(), storyBoardId: "PushViewController")
//    }
//    @IBAction func gotoSearchAction(_ sender: AnyObject) {
//        
//    }
//    
//    @IBAction func gotoNotifAction(_ sender: AnyObject) {
//        
//    }
//    
//    @IBAction func gotoChat(_ sender: AnyObject) {
//        
//    }
//}
