//
//  FAQTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 1/1/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class FAQTableViewCell: UITableViewCell {
    
    @IBOutlet weak var QView: UIView!
    @IBOutlet weak var QLable: UILabel!
    @IBOutlet weak var ALable: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
