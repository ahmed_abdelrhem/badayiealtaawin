//
//  SaidData.swift
//  My App
//
//  Created by Kirollos Maged on 1/1/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
struct SaidData : Codable {
    let id : Int?
    let name : String?
    let note : String?
    let logo : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case note = "note"
        case logo = "logo"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        note = try values.decodeIfPresent(String.self, forKey: .note)
        logo = try values.decodeIfPresent(String.self, forKey: .logo)
    }
}

