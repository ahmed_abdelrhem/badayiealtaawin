//
//  FavorateViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/20/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreData

class FavorateViewController: UIViewController {

    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var settings: Settings!
    var itemModelResponse: ItemModelResponse!
    var ItemList = [ItemsModel]()
    var fromSide :Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settings = MyAppSettings.data!.settings![0]
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        print(getIDs())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "", backBtn: !fromSide, LargTitle: false)
        let param = ["method":"favourites",
                     "favourite":"\(getIDs())",
            "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        getAllFavs(param)
    }
    
    func getIDs() -> String {
        let fetchRequest  : NSFetchRequest<FavEntityMo> = FavEntityMo.fetchRequest()
        var predicate : NSPredicate!
        predicate = NSPredicate(format: "idForFav == \(true)")
        fetchRequest.predicate = predicate
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequest)
            if test.count > 0
            {
                print(test.count)
                var stringIDs = ""
                test.forEach { (row) in
                    print(row.id!)
                    stringIDs.append("\(row.id!),")
                }
                return stringIDs
            }
        }
        catch {
            print(error)
            return ""
        }
        return ""
    }
    
    //MARK:- Requests Online
    func getAllFavs(_ param: [String: Any]) {
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.itemModelResponse = try JSONDecoder().decode(ItemModelResponse.self, from: response.data!)
                    self.ItemList = self.itemModelResponse.data!.items!
                    self.tableView.reloadData()
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
        }
    }


    //MARK:- Helper Methods
    //MARK: handle Product table View
    func HandleProductsCell(tableView: UITableView, indexPath: IndexPath) -> CustomProductTableViewCell {
        var ProductCell : CustomProductTableViewCell!
        switch settings.item_style {
        case "1":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! CustomProductTableViewCell)
        case "2":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "galaryCell", for: indexPath) as! CustomProductTableViewCell)
            ProductCell.PriceBackground.backgroundColor = UIColor(hexString: SharedData.AppColor)
            ProductCell.Price.textColor = UIColor.white
            ProductCell.DiscountPrice.textColor = UIColor.white
            ProductCell.DiscountLine.backgroundColor = UIColor.white
        case "3":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "panoramaCell", for: indexPath) as! CustomProductTableViewCell)
            ProductCell.ProductDescription.text = ItemList[indexPath.row].details!
        default:
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "panoramaCell", for: indexPath) as! CustomProductTableViewCell)
            ProductCell.ProductDescription.text = ItemList[indexPath.row].details!
        }
        ImageTools.setImage(url: ItemList[indexPath.row].image!,
                            imageView: ProductCell.ProductImage, placeHolder: "myappIcon")
        if ItemList[indexPath.row].price_after_disc != "0.00" {
            ProductCell.DiscountPrice.text = "\(ItemList[indexPath.row].price_after_disc!) \(settings.currency!)"
            ProductCell.DiscountPriceView.isHidden = false
            ProductCell.DiscountLine.isHidden = false
        } else {
            ProductCell.DiscountPriceView.isHidden = true
            ProductCell.DiscountLine.isHidden = true
        }
        if ItemList[indexPath.row].sold == "1" {
            ProductCell.SoldProduct.isHidden = false
        }
        ProductCell.Price.text = "\(ItemList[indexPath.row].price!) \(settings.currency!)"
        ProductCell.ProuductName.text = ItemList[indexPath.row].name!
        ProductCell.ProductID = ItemList[indexPath.row].id!
        if SharedData.IsFavorate(ID: ItemList[indexPath.row].id!) {
            ProductCell.favBTN.tintColor = UIColor(hexString: SharedData.AppColor)
        } else {
            ProductCell.favBTN.tintColor = UIColor(hexString: "#CCCCCC")
        }
        return ProductCell
    }
    
    //MARK: handle Post table View
    func HandlePostsCell(tableView: UITableView, indexPath: IndexPath) -> CustomPostTableViewCell {
        var Postcell : CustomPostTableViewCell!
        switch settings.post_style {
        case "1":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventnewsCell", for: indexPath) as! CustomPostTableViewCell)
        case "2":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventgalaryCell", for: indexPath) as! CustomPostTableViewCell)
        case "3":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventpanoramaCell", for: indexPath) as! CustomPostTableViewCell)
            Postcell.descriptionInPanorama.text = ItemList[indexPath.row].details!
        default:
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventpanoramaCell", for: indexPath) as! CustomPostTableViewCell)
            Postcell.descriptionInPanorama.text = ItemList[indexPath.row].details!
        }
        ImageTools.setImage(url: ItemList[indexPath.row].image!,
                            imageView: Postcell.postImage, placeHolder: "myappIcon")
        Postcell.PostTitle.text = ItemList[indexPath.row].name!
        Postcell.postDate.text = ItemList[indexPath.row].created!
        Postcell.PostID = ItemList[indexPath.row].id!
        if SharedData.IsFavorate(ID: ItemList[indexPath.row].id!) {
            Postcell.postLike.tintColor = UIColor(hexString: SharedData.AppColor)
        } else {
            Postcell.postLike.tintColor = UIColor(hexString: "#CCCCCC")
        }
        return Postcell
    }
    
}

extension FavorateViewController :UITableViewDelegate , UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ItemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ItemList[indexPath.row].type == "1" {
            let cell = HandleProductsCell(tableView: tableView, indexPath: indexPath)
            return cell
        } else {
            let cell = HandlePostsCell(tableView: tableView, indexPath: indexPath)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ItemList[indexPath.row].type == "1" {
            switch settings.item_style {
            case "1": return 210
            case "2": return 228
            case "3": return 159
            default: return 159
            }
        } else if ItemList[indexPath.row].type == "0" {
            switch settings.post_style {
            case "1": return 200
            case "2": return 228
            case "3": return 144
            default: return 144
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ItemList[indexPath.row].type == "1" {
            tableView.deselectRow(at: indexPath, animated: true)
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
            vc.ProductID = ItemList[indexPath.row].id!
            vc.ProductName = ItemList[indexPath.row].name!
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SinglePostViewController") as! SinglePostViewController
            vc.PostID = ItemList[indexPath.row].id!
            vc.PostName = ItemList[indexPath.row].name!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(
            withDuration: 0.2,
            delay: 0.02 * Double(indexPath.row),
            animations: {
                cell.alpha = 1
        })
    }
}
