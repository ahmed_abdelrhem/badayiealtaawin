//
//  ImageTools.swift
//  My App
//
//  Created by Kirollos Maged on 12/9/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation

import Kingfisher

class ImageTools{
    class func imageRotatedByDegrees(oldImage: UIImage, deg: CGFloat) -> UIImage {
        return rotateImage(oldImage: oldImage,degrees: deg)
    }
    
    class func imageRotatedByDegrees(imgName: String, deg: CGFloat) -> UIImage {
        let oldImage = UIImage(named: imgName)
        return rotateImage(oldImage: oldImage!,degrees: deg)
    }
    
    class fileprivate func rotateImage(oldImage: UIImage, degrees: CGFloat) -> UIImage{
        //Calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox: UIView = UIView(frame: CGRect(x: 0, y: 0, width: oldImage.size.width, height: oldImage.size.height))
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: degrees * CGFloat(Double.pi / 180))
        rotatedViewBox.transform = t
        let rotatedSize: CGSize = rotatedViewBox.frame.size
        //Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat(Double.pi / 180)))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        bitmap.draw(oldImage.cgImage!, in: CGRect(x: -oldImage.size.width / 2, y: -oldImage.size.height / 2, width: oldImage.size.width, height: oldImage.size.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
        
    }
    
    static func downloadImage(url: URL) ->UIImage?{
        var img:UIImage? = nil
        print("Download Started")
        
        getDataFromUrl(url: url) { (data, response, error)  in
            
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() { () -> Void in
                img = UIImage(data: data)
            }
        }
        return img
    }
    
    static func setImage(url : String,imageView: UIImageView,placeHolder: String) {
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                imageView.kf.setImage(with: URL(string: url), placeholder: UIImage(named: placeHolder), options: nil, progressBlock: nil) { (image, error, cashe, url) in
                    if error != nil {
                        imageView.image = UIImage(named: placeHolder)
                    }
                }
            }
        }
    }
    
    fileprivate static func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        print(url)
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
}
extension UIImageView {
    public func imageFromUrl(urlString: String) {
        let url = URL(string: urlString)
        if let urlData = url{
            self.kf.setImage(with: urlData)
        }
        //        self.image
    }
    
    public func addTintColor(color:UIColor){
        
        self.image = self.image!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.tintColor = color
        
    }
    
    

    
    
    
}

extension UIImage {
    var uncompressedPNGData: Data  {return self.pngData()! }
    var highestQualityJPEGNSData: Data {return self.jpegData(compressionQuality: 1.0)!}
    var highQualityJPEGNSData: Data {return self.jpegData(compressionQuality: 0.75)!}
    var mediumQualityJPEGNSData: Data {return self.jpegData(compressionQuality: 0.5)!}
    var lowQualityJPEGNSData: Data  {return self.jpegData(compressionQuality: 0.25)!}
    var lowestQualityJPEGNSData:Data {return self.jpegData(compressionQuality: 0.09)! }
    
    
}


