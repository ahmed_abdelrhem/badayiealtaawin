//
//  IconCollectionViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/18/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class IconCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var iconTitle: UILabel!
    @IBOutlet weak var roundViewStyle4: RoundImage!
    @IBOutlet weak var TitleView: UIView!
}
