//
//  MemberPopUpViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/27/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class MemberPopUpViewController: UIViewController {

    @IBOutlet weak var memberName: UILabel!
    @IBOutlet weak var memberDetails: UILabel!
    @IBOutlet weak var closeBTN: UIButton!
    
    var name = "Name Empty from server"
    var details = "No Details In DataBase"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closeBTN.backgroundColor = UIColor(hexString: SharedData.AppColor)
        memberDetails.text = details
        memberName.text = name

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        showAnimate()
     
    }
    
    @IBAction func closeButton(_ sender: Any) {
        removeAnimation()
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }


}
