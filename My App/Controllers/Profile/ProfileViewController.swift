//
//  ProfileViewController.swift
//  My App
//
//  Created by a7med on 8/23/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import KMPopUp
import SwiftyJSON

class ProfileViewController: UIViewController {

    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var ProfileImageBTN: UIButton!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var mailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var confirmPassText: UITextField!
    
    @IBOutlet weak var signUp: UIButton!
    
    var settings : Settings!
    var RequestWithImage : Bool = false
    var logo_data : UIImage?
    
    var fromSide: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "Profile".localized, backBtn: !fromSide, LargTitle: false)
    }
    
    //MARK:- Helper Methods
    func setupView() {
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        settings = MyAppSettings.data!.settings![0]
        nameText.text = UserDefaults.standard.string(forKey: "DEFname")
        phoneText.text = UserDefaults.standard.string(forKey: "DEFuserphone")
        mailText.text = UserDefaults.standard.string(forKey: "DEFemail")
        print("#pw",UserDefaults.standard.string(forKey: "DEFPWD"))
        passwordText.text = UserDefaults.standard.string(forKey: "DEFPWD")
        confirmPassText.text = UserDefaults.standard.string(forKey: "DEFPWD")
        // as per Mohamed Atef
        if settings.register_mobile_active == "1" {
            mailText.isHidden = true
        } else if settings.register_mobile_active == "0" {
            mailText.isHidden = false
        }
        signUp.backgroundColor = UIColor(hexString: SharedData.AppColor)
    }
    
    func setStrinImage(toNSString image: UIImage) -> String {
        let data: Data? = image.jpegData(compressionQuality: 0.7)
        return data?.base64EncodedString(options: .endLineWithLineFeed) ?? ""
    }
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
    
    @IBAction func ProfileImage(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func updateBTN(_ sender: Any) {
        
        var txtFArray: [UITextField] = [nameText,phoneText,passwordText,confirmPassText]
        if settings.register_mobile_active == "0" {
            txtFArray.append(mailText)
        }
        if MyAppHandller.shared.isValidAction(textFeilds: txtFArray) {
            if UITextField.compare_confirm_pass(testStr: passwordText, passconfirm: confirmPassText) {
                let imageName = "\(randomString(length: 16))"
                var param = ["method":"update_profile" ,
                             "cols":"name,email,password,phone,address",
                             "name":"\(nameText.text!)" ,
                    "phone":"\(phoneText.text!)" ,
                    "email":"\(mailText.text!)" ,
                    "address":"testing for address",
                    "password": passwordText.text ?? "",
                    "id": UserDefaults.standard.integer(forKey: "DEFuserid")

                    
                    ] as [String : Any] // as per Mohamed Atef
                print("##id", UserDefaults.standard.integer(forKey: "DEFuserid"))
        
                
                if logo_data != nil {
                    param["file_data"] = "\(setStrinImage(toNSString: logo_data!))"
                    param["file_name"] = "\(imageName)"
                    param["photo"] = "\(imageName)"

                }
                print(param)
                
                update(param, loginType: APP_LOGIN)
                
            } else {
                KMPopUp.ShowMessage(self, message: PASSWORD_ERROR_MESSAGE, image: "warning", withAlpha: 0.7)
            }
          
        } else {
            KMPopUp.ShowMessage(self, message: COMPLETE_DATA_ERROR, image: "warning", withAlpha: 0.7)
        }
    }
    
    func update(_ param: [String:Any],loginType: String) {
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                let json = JSON(response.value!)
                print(json)
                guard let status = json["state"].string else { return }
                if status == ResponseState.sucess.rawValue {
                    guard let name = self.nameText.text else { return }
                    guard let email = self.mailText.text else { return }
                    guard let phone = self.phoneText.text else { return }
                    guard let password = self.passwordText.text else { return }

                    UserDefaults.standard.setValue(email, forKey: "DEFemail")
                    UserDefaults.standard.set(name, forKey: "DEFname")
                    UserDefaults.standard.set(phone, forKey: "DEFuserphone")
                    UserDefaults.standard.set(password, forKey: "DEFPWD")

                    KMPopUp.ShowMessage(self, message: "Profile updated successfully".localized, image: "like", withAlpha: 0.7)
                    
                }  else if status == ResponseState.failed.rawValue {
                    KMPopUp.ShowMessage(self, message: GENERAL_UNKHOWN_ERROR, image: "warning")
                } else if status == ResponseState.used_mail.rawValue {
                    KMPopUp.ShowMessage(self, message: "this E-mail is already used", image: "warning")
                } else if status == ResponseState.wrong_mail.rawValue {
                    KMPopUp.ShowMessage(self, message: "Phone or e-mail not found", image: "warning")
                }
            } else {
                KMPopUp.ShowMessage(self, message: response.error!.localizedDescription, image: "warning")
            }
            self.indicator.stopAnimating()
        }
    }
        
}

extension ProfileViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        logo_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        ProfileImageBTN.setImage(logo_data, for: .normal)
        RequestWithImage = true
        dismiss(animated: true, completion: nil)
    }
    
}
