//
//  PartenerTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/30/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class PartenerTableViewCell: UITableViewCell {

    @IBOutlet weak var partenerHeader: UIImageView!
    @IBOutlet weak var PartenerDetails: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
