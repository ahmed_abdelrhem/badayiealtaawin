//
//  EndPurchasesPopUpViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/27/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import Gifu
import AVFoundation
import MOLH
import CoreData
import NVActivityIndicatorView

class EndPurchasesPopUpViewController: UIViewController {

   
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var image: GIFImageView!
    @IBOutlet weak var price: UILabel!
    var totalValue: String!
    var myAudio: AVAudioPlayer!
    var param : [String: Any]!
    var settings: Settings!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        showAnimate()
        self.perform(#selector(playSound), with: nil, afterDelay: 0.95)
        // Do any additional setup after loading the view.
        print("#last param ==>>",param)
    }
    
    @objc func playSound() {
        let path = Bundle.main.path(forResource: "cheerful", ofType: "mp3")!
        let url = URL(fileURLWithPath: path)
        do {
            let sound = try AVAudioPlayer(contentsOf: url)
            myAudio = sound
            sound.play()
        } catch {
            //
        }
    }
    
    func setupView() {
        settings = MyAppSettings.data!.settings![0]
        image.animate(withGIFNamed: "checkmark", loopCount: 1)
        image.tintColor = UIColor(hexString: SharedData.AppColor)
        if MOLHLanguage.isArabic() {
            price.text = "السعر : \(totalValue!)"
        } else {
            price.text = "Total : \(totalValue!)"
        }
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        removeAnimation()
    }
    
    @IBAction func Done(_ sender: Any) {
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                print("Done 👍")
                self.perform(#selector(self.createObserver), with: nil, afterDelay: 0.01)
                self.deleteAllRecords()
                self.removeAnimation()
                self.indicator.stopAnimating()
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
        }
    }
    
    func deleteAllRecords() {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CartEntity")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try PresistanceServce.context.execute(deleteRequest)
            PresistanceServce.saveContext()
        } catch {
            print ("There was an error")
        }
    }
    
    @objc func createObserver() {
        NotificationCenter.default.post(name: END_PURCHASE, object: nil)
    }

}
