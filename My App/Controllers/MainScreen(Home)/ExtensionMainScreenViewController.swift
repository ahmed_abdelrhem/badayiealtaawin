//
//  ExtensionMainScreenViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/18/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import Kingfisher
import ImageSlideshow

extension MainScreenViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 0 { // main tableView
            if settings.slider_full == "1" {  // check if application setup main screen as full screen
                print("##slider_full")
                return 1        // only slider cell
            } else {
                return 4        // all cells with slider
            }
        } else {
            return ItemList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        print("#TbTsg",tableView.tag)
        if tableView.tag == 0 {             // main container UItableView
            let cell = HandleMainTableView(tableView: tableView, indexPath: indexPath)
            return cell
        }
        else if tableView.tag == 1 {        // Posts TableView
            let Postcell = HandlePostsCell(tableView: tableView, indexPath: indexPath)
            
            return Postcell
        }
        else if tableView.tag == 2 {        // Products TableView
            let cell = HandleProductsCell(tableView: tableView, indexPath: indexPath)
            return cell
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .zero
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        /*
         1 slider height full or 250
         2 icons height "dynamic height"
         3 products or posts height "dynamic height"
         */
        print("#heightForRowAt tb_tag",tableView.tag )
        if tableView.tag == 0 {
            // handle Slider height one from two cases 1 - full screen height
            //                                         2 - 250 height
            if indexPath.row == 0 {
                if settings.icon_slider == "1" {
                    if settings.slider_full == "1" {
                        print("#X")
                        return MaintableView.frame.size.height
                    }
                    else {
                        return 250
                    }
                } else {
                    return 0
                }
            }
            // handle icon list height one from two cases 1 - not active 0
            //                                            2 - active - height calculated based on (icon list / 2)
            if indexPath.row == 1 {
                if settings.icon_active == "1" {
                    let width = screenSize.width
                    let squreCell = (width / 2) - 10
                    let x : Double = Double(iconList.count) / 2.0
                    if settings.icon_style == "1" {
                        return CGFloat((iconList.count * Int(squreCell)) + (iconList.count * 10))
                    } else {
                        let isInteger = floor(x) == x // for check if reminder one cell or no
                        if isInteger {
                            return CGFloat(((Int(x)) * Int(squreCell)) + (Int(x) * 12))
                        } else {
                            return CGFloat(((Int(x) + 1) * Int(squreCell)) + (Int(x) * 12))
                        }
                    }
                } else {
                    return 0
                }
            }
            if indexPath.row == 2 {
                if settings.post_active == "1" {
                    switch settings.post_style {
                    case "1": return CGFloat((ItemList.count * 200))
                    case "2": return CGFloat((ItemList.count * 228))
                    case "3": return CGFloat((ItemList.count * 144))
                    default: print("No Items design available")
                    }
                } else {
                    return 0
                }
            }
            if indexPath.row == 3 {
                if settings.item_active == "1" {
                    switch settings.item_style {
                    case "1": return CGFloat((ItemList.count * 210) + 30)
                    case "2": return CGFloat((ItemList.count * 228) + 30)
                    case "3": return CGFloat((ItemList.count * 159) + 30)
                    default: print("No Items design available")
                    }
                } else {
                    return 0
                }
            }
        }
        if tableView.tag == 1 {
            switch settings.post_style {
            case "1": return 200
            case "2": return 228
            case "3": return 144
            default: print("No Items design available")
            }
        }
        if tableView.tag == 2 {
            switch settings.item_style {
            case "1": return 210
            case "2": return 228
            case "3": return 159
            default: print("No Items design available")
            }
        }
        return 0
    }
    
    //MARK:- Helper Methods
    //MARK: handle mainTableView
    func HandleMainTableView(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
    
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell", for: indexPath) as! SliderMainScreenTableViewCell
            if settings.icon_slider == "1" {
                if sliderList.count > 0 {
                    var arr = [KingfisherSource]()
                    sliderList.forEach { (icon) in
                        arr.append(KingfisherSource(urlString: icon.image!)!)
                    }
                    cell.sliderShow.setImageInputs(arr)
                }
            }
                print("#sliderCell==>>",indexPath.row)
            
            return cell
        }
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IconTableViewCell", for: indexPath) as! IconTableViewCell
            cell.collectionView.reloadData()
            print("#IconTableViewCell==>>",indexPath.row)

            return cell
        }
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath) as! PostTableViewCell
            cell.postsTableView.reloadData()
            print("#PostTableViewCell==>>",indexPath.row)

            return cell
        }
        if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell", for: indexPath) as! ProductTableViewCell
            cell.ProuductTableView.reloadData()
            print("#ProductTableViewCell==>>",indexPath.row)

            return cell
            
        }
        print("#UITableViewCell==>>",indexPath.row)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView.tag == 1 {
            
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SinglePostViewController") as! SinglePostViewController
            vc.PostID = homeModelResponse.data!.items![indexPath.row].id!
            vc.PostName = homeModelResponse.data!.items![indexPath.row].name!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if tableView.tag == 2 {
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
            vc.ProductID = homeModelResponse.data!.items![indexPath.row].id!
            vc.ProductName = homeModelResponse.data!.items![indexPath.row].name!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: handle Post table View
    func HandlePostsCell(tableView: UITableView, indexPath: IndexPath) -> CustomPostTableViewCell {
        var Postcell : CustomPostTableViewCell!
        print("#settings.post_style ==>",settings.post_style)
        switch settings.post_style {
        case "1":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! CustomPostTableViewCell)
        case "2":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "galaryCell", for: indexPath) as! CustomPostTableViewCell)
        case "3":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "panoramaCell", for: indexPath) as! CustomPostTableViewCell)
            Postcell.descriptionInPanorama.text = ItemList[indexPath.row].detials!
        default: print("No Cells")
        }
        ImageTools.setImage(url: ItemList[indexPath.row].image!,
                            imageView: Postcell.postImage, placeHolder: "myappIcon")
        Postcell.PostTitle.text = ItemList[indexPath.row].name!
        Postcell.postDate.text = ItemList[indexPath.row].created!
        Postcell.PostID = ItemList[indexPath.row].id!
        if AppDelegate.Static.isThereFavorate {
            Postcell.postLike.isHidden = false
            if SharedData.IsFavorate(ID: ItemList[indexPath.row].id!) {
                Postcell.postLike.tintColor = UIColor(hexString: SharedData.AppColor)
            } else {
                Postcell.postLike.tintColor = UIColor(hexString: "#CCCCCC")
            }
        } else {
            Postcell.postLike.isHidden = true
        }
        return Postcell
    }
    
    
    
    //MARK: handle Product table View
    func HandleProductsCell(tableView: UITableView, indexPath: IndexPath) -> CustomProductTableViewCell {
        var ProductCell : CustomProductTableViewCell!
        print("#settings.item_style ==>",settings.post_style)

        switch settings.item_style {
        case "1":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! CustomProductTableViewCell)
        case "2":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "galaryCell", for: indexPath) as! CustomProductTableViewCell)
            ProductCell.PriceBackground.backgroundColor = UIColor(hexString: SharedData.AppColor)
            ProductCell.Price.textColor = UIColor.white
            ProductCell.DiscountPrice.textColor = UIColor.white
            ProductCell.DiscountLine.backgroundColor = UIColor.white
        case "3":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "panoramaCell", for: indexPath) as! CustomProductTableViewCell)
        default: print("No Cells")
        }
        ImageTools.setImage(url: ItemList[indexPath.row].image!,
                            imageView: ProductCell.ProductImage, placeHolder: "myappIcon")
        if ItemList[indexPath.row].price_after_disc != "0.00" {
            ProductCell.DiscountPrice.text = "\(ItemList[indexPath.row].price_after_disc!) \(settings.currency!)"
            ProductCell.DiscountPriceView.isHidden = false
            ProductCell.DiscountLine.isHidden = false
        } else {
            ProductCell.DiscountPriceView.isHidden = true
            ProductCell.DiscountLine.isHidden = true
        }
        if ItemList[indexPath.row].sold == "1" {
            ProductCell.SoldProduct.isHidden = false
        }
        
        ProductCell.Price.text = "\(ItemList[indexPath.row].price!) \(settings.currency!)"
        ProductCell.ProuductName.text = ItemList[indexPath.row].name!
        ProductCell.ProductID = ItemList[indexPath.row].id!
        if AppDelegate.Static.isThereFavorate {
            ProductCell.favBTN.isHidden = false
            if SharedData.IsFavorate(ID: ItemList[indexPath.row].id!) {
                ProductCell.favBTN.tintColor = UIColor(hexString: SharedData.AppColor)
            } else {
                ProductCell.favBTN.tintColor = UIColor(hexString: "#CCCCCC")
            }
        } else {
            ProductCell.favBTN.isHidden = true
        }
        if AppDelegate.Static.isTherecart {
            if SharedData.IsFavorate(ID: ItemList[indexPath.row].id!,forFav: false) {
                ProductCell.cartBTN.tintColor = UIColor(hexString: SharedData.AppColor)
            } else {
                ProductCell.cartBTN.tintColor = UIColor(hexString: "#CCCCCC")
            }
            ProductCell.cartBTN.isHidden = false
        } else {
            ProductCell.cartBTN.isHidden = true
        }
        return ProductCell
    }
}

extension MainScreenViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return iconList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell : IconCollectionViewCell!
        switch settings.icon_style {
        case "1":
            cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! IconCollectionViewCell)
            if iconList[indexPath.row].title == "" {
                cell.TitleView.isHidden = true
            } else {
                cell.TitleView.isHidden = false
            }
        case "2":
            cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! IconCollectionViewCell)
            cell.backgroundColor = UIColor(hexString: "#\(settings.color_c_s!)")
            if iconList[indexPath.row].title == "" {
                cell.TitleView.isHidden = true
            } else {
                cell.TitleView.isHidden = false
            }
        case "3":
            cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! IconCollectionViewCell)
            cell.iconTitle.textColor = UIColor(hexString: "#\(settings.font_color_icon!)")
            cell.backgroundColor = UIColor(hexString: "#\(settings.color_c_s!)")
        case "4":
            cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! IconCollectionViewCell)
            cell.iconTitle.textColor = UIColor(hexString: "#\(settings.font_color_icon!)")
//            cell.roundViewStyle4.backgroundColor = UIColor(hexString: "#\(settings.color_c_s!)")
        default:
            print("No Style Available")
        }
        
        if settings.icon_active == "1" {
            cell.iconTitle.text = iconList[indexPath.row].title!
            ImageTools.setImage(url: iconList[indexPath.row].image!,
                                imageView: cell.iconImage,
                                placeHolder: "myappIcon")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = screenSize.width
        let squreCell = (width / 2)
        if settings.icon_style == "1" {
            return CGSize(width: width, height: squreCell)
        }
        return CGSize(width: squreCell, height: squreCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(iconList[indexPath.row].type!)
        MyAppHandller.shared.NavigateTo(Type: iconList[indexPath.row].type!,
                                        typeStyle: iconList[indexPath.row].style_type!, typeID: iconList[indexPath.row].target_id!, ViewController: self)
    }
    
}
