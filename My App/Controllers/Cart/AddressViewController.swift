//
//  AddressViewController.swift
//  My App
//
//  Created by apple on 3/13/21.
//  Copyright © 2021 apple. All rights reserved.
//

import UIKit

protocol AddressViewControllerDelegate {
    func saveAction(comment: String?, address: String?)
}

class AddressViewController: UIViewController {

    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var addressTextView: UITextView!
    
    var delegate: AddressViewControllerDelegate?
            
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                        
        let tapView = UITapGestureRecognizer(target: self, action: #selector(self.handleTapView(_:)))
        view.addGestureRecognizer(tapView)
        
    }
    
    @objc func handleTapView(_ sender: UITapGestureRecognizer? = nil) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if self.delegate != nil {
            dismiss(animated: true, completion: nil)
            delegate?.saveAction(comment: commentTextView.text, address: addressTextView.text)
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
        
}
