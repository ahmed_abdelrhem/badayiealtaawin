//
//  ViewController.swift
//  My App
//
//  Created by Kirollos Maged on 11/5/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import MOLH

class GIFVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("TOKEN :   ",MyAppHandller.shared.Value(of: DEVICE_TOKEN))
        GetMyyAppSetting()
        
    }
    //MARK:-  Send post request to get all my app configurations
    func GetMyyAppSetting() {
        let param = ["method":"setting",
                     "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    let res = try JSONDecoder().decode(MyAppSettingResponse.self, from: response.data!)
                    MyAppHandller.shared.CashMyAppSettings(from: res)
                    SettingWrapper.setupMyAppFromSetting()
                    self.HandleUserState()
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                print(response.error!.localizedDescription)
            }
        }
    }
    
    //MARK:- as per my app configurations in dashboard we setup language (BED)
    func HandleMyAppLanguage() {
        if MyAppHandller.shared.isFirstLaunch() {
            if AppDelegate.Static.isThereLangues {
                if AppDelegate.Static.isArabic {
                    MOLH.setLanguageTo("ar")
                } else {
                    MOLH.setLanguageTo("en")
                }
            } else {
                if AppDelegate.Static.isArabic {
                    MOLH.setLanguageTo("ar")
                } else {
                    MOLH.setLanguageTo("en")
                }
            }
            UserDefaults.standard.set("-1", forKey: "fLaunch")
        }
    }
    
    //MARK:- handle MyApp language from dashboard (BED)
    func MyAppLanguageConfiguredGood() -> Bool {
        if MyAppHandller.shared.isFirstLaunch() {
            var myAppLanguage: String {
                if AppDelegate.Static.isArabic {
                    return "ar"
                }
                return "en"
            }
            var MobileLanguage: String {
                return MOLHLanguage.currentAppleLanguage()
            }
            
            if MobileLanguage != myAppLanguage {
                return false
            }
            return true
        }
        return true
    }
    
    // MARK:- handle if user is Loged in or no if not loged in we send fake regestration
    func HandleUserState() {
        var param : [String: Any] = ["":""]
        if let user_id = UserDefaults.standard.value(forKey: User_id) {
            //MARK: Login
           HandleNavigation()
        } else {
            //MARK: Fake Login
            param = ["method":"fake_account",
                     "device_id":"\(DEVICE_ID)",
                "password":"\(DEVICE_ID)",
                "email":"\(DEVICE_ID)@mail.com",
                "active":"1",
                "type":"1",
                "google_id":"\(MyAppHandller.shared.Value(of: DEVICE_TOKEN))"]
            AuthRequest(param, IsFakeLogin: true)
        }
        print("USER_ID , ",MyAppHandller.shared.Value(of: "DEFuserid"))
    }
    
    // MARK:- handle MyApp navigation as per currunt situation arabic or englis
    func HandleNavigation() {
        var identfier :String!
        if MyAppLanguageConfiguredGood() {
            if MOLHLanguage.isArabic() {
                identfier = "SWRevealViewController1ar"
            } else {
                identfier = "SWRevealViewController1"
            }
            
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identfier)
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
            
        } else {
            //Language Configuration Message
            let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LanguageMessageViewController") as! LanguageMessageViewController
            self.addChild(popUpVC)
            self.view.addSubview(popUpVC.view)
            popUpVC.didMove(toParent: self)
            self.HandleMyAppLanguage()
        }
    }
    
    // MARK:- after create login method we create navigation
    func AuthRequest(_ parameters: [String:Any],IsFakeLogin: Bool) {
        print(parameters)
        APIManager.sharedInstance.postReques(Parameters: parameters) { (res) in
            if res.error == nil {
                let json = JSON(res.value!)
                print(json)
                if IsFakeLogin {
                    guard let password = json["data"][0]["password"].string else { return }
                    guard let email = json["data"][0]["email"].string else { return }
                    guard let id = json["data"][0]["id"].string else { return }
                    print("FakeID : ",id)
                    UserDefaults.standard.setValue(email, forKey: "DEFemail")
                    UserDefaults.standard.setValue(id, forKey: "DEFuserid")
                    UserDefaults.standard.set(email, forKey: "DEFuserphone")
                    UserDefaults.standard.setValue(password, forKey: "DEFPWD")
                }
                self.HandleNavigation()
            }
        }
    }
    
}

