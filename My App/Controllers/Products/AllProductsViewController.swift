//
//  AllProductsViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftyJSON
import CoreData

class AllProductsViewController: UIViewController {
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var catCollectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    var pageTitle = ""
    var type_style: String = "0" // Screen Style
    var type_id: String = "0" // contecntID
    var fromSide :Bool = false
    var settings: Settings!
    var itemModelResponse: ItemModelResponse!
    var ItemList = [ItemsModel]()
    var CatList = [CatRandom]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateCartCount), name: Notification.Name("updateCartCount"), object: nil)
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        settings = MyAppSettings.data!.settings![0]
        let param = ["category_id":"\(type_id)",
            "from":"0",
            "method":"items_and_random_items",
            "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        print(param)
        getAllProducts(param)
    }
    
    @objc func updateCartCount() {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: pageTitle, backBtn: !fromSide, LargTitle: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: pageTitle, backBtn: !fromSide, LargTitle: false)
    }
    
    //MARK:- Requests Online
    func getAllProducts(_ param: [String: Any]) {
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    print(JSON(response.value!))
                    self.itemModelResponse = try JSONDecoder().decode(ItemModelResponse.self, from: response.data!)
                    self.ItemList = self.itemModelResponse.data!.items!
                    if let _random = self.itemModelResponse.data?.random {
                        self.CatList = _random
                        self.catCollectionView.reloadData()
                    }
                    if self.CatList.count == 0 {
                        self.catCollectionView.isHidden = true
                    }
                    self.tableView.reloadData()
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
        }
    }
    
    //MARK:- Helper Methods
    //MARK: handle Product table View
    func HandleProductsCell(tableView: UITableView, indexPath: IndexPath) -> CustomProductTableViewCell {
        var ProductCell : CustomProductTableViewCell!
        switch type_style {
        case "1":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! CustomProductTableViewCell)
        case "2":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "galaryCell", for: indexPath) as! CustomProductTableViewCell)
            ProductCell.PriceBackground.backgroundColor = UIColor(hexString: SharedData.AppColor)
            ProductCell.Price.textColor = UIColor.white
            ProductCell.DiscountPrice.textColor = UIColor.white
            ProductCell.DiscountLine.backgroundColor = UIColor.white
        case "3":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "panoramaCell", for: indexPath) as! CustomProductTableViewCell)
            ProductCell.ProductDescription.text = ItemList[indexPath.row].details!
        default:
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "panoramaCell", for: indexPath) as! CustomProductTableViewCell)
            ProductCell.ProductDescription.text = ItemList[indexPath.row].details!
        }
        ImageTools.setImage(url: ItemList[indexPath.row].image!,
                            imageView: ProductCell.ProductImage, placeHolder: "myappIcon")
        if ItemList[indexPath.row].price_after_disc != "0.00" {
            print(settings.currency!)
            ProductCell.DiscountPrice.text = "\(ItemList[indexPath.row].price_after_disc!) \(settings.currency!)"
            ProductCell.DiscountPriceView.isHidden = false
            ProductCell.DiscountLine.isHidden = false
        } else {
            ProductCell.DiscountPriceView.isHidden = true
            ProductCell.DiscountLine.isHidden = true
        }
        if ItemList[indexPath.row].sold == "1" {
            ProductCell.SoldProduct.isHidden = false
        }
        ProductCell.Price.text = "\(ItemList[indexPath.row].price!) \(settings.currency!)"
        ProductCell.ProuductName.text = ItemList[indexPath.row].name!
        ProductCell.ProductID = ItemList[indexPath.row].id!
        if AppDelegate.Static.isThereFavorate {
            ProductCell.favBTN.isHidden = false
            if SharedData.IsFavorate(ID: ItemList[indexPath.row].id!) {
                ProductCell.favBTN.tintColor = UIColor(hexString: SharedData.AppColor)
            } else {
                ProductCell.favBTN.tintColor = UIColor(hexString: "#CCCCCC")
            }
        } else {
            ProductCell.favBTN.isHidden = true
        }
        if AppDelegate.Static.isTherecart {
            if SharedData.IsCart(ID: ItemList[indexPath.row].id!,forFav: false) {
                ProductCell.cartBTN.tintColor = UIColor(hexString: SharedData.AppColor)
            } else {
                ProductCell.cartBTN.tintColor = UIColor(hexString: "#CCCCCC")
            }
            ProductCell.cartBTN.isHidden = false
        } else {
            ProductCell.cartBTN.isHidden = true
        }
        return ProductCell
    }
    
    //MARK: handle Post table View
    func HandlePostsCell(tableView: UITableView, indexPath: IndexPath) -> CustomPostTableViewCell {
        var Postcell : CustomPostTableViewCell!
        switch type_style {
        case "1":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventnewsCell", for: indexPath) as! CustomPostTableViewCell)
        case "2":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventgalaryCell", for: indexPath) as! CustomPostTableViewCell)
        case "3":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventpanoramaCell", for: indexPath) as! CustomPostTableViewCell)
            Postcell.descriptionInPanorama.text = ItemList[indexPath.row].details!
        default:
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventpanoramaCell", for: indexPath) as! CustomPostTableViewCell)
            Postcell.descriptionInPanorama.text = ItemList[indexPath.row].details!
        }
        ImageTools.setImage(url: ItemList[indexPath.row].image!,
                            imageView: Postcell.postImage, placeHolder: "myappIcon")
        Postcell.PostTitle.text = ItemList[indexPath.row].name!
        Postcell.postDate.text = ItemList[indexPath.row].created!
        Postcell.PostID = ItemList[indexPath.row].id!
        if AppDelegate.Static.isThereFavorate {
            Postcell.postLike.isHidden = false
            if SharedData.IsFavorate(ID: ItemList[indexPath.row].id!) {
                Postcell.postLike.tintColor = UIColor(hexString: SharedData.AppColor)
            } else {
                Postcell.postLike.tintColor = UIColor(hexString: "#CCCCCC")
            }
        } else {
            Postcell.postLike.isHidden = true
        }
        return Postcell
    }

}

extension AllProductsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ItemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ItemList[indexPath.row].type == "1" {
            let cell = HandleProductsCell(tableView: tableView, indexPath: indexPath)
            return cell
        } else {
            let cell = HandlePostsCell(tableView: tableView, indexPath: indexPath)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ItemList[indexPath.row].type == "1" {
            switch type_style {
            case "1": return 210
            case "2": return 228
            case "3": return 159
            default: return 159
            }
        } else if ItemList[indexPath.row].type == "0" {
            switch type_style {
            case "1": return 200
            case "2": return 228
            case "3": return 144
            default: return 144
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ItemList[indexPath.row].type == "1" {
            tableView.deselectRow(at: indexPath, animated: true)
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
            vc.ProductID = ItemList[indexPath.row].id!
            vc.ProductName = ItemList[indexPath.row].name!
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SinglePostViewController") as! SinglePostViewController
            vc.PostID = ItemList[indexPath.row].id!
            vc.PostName = ItemList[indexPath.row].name!
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(
            withDuration: 0.2,
            delay: 0.02 * Double(indexPath.row),
            animations: {
                cell.alpha = 1
        })
    }
}



extension AllProductsViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CatList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CategoryCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
        cell.relatedProductName.text = CatList[indexPath.row].name!
        ImageTools.setImage(url: CatList[indexPath.row].image!, imageView: cell.relatedProductImage, placeHolder: "myappIcon")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.ItemList = []
        self.tableView.reloadData()
        let param = ["category_id":"\(CatList[indexPath.row].category_id!)",
            "from":"0",
            "method":"items_and_random_items",
            "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        type_style = CatList[indexPath.row].style_type!
        getAllProducts(param)
    }
}


