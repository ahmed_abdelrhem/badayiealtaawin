//
//  PartenerCollectionViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/30/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class PartenerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var partImage: RoundImage!
    @IBOutlet weak var partName: UILabel!
}
