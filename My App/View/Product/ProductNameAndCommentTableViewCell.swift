//
//  ProductNameAndCommentTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/25/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class ProductNameAndCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var comments: UIButton!
    @IBOutlet weak var ProductName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func Comments(_ sender: Any) {
    }
    
}

class PriceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var PriceTitleLable: UILabel!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var dicountLable: UILabel!
    @IBOutlet weak var lineInDiscount: UIView!
    @IBOutlet weak var discountView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
  
    
}

class DetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var detailsTitleLable: UILabel!
    
    @IBOutlet weak var PostDate: UILabel!
    @IBOutlet weak var productDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
  
}


class RelatedTableViewCell: UITableViewCell {
    

    @IBOutlet weak var relatedTitleLable: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class AdsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var AdImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

class RelatedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var relatedProductImage: UIImageView!
    
    @IBOutlet weak var relatedProductName: UILabel!
    
    @IBOutlet weak var soldView: UIView!
}

