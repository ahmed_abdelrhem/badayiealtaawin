//
//  Extensions.swift
//  My App
//
//  Created by Kirollos Maged on 12/6/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import UIKit


//MARK:- View
extension UIView {
    
    func bindFrameToSuperviewBounds(_ padding:CGFloat = 0) {
        guard let superview = self.superview  else {
            //            print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["subview": self]))
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(padding)-[subview]-0-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["subview": self]))
    }
    
    func roundedCorners(_ radius:CGFloat = 5, color:UIColor = UIColor.lightGray,borderSize:CGFloat = 0.5) {
        
        self.layer.cornerRadius = radius
        self.layer.borderWidth = borderSize
        self.layer.borderColor = color.cgColor
        self.clipsToBounds = true
        
    }
    
    func addTopBorderWithColor(_ color: UIColor, width: CGFloat,view:UIView) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 10/2, y: 0, width: view.frame.size.width-10, height: width)
        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(_ color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(_ color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(_ color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func circularView(_ color:UIColor = UIColor.clear,_ brdWidth:CGFloat = 0){
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true;
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = brdWidth
    }
    
}


//MARK:- Buttons
extension UIButton {
    public func tintColorToButton(_ color: UIColor) {
        //        let button = self
        //        button.ty
        self.currentImage?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
        //        return button
    }
}
