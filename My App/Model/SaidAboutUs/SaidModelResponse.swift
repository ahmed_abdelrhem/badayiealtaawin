//
//  SaidModelResponse.swift
//  My App
//
//  Created by Kirollos Maged on 1/1/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
struct SaidModelResponse : Codable {
    let state : String?
    let data : [SaidData]?
    
    enum CodingKeys: String, CodingKey {
        
        case state = "state"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        data = try values.decodeIfPresent([SaidData].self, forKey: .data)
    }
    
}
