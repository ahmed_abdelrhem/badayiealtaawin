

import Foundation
struct Items : Codable {
	let id : String?
	let category_id : String?
	let name : String?
	let detials : String?
	let sold : String?
	let created : String?
	let image : String?
	let price : String?
	let price_after_disc : String?
	let type : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case category_id = "category_id"
		case name = "name"
		case detials = "detials"
		case sold = "sold"
		case created = "created"
		case image = "image"
		case price = "price"
		case price_after_disc = "price_after_disc"
		case type = "type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		detials = try values.decodeIfPresent(String.self, forKey: .detials)
		sold = try values.decodeIfPresent(String.self, forKey: .sold)
		created = try values.decodeIfPresent(String.self, forKey: .created)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		price = try values.decodeIfPresent(String.self, forKey: .price)
		price_after_disc = try values.decodeIfPresent(String.self, forKey: .price_after_disc)
		type = try values.decodeIfPresent(String.self, forKey: .type)
	}

}
