//
//  SocialMediaTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/30/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class SocialMediaTableViewCell: UITableViewCell {

    @IBOutlet weak var HeaderImage: UIImageView!
    @IBOutlet weak var socialImage: UIImageView!
    @IBOutlet weak var socialName: UILabel!
    @IBOutlet weak var socialurl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
