//
//  AppDelegateExtension.swift
//  My App
//
//  Created by Kirollos Maged on 12/6/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import UIKit

extension AppDelegate {
    struct Static {
        static var isFirstTime = false
        static var isArabic = false
        static var deviceToken:String = ""
        static var compImage:String = ""
        static var compName:String = ""
        static var deviceID = ""
        static var isRegistered = false
        static var isTherecart = true            // cart
        static var isThereLangues = true         // language
        static var isThereNotification = true    // notification
        static var isThereProfile = true    // notification

        static var isThereFavorate = true        // favorate
        static var isThereSearch = true          // search
        static var isThereChat = true            // chat
        static var isFromNotif = true
        static var isThereSparatorInSideMenu = true // asking for saparator between cells in side menu
        static var isThereHeaderInSideMenu = true
    }
}
