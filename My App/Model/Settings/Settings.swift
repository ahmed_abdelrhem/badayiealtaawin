

//  My App
//
//  Created by Kirollos Maged on 12/6/18.
//  Copyright © 2018 apple. All rights reserved.
//
import Foundation

struct Settings : Codable {

    let lang : String?
    let d_lang : String?
    let app_color : String?
    let logo : String?
    let search_icon : String?
    let chat_icon : String?
    let chat_admin : String?
    let chat : String?
    let notification_icon : String?
    let fav_icon : String?
    let fav_admin : String?
    let carts : String?
    let cart_icon : String?
    let cart_admin : String?
    let member_app : String?
    let members : String?
    let member_admin : String?
    let menu_bgcolor : String?
    let menutop_text_color : String?
    let menu_name : String?
    let menutop_logo : String?
    let menutop_image : String?
    let menutop_bgcolor : String?
    let menu_text_color : String?
    let menu_line_color : String?
    let arrow_color : String?
    let menu_devided_line_color : String?
    let menu_line_check : String?
    let grand_phone : String?
    let menutop_image_check : String?
    let icon_active : String?
    let icon_style : String?
    let slider_full : String?
    let background_color_icon : String?
    let icon_slider : String?
    let item_active : String?
    let post_active : String?
    let item_num_app : String?
    let post_num_app : String?
    let social_header : String?
    let team_header : String?
    let team_description : String?
    let chat_reply : String?
    let reply : String?
    let map_company : String?
    let register_mobile_active : String?
    let email_activation : String?
    let admin_approval : String?
    let partner_header : String?
    let partners_description : String?
    let comments_active : String?
    let website : String?
    let android_url : String?
    let posts_enable_location : String?
    let posts_enable_file : String?
    let posts_enable_url : String?
    let comments_attash : String?
    let comment_approval_active : String?
    let actionbar_language_icon : String?
    let actionbar_cart_icon : String?
    let actionbar_search_icon : String?
    let actionbar_fav_icon : String?
    let actionbar_chat_icon : String?
    let actionbar_notification_icon : String?
    let item_style : String?
    let post_style : String?
    let font_color_icon : String?
    let color_c_s : String?
    let currency : String?
    let loading : String?
    let menutop_logo_check: String?
    let copyright: String?

    
    enum CodingKeys: String, CodingKey {
        
        case lang = "lang"
        case d_lang = "d_lang"
        case app_color = "app_color"
        case logo = "logo"
        case search_icon = "search_icon"
        case chat_icon = "chat_icon"
        case chat_admin = "chat_admin"
        case chat = "chat"
        case notification_icon = "notification_icon"
        case fav_icon = "fav_icon"
        case fav_admin = "fav_admin"
        case carts = "carts"
        case cart_icon = "cart_icon"
        case cart_admin = "cart_admin"
        case member_app = "member_app"
        case members = "members"
        case member_admin = "member_admin"
        case menu_bgcolor = "menu_bgcolor"
        case menutop_text_color = "menutop_text_color"
        case menu_name = "menu_name"
        case menutop_logo = "menutop_logo"
        case menutop_image = "menutop_image"
        case menutop_bgcolor = "menutop_bgcolor"
        case menu_text_color = "menu_text_color"
        case menu_line_color = "menu_line_color"
        case arrow_color = "arrow_color"
        case menu_devided_line_color = "menu_devided_line_color"
        case menu_line_check = "menu_line_check"
        case grand_phone = "grand_phone"
        case menutop_image_check = "menutop_image_check"
        case icon_active = "icon_active"
        case icon_style = "icon_style"
        case slider_full = "slider_full"
        case background_color_icon = "background_color_icon"
        case icon_slider = "icon_slider"
        case item_active = "item_active"
        case post_active = "post_active"
        case item_num_app = "item_num_app"
        case post_num_app = "post_num_app"
        case social_header = "social_header"
        case team_header = "team_header"
        case team_description = "team_description"
        case chat_reply = "chat_reply"
        case reply = "reply"
        case map_company = "map_company"
        case register_mobile_active = "register_mobile_active"
        case email_activation = "email_activation"
        case admin_approval = "admin_approval"
        case partner_header = "partner_header"
        case partners_description = "partners_description"
        case comments_active = "comments_active"
        case website = "website"
        case android_url = "android_url"
        case posts_enable_location = "posts_enable_location"
        case posts_enable_file = "posts_enable_file"
        case posts_enable_url = "posts_enable_url"
        case comments_attash = "comments_attash"
        case comment_approval_active = "comment_approval_active"
        case actionbar_language_icon = "actionbar_language_icon"
        case actionbar_cart_icon = "actionbar_cart_icon"
        case actionbar_search_icon = "actionbar_search_icon"
        case actionbar_fav_icon = "actionbar_fav_icon"
        case actionbar_chat_icon = "actionbar_chat_icon"
        case actionbar_notification_icon = "actionbar_notification_icon"
        case item_style = "item_style"
        case post_style = "post_style"
        case font_color_icon = "font_color_icon"
        case color_c_s = "color_c_s"
        case currency = "currency"
        case loading = "loading"
        case menutop_logo_check = "menutop_logo_check"
        case copyright = "copyright"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lang = try values.decodeIfPresent(String.self, forKey: .lang)
        d_lang = try values.decodeIfPresent(String.self, forKey: .d_lang)
        app_color = try values.decodeIfPresent(String.self, forKey: .app_color)
        logo = try values.decodeIfPresent(String.self, forKey: .logo)
        search_icon = try values.decodeIfPresent(String.self, forKey: .search_icon)
        chat_icon = try values.decodeIfPresent(String.self, forKey: .chat_icon)
        chat_admin = try values.decodeIfPresent(String.self, forKey: .chat_admin)
        chat = try values.decodeIfPresent(String.self, forKey: .chat)
        notification_icon = try values.decodeIfPresent(String.self, forKey: .notification_icon)
        fav_icon = try values.decodeIfPresent(String.self, forKey: .fav_icon)
        fav_admin = try values.decodeIfPresent(String.self, forKey: .fav_admin)
        carts = try values.decodeIfPresent(String.self, forKey: .carts)
        cart_icon = try values.decodeIfPresent(String.self, forKey: .cart_icon)
        cart_admin = try values.decodeIfPresent(String.self, forKey: .cart_admin)
        member_app = try values.decodeIfPresent(String.self, forKey: .member_app)
        members = try values.decodeIfPresent(String.self, forKey: .members)
        member_admin = try values.decodeIfPresent(String.self, forKey: .member_admin)
        menu_bgcolor = try values.decodeIfPresent(String.self, forKey: .menu_bgcolor)
        menutop_text_color = try values.decodeIfPresent(String.self, forKey: .menutop_text_color)
        menu_name = try values.decodeIfPresent(String.self, forKey: .menu_name)
        menutop_logo = try values.decodeIfPresent(String.self, forKey: .menutop_logo)
        menutop_image = try values.decodeIfPresent(String.self, forKey: .menutop_image)
        menutop_bgcolor = try values.decodeIfPresent(String.self, forKey: .menutop_bgcolor)
        menu_text_color = try values.decodeIfPresent(String.self, forKey: .menu_text_color)
        menu_line_color = try values.decodeIfPresent(String.self, forKey: .menu_line_color)
        arrow_color = try values.decodeIfPresent(String.self, forKey: .arrow_color)
        menu_devided_line_color = try values.decodeIfPresent(String.self, forKey: .menu_devided_line_color)
        menu_line_check = try values.decodeIfPresent(String.self, forKey: .menu_line_check)
        grand_phone = try values.decodeIfPresent(String.self, forKey: .grand_phone)
        menutop_image_check = try values.decodeIfPresent(String.self, forKey: .menutop_image_check)
        icon_active = try values.decodeIfPresent(String.self, forKey: .icon_active)
        icon_style = try values.decodeIfPresent(String.self, forKey: .icon_style)
        slider_full = try values.decodeIfPresent(String.self, forKey: .slider_full)
        background_color_icon = try values.decodeIfPresent(String.self, forKey: .background_color_icon)
        icon_slider = try values.decodeIfPresent(String.self, forKey: .icon_slider)
        item_active = try values.decodeIfPresent(String.self, forKey: .item_active)
        post_active = try values.decodeIfPresent(String.self, forKey: .post_active)
        item_num_app = try values.decodeIfPresent(String.self, forKey: .item_num_app)
        post_num_app = try values.decodeIfPresent(String.self, forKey: .post_num_app)
        social_header = try values.decodeIfPresent(String.self, forKey: .social_header)
        team_header = try values.decodeIfPresent(String.self, forKey: .team_header)
        team_description = try values.decodeIfPresent(String.self, forKey: .team_description)
        chat_reply = try values.decodeIfPresent(String.self, forKey: .chat_reply)
        reply = try values.decodeIfPresent(String.self, forKey: .reply)
        map_company = try values.decodeIfPresent(String.self, forKey: .map_company)
        register_mobile_active = try values.decodeIfPresent(String.self, forKey: .register_mobile_active)
        email_activation = try values.decodeIfPresent(String.self, forKey: .email_activation)
        admin_approval = try values.decodeIfPresent(String.self, forKey: .admin_approval)
        partner_header = try values.decodeIfPresent(String.self, forKey: .partner_header)
        partners_description = try values.decodeIfPresent(String.self, forKey: .partners_description)
        comments_active = try values.decodeIfPresent(String.self, forKey: .comments_active)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        android_url = try values.decodeIfPresent(String.self, forKey: .android_url)
        posts_enable_location = try values.decodeIfPresent(String.self, forKey: .posts_enable_location)
        posts_enable_file = try values.decodeIfPresent(String.self, forKey: .posts_enable_file)
        posts_enable_url = try values.decodeIfPresent(String.self, forKey: .posts_enable_url)
        comments_attash = try values.decodeIfPresent(String.self, forKey: .comments_attash)
        comment_approval_active = try values.decodeIfPresent(String.self, forKey: .comment_approval_active)
        actionbar_language_icon = try values.decodeIfPresent(String.self, forKey: .actionbar_language_icon)
        actionbar_cart_icon = try values.decodeIfPresent(String.self, forKey: .actionbar_cart_icon)
        actionbar_search_icon = try values.decodeIfPresent(String.self, forKey: .actionbar_search_icon)
        actionbar_fav_icon = try values.decodeIfPresent(String.self, forKey: .actionbar_fav_icon)
        actionbar_chat_icon = try values.decodeIfPresent(String.self, forKey: .actionbar_chat_icon)
        actionbar_notification_icon = try values.decodeIfPresent(String.self, forKey: .actionbar_notification_icon)
        item_style = try values.decodeIfPresent(String.self, forKey: .item_style)
        post_style = try values.decodeIfPresent(String.self, forKey: .post_style)
        font_color_icon = try values.decodeIfPresent(String.self, forKey: .font_color_icon)
        color_c_s = try values.decodeIfPresent(String.self, forKey: .color_c_s)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        loading = try values.decodeIfPresent(String.self, forKey: .loading)
        menutop_logo_check = try values.decodeIfPresent(String.self, forKey: .menutop_logo_check)
        copyright = try values.decodeIfPresent(String.self, forKey: .copyright)
        
    }

    
    static func setSettingObject(settingObject:Settings){
        SharedData.setObjectData(anyObject: settingObject, key: "settings")
    }
    
    static func getSettingObject() -> Settings?{
        if let myObj = SharedData.getObjectData(key: "settings") as? Settings {
            return myObj
        }
        return nil
    }

}
