//
//  ChatTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 2/18/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import Kingfisher

class ChatTableViewCell: UITableViewCell {
    

    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var messagePicture: RoundImage!
    @IBOutlet weak var timeLable: UILabel!
    @IBOutlet weak var seenStatusImage: UIImageView!
    
  

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImage(url : String,placeHolder: String) {
        if url.contains(".jpg") || url.contains(".JPG") || url.contains(".JPEG") || url.contains(".jpeg") || url.contains(".png") || url.contains(".PNG") {
            DispatchQueue.global().async {
                DispatchQueue.main.async {
                    self.messagePicture.kf.setImage(with: URL(string: "http://mobile-app-company.com/admin/files/chat/" + url), placeholder: UIImage(named: placeHolder), options: nil, progressBlock: nil) { (image, error, cashe, url) in
                        if error != nil {
                            self.messagePicture.image = UIImage(named: placeHolder)
                        }
                    }
                }
            }
        } else {
            let image = convertImage(from: url)
            self.messagePicture.image = image
        }
    }
    
    func convertImage(from base64: String) -> UIImage {
        let dataDecoded:NSData = NSData(base64Encoded: base64, options: NSData.Base64DecodingOptions(rawValue: 0))!
        let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
        
        return decodedimage
    }
    
}
