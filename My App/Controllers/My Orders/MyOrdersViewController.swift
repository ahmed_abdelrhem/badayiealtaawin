//
//  MyOrdersViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/31/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class MyOrdersViewController: UIViewController {

    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    var fromSide: Bool = false
    var order: OrderModelResponse?
    var OrderList = [OrderData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        getOrders()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "My Orders".localized, backBtn: !fromSide, LargTitle: false)
    }
    
    //MARK:- Requests Online
    func getOrders() {
        let param = [
            "method":"orders",
            "user_id":"\(SharedData.userId)",
            "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"
        ]
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.order = nil
                    self.tableView.reloadData()
                    self.order = try JSONDecoder().decode(OrderModelResponse.self, from: response.data!)
                    switch self.order!.state {
                    case ResponseState.sucess.rawValue :
                        self.order!.data!.forEach({ (newitem) in
                            self.OrderList.append(newitem)
                        })
                    case ResponseState.empty.rawValue:
                        print("empty data")
                    case ResponseState.failed.rawValue:
                        print("Faild Request")
                    default:
                        print("No State in Reponse")
                    }
                    self.indicator.stopAnimating()
                    self.tableView.reloadData()
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
        }
    }

}

extension MyOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = order?.data?.count {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrderTableViewCell
        cell.orderNumLbl.text = " \(order?.data?[indexPath.row].id ?? 0)"
        cell.dateLbl.text = order?.data?[indexPath.row].created
        cell.statusLbl.text = order?.data?[indexPath.row].status_name
        cell.statusLbl.textColor = UIColor(hexString: (order?.data?[indexPath.row].status_color)!)
        cell.totalLbl.text = " \(order?.data?[indexPath.row].total_price ?? 0)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailsViewController") as! OrderDetailsViewController
        vc.items = (order?.data?[indexPath.row].items)!
        navigationController?.pushViewController(vc, animated: true)
    }
}
