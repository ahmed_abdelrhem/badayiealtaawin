

import Foundation
struct SocialData : Codable {
	let id : Int?
	let title : String?
	let icon : String?
	let link : String?
	let kind_link : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case title = "title"
		case icon = "icon"
		case link = "link"
		case kind_link = "kind_link"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		icon = try values.decodeIfPresent(String.self, forKey: .icon)
		link = try values.decodeIfPresent(String.self, forKey: .link)
		kind_link = try values.decodeIfPresent(Int.self, forKey: .kind_link)
	}

}
