//
//  SideCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/13/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Kingfisher

class SideCell: UIView {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellText: UILabel!
    @IBOutlet weak var arrowBTN: UIButton!

    
    var MyAppModel = MyAppHandller.shared.getMyAppSettings()
    var view: UIView!
    
    //MARK:- initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK:- helper Methpds
   
    func setImage(url : String) {
        DispatchQueue.global().async {
            if url != "" {
                let Myurl = URL(string: url)!
                DispatchQueue.main.async {
                    if UIApplication.shared.canOpenURL(Myurl) {
                        self.cellImage.kf.setImage(with: URL(string: url))
                    }
                }
            }
        }
    }
    
    func setupCellView() {
        guard let settings = MyAppModel.data?.settings?[0] else { return }
        if let arrowC = settings.arrow_color {
            arrowBTN.tintColor = UIColor(hexString: "#" + arrowC)
        }
        if let menuTextColor = settings.menu_text_color {
            cellText.textColor = UIColor(hexString: "#" + menuTextColor)
        }
    }
    
    func setup() {
        view = loadViewFromNib()
        
        guard let settings = MyAppModel.data?.settings?[0] else { return }
        if let arrowC = settings.arrow_color {
            arrowBTN.tintColor = UIColor(hexString: "#" + arrowC)
        }
        if let menuTextColor = settings.menu_text_color {
            cellText.textColor = UIColor(hexString: "#" + menuTextColor)
        }
        view.clipsToBounds = true
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]; addSubview(view)
    }
    
    func haveArrow(MainCell:Bool) {
        arrowBTN.isHidden = !MainCell
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for:type(of: self))
        let nib = UINib(nibName: "SideCell", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
}
