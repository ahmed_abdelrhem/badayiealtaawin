//
//  ChatViewController.swift
//  My App
//
//  Created by Kirollos Maged on 2/14/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

class ChatViewController: UIViewController , UITextFieldDelegate, UITextViewDelegate {
    
    //    @IBOutlet weak var messageText: UITextField!
//    @IBOutlet weak var textViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var textViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageText: UITextView!
    @IBOutlet weak var optionsBTN: UIButton!
    @IBOutlet weak var sendBTN: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var seenStatusImage: UIImageView!
    var locManager = CLLocationManager()
    var lat: String = ""
    var long: String = ""
    var chatList = [ChatEntityMo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isHidden = true
        
        getMessagesFromDatabase()
        NotificationCenter.default.addObserver(forName: MESSAGE_NOTIFY, object: nil, queue: nil) { notification in
            self.getMessagesFromDatabase()
        }
        locationConfiguration()
        setupFirstTimeToChat()
        setupView()
    }
    
    //MARK:- helper methods
    
    func setupFirstTimeToChat() {
        if MyAppHandller.shared.Value(of: "FTChat") != "1" {
            let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatPopupViewController") as! ChatPopupViewController
            self.addChild(popUpVC)
            self.view.addSubview(popUpVC.view)
            popUpVC.didMove(toParent: self)
        }
       
    }
    
    func getMessagesFromDatabase() {
        self.chatList = []
        let fetchRequest  : NSFetchRequest<ChatEntityMo> = ChatEntityMo.fetchRequest()
        do
        {
            let coredataList = try PresistanceServce.context.fetch(fetchRequest)
            if coredataList.count > 0
            {
                print(coredataList.count)
                self.chatList = coredataList
                self.tableView.reloadData()
                scrollBottom()
                
            }
        }
        catch {
            MyAppHandller.shared.handleErrors(from: self, error: error)
        }
    }
    
    func locationConfiguration() {
        locManager.requestWhenInUseAuthorization()
        var currentLocation: CLLocation!
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            currentLocation = self.locManager.location
            self.lat = String(currentLocation.coordinate.latitude)
            self.long = String(currentLocation.coordinate.longitude)
        }
    }
    func setupView() {
        sendBTN.tintColor = UIColor(hexString: SharedData.AppColor)
        optionsBTN.tintColor = UIColor(hexString: SharedData.AppColor)
        tableView.isHidden = false
    }
    
    func scrollBottom() {
        let numberOfSections = self.tableView.numberOfSections
        let numberOfRows = chatList.count
        
        let indexPath = IndexPath(row: numberOfRows-1 , section: numberOfSections-1)
        self.tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: false)
    }
    
    func saveMessage(reciver_id: String,sender_id:String,message_id: Int64,notification_type: String,message: String,location:String,voice:String,image:String,time: String) {
        let subject = ChatEntityMo(context: PresistanceServce.context)
        print("New MEssage ID: ", message_id)
        subject.reciver_id = reciver_id
        subject.sender_id = sender_id
        subject.message_id = message_id
        subject.notification_type = notification_type
        subject.message = message
        subject.location = location
        subject.voice = voice
        subject.image = image
        subject.time = time
        subject.messageSent = 0
        PresistanceServce.saveContext()
        
    }
    
    func lastMessageID() -> Int64 {
        let fetchRequest  : NSFetchRequest<ChatEntityMo> = ChatEntityMo.fetchRequest()
        
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequest)
            if test.count > 0
            {
                test.forEach { (item) in
                    print("DBMESSAGEID : ",(item.message_id))
                    
                }
                return (test.last?.message_id)!
            }
        }
        catch {
            print(error)
            return 0
        }
        return 0
    }
    
    func sentSuccess(_ status:Bool,messageId: Int64,
                     reciver_id: String,
                     sender_id:String,
                     message_id: Int64,
                     notification_type: String,
                     message: String,
                     location:String,
                     voice:String,
                     image:String,
                     time: String) {
        if status == true {
            UpdateSentActionInCoredata(identifier: messageId,
                                       reciver_id: reciver_id,
                                       sender_id:sender_id,
                                       message_id: message_id,
                                       notification_type: notification_type,
                                       message: message,
                                       location:location,
                                       voice:voice,
                                       image:image,
                                       time: time)
            
        }
    }
    
    func NavigateToGoogleMap(toLatitude: String, toLongitude: String) {
        // if GoogleMap installed
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?saddr=\(self.lat),\(self.long)&daddr=\(toLatitude),\(toLongitude)&directionsmode=driving")! as URL)
            
        } else {
            // if GoogleMap App is not installed
            UIApplication.shared.openURL(NSURL(string:
                "https://www.google.co.in/maps/dir/?saddr=\(self.lat),\(self.long)&daddr=\(toLatitude),\(toLongitude)&directionsmode=driving")! as URL)
        }
    }
    
    func UpdateSentActionInCoredata(identifier: Int64,reciver_id: String,
                                    sender_id:String,
                                    message_id: Int64,
                                    notification_type: String,
                                    message: String,
                                    location:String,
                                    voice:String,
                                    image:String,
                                    time: String) {
        let fetchRequest  : NSFetchRequest<ChatEntityMo> = ChatEntityMo.fetchRequest()
        let predicate = NSPredicate(format: "message_id = '\(identifier)'")
        fetchRequest.predicate = predicate
        do
        {
            let object = try PresistanceServce.context.fetch(fetchRequest)
            if object.count == 1
            {
                
                let objectUpdate = object.first
                objectUpdate!.setValue(1, forKey: "messageSent")
                objectUpdate!.setValue(reciver_id, forKey: "reciver_id")
                objectUpdate!.setValue(sender_id, forKey: "sender_id")
                objectUpdate!.setValue(message_id, forKey: "message_id")
                objectUpdate!.setValue(notification_type, forKey: "notification_type")
                objectUpdate!.setValue(message, forKey: "message")
                objectUpdate!.setValue(location, forKey: "location")
                objectUpdate!.setValue(voice, forKey: "voice")
                objectUpdate!.setValue(image, forKey: "image")
                objectUpdate!.setValue(time, forKey: "time")
                
                
                PresistanceServce.saveContext()
                getMessagesFromDatabase()
            }
        }
        catch
        {
            print(error)
        }
    }
    
    
    func getTime() -> String {
        let timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .medium, timeStyle: .short)
        print(timestamp)
        return timestamp
    }
    
    func convertImage(from base64: String) -> UIImage {
        let dataDecoded:NSData = NSData(base64Encoded: base64, options: NSData.Base64DecodingOptions(rawValue: 0))!
        let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
        
        return decodedimage
    }
    
    func setStrinImage(toNSString image: UIImage) -> String {
        let data: Data? = image.jpegData(compressionQuality: 0.25)
        return data?.base64EncodedString(options: .endLineWithLineFeed) ?? ""
    }
    
    //MARK:- ACTIONS
    
    @IBAction func ChatOptions(_ sender: Any) {
        
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Picture", style: .default , handler:{ (UIAlertAction)in
            print("User click Picture")
            ImagePickerManager().pickImage(self){ image in
                //here is the image
                let param = ["google_id":"\(MyAppHandller.shared.Value(of: DEVICE_TOKEN))",
                    "method":"chat",
                    "message":"",
                    "sender_id":"\(SharedData.userId)",
                    "receiver_id":"\(SharedData.userId)",
                    "name":"iPhone Chat",
                    "type":"1",
                    "is_user":"1",
                    "image":"qqq.jpg",
                    "file_name":"qqq.jpg",
                    "file_data":"\(self.setStrinImage(toNSString: image))",
                    "voice":"",
                    "location":"",
                    "message_id":"\(self.lastMessageID() + 1)",
                    "notification_type":"7"]
                print(param["message_id"]!)
                self.sendMessage(param)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Currunt location", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            let curruntL: String = "\(self.lat)" + "|" + "\(self.long)" + "|0"
            print(curruntL)
            let param = ["google_id":"\(MyAppHandller.shared.Value(of: DEVICE_TOKEN))",
                "method":"chat",
                "message":"",
                "sender_id":"\(SharedData.userId)",
                "receiver_id":"\(SharedData.userId)",
                "name":"iPhone Chat",
                "type":"1",
                "is_user":"1",
                "image":"",
                "voice":"",
                "location":curruntL,
                "message_id":"\(self.lastMessageID() + 1)",
                "notification_type":"7"]
            print(param["message_id"]!)
            self.sendMessage(param)
        }))
        
        //        alert.addAction(UIAlertAction(title: "", style: .destructive , handler:{ (UIAlertAction)in
        //            print("User click Delete button")
        //        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @IBAction func sendAction(_ sender: Any) {
        self.view.endEditing(true)
        self.textViewConstraint.constant = 44
        if messageText.hasText {
            let param = ["google_id":"\(MyAppHandller.shared.Value(of: DEVICE_TOKEN))",
                "method":"chat",
                "message":"\(messageText.text!)",
                "sender_id":"\(SharedData.userId)",
                "receiver_id":"\(SharedData.userId)",
                "name":"iPhone Chat",
                "type":"1",
                "is_user":"1",
                "image":"",
                "voice":"",
                "location":"",
                "message_id":"\(lastMessageID() + 1)",
                "notification_type":"7"]
            print(param["message_id"]!)
            sendMessage(param)
        }
    }
    
    
    // MARK:- Request online
    func sendMessage(_ param: [String:String]) {
        // save offline
        saveMessage(reciver_id: "\(SharedData.userId)",
            sender_id: "\(SharedData.userId)",
            message_id: Int64(param["message_id"]!)!,
            notification_type: "\(param["notification_type"] ?? "")",
            message: "\(param["message"] ?? "")",
            location: "\(param["location"] ?? "")",
            voice: "\(param["voice"] ?? "")",
            image: "\(param["file_data"] ?? "")",
            time: "\(getTime())")
        self.messageText.text = ""
        getMessagesFromDatabase()
        // send online
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error != nil {
                
                self.sentSuccess(true, messageId: self.lastMessageID(),
                                 reciver_id: "\(SharedData.userId)",
                    sender_id: "\(SharedData.userId)",
                    message_id: Int64(param["message_id"]!)!,
                    notification_type: "\(param["notification_type"] ?? "")",
                    message:  "\(param["message"] ?? "")",
                    location:  "\(param["location"] ?? "")",
                    voice:  "\(param["voice"] ?? "")",
                    image:  "\(param["file_data"] ?? "")",
                    time: "\(self.getTime())")
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendAction(textField)
        return true
    }
    
    
    
    func textViewDidChange(_ textView: UITextView) {
        textView.didchange(constraints: textViewConstraint)
    }
}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : ChatTableViewCell!
        if let message = chatList[indexPath.row].message, message != "" {
            var identfier : String {
                if chatList[indexPath.row].sender_id == SharedData.userId {
                    return "messageUserCell"
                } else {
                    return "messageAdminCell"
                }
            }
            cell = tableView.dequeueReusableCell(withIdentifier: identfier, for: indexPath) as? ChatTableViewCell
            if chatList[indexPath.row].sender_id == SharedData.userId {
                if chatList[indexPath.row].messageSent == 0 {
                    cell.seenStatusImage.image = UIImage(named: "clock")
                } else if chatList[indexPath.row].messageSent == 1 {
                    cell.seenStatusImage.image = UIImage(named: "checked-2")
                }
                
            }
            cell.messageText.text = chatList[indexPath.row].message!
            cell.timeLable.text = chatList[indexPath.row].time!
            cell.selectionStyle = .none
            return cell
        }
        if let image = chatList[indexPath.row].image, image != "" {
            var identfier : String {
                if chatList[indexPath.row].sender_id == SharedData.userId {
                    return "imageUserCell"
                } else {
                    return "imageAdminCell"
                }
            }
            cell = tableView.dequeueReusableCell(withIdentifier: identfier, for: indexPath) as? ChatTableViewCell
            if chatList[indexPath.row].sender_id == SharedData.userId {
                if chatList[indexPath.row].messageSent == 0 {
                    cell.seenStatusImage.image = UIImage(named: "clock")
                } else if chatList[indexPath.row].messageSent == 1 {
                    cell.seenStatusImage.image = UIImage(named: "checked-2")
                }
                
            }
            cell.setImage(url: image, placeHolder: "myappIcon")
            cell.timeLable.text = chatList[indexPath.row].time!
            cell.selectionStyle = .none
            return cell
        }
        if let location = chatList[indexPath.row].location, location != "" {
            var identfier : String {
                if chatList[indexPath.row].sender_id == SharedData.userId {
                    return "locationUserCell"
                } else {
                    return "locationAdminCell"
                }
            }
            cell = tableView.dequeueReusableCell(withIdentifier: identfier, for: indexPath) as? ChatTableViewCell
            if chatList[indexPath.row].sender_id == SharedData.userId {
                if chatList[indexPath.row].messageSent == 0 {
                    cell.seenStatusImage.image = UIImage(named: "clock")
                } else if chatList[indexPath.row].messageSent == 1 {
                    cell.seenStatusImage.image = UIImage(named: "checked-2")
                }
                
            }
            cell.timeLable.text = chatList[indexPath.row].time!
            cell.selectionStyle = .none
            return cell
        }
        else {
            var identfier : String {
                if chatList[indexPath.row].sender_id == SharedData.userId {
                    return "messageUserCell"
                } else {
                    return "messageAdminCell"
                }
            }
            cell = tableView.dequeueReusableCell(withIdentifier: identfier, for: indexPath) as? ChatTableViewCell
            cell.messageText.text = ""
            cell.timeLable.text = chatList[indexPath.row].time!
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let image = chatList[indexPath.row].image, image != "" {
            let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ImagePopUpViewController") as! ImagePopUpViewController
            let cell = tableView.cellForRow(at: indexPath) as! ChatTableViewCell
            popUpVC.image = cell.messagePicture.image!
            self.addChild(popUpVC)
            self.view.addSubview(popUpVC.view)
            popUpVC.didMove(toParent: self)
        }
        if let location = chatList[indexPath.row].location, location != "" {
            let cordinates = location.components(separatedBy: "|")
            let lat = cordinates[0]
            let long = cordinates[1]
            NavigateToGoogleMap(toLatitude: lat, toLongitude: long)
        }
    }
    
}
