//
//  saidAboutUsViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/1/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class saidAboutUsViewController: UIViewController {
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var MainImage: UIImageView!
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    @IBOutlet weak var MainTitle: UILabel!
    @IBOutlet weak var saidText: UILabel!
    
    var SaidModel : SaidModelResponse?
    var fromSide : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        getData()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "Said about us".localized, backBtn: !fromSide, LargTitle: false)
    }
    
    //MARK:- Online Requests
    func getData() {
        let param = ["method":"said_about_us" ,
                     "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.SaidModel = try JSONDecoder().decode(SaidModelResponse.self, from: response.data!)
                    switch self.SaidModel!.state {
                    case ResponseState.sucess.rawValue :
                        self.setupView(at: 0)
                        self.sliderCollectionView.reloadData()
                    case ResponseState.empty.rawValue:
                        print("empty data")
                    case ResponseState.failed.rawValue:
                        print("Faild Request")
                    default:
                        print("No State in Reponse")
                    }
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
        }
    }
    
    //MARK:- helper Methods
    func setupView(at index: Int) {
        ImageTools.setImage(url: SaidModel!.data![index].logo!, imageView: MainImage, placeHolder: "myappIcon")
        MainTitle.text = SaidModel!.data![index].name!
        saidText.text = SaidModel!.data![index].note!
    }
}

extension saidAboutUsViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = SaidModel?.data?.count {
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: saidCVCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! saidCVCell
        
        ImageTools.setImage(url: SaidModel!.data![indexPath.row].logo!,
                            imageView: cell.saidImage, placeHolder: "myappIcon")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        setupView(at: indexPath.row)
    }
    
}

class saidCVCell : UICollectionViewCell {
    @IBOutlet weak var saidImage: UIImageView!
}
