//
//  SingleNotificationViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/15/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import ImageSlideshow

class SingleNotificationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var notificationData: NotificationData!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNibs()
    }
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "", backBtn: true, LargTitle: false, isSideButton: false)
    }
    
    func setupNibs() {
        var nibName = UINib()
        if notificationData.youtube != "" {
            nibName = UINib(nibName: "YoutubeTableViewCell", bundle: nil)
        } else {
            nibName = UINib(nibName: "SliderMainScreenTableViewCell", bundle: nil)
        }
        tableView.register(nibName, forCellReuseIdentifier: "sliderCell")
    }

}

extension SingleNotificationViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        if indexPath.row == 0 {
            if notificationData.youtube != "" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell", for: indexPath) as! YoutubeTableViewCell
                
                cell.PlayYoutubeVideo(videoCode: notificationData.youtube!)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell", for: indexPath) as! SliderMainScreenTableViewCell
                let image = KingfisherSource(urlString: notificationData.image!)!
                image.placeholder = #imageLiteral(resourceName: "myappIcon")
                cell.sliderShow.setImageInputs([image])
                return cell
            }
        }
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath) as! NotificationTableViewCell
            cell.NotTitle.text = notificationData.title!
            
            return cell
        }
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath) as! NotificationTableViewCell
            cell.NotDes.text = notificationData.details!
            return cell
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 305
        } else {
            return tableView.estimatedRowHeight
        }
    }
}
