
//  My App
//
//  Created by Kirollos Maged on 12/6/18.
//  Copyright © 2018 apple. All rights reserved.
//
import Foundation
struct MyAppSettingsData : Codable {
	let menu : [Menu]?
	let settings : [Settings]?
	let banners : [Banners]?

	enum CodingKeys: String, CodingKey {

		case menu = "menu"
		case settings = "settings"
		case banners = "banners"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		menu = try values.decodeIfPresent([Menu].self, forKey: .menu)
		settings = try values.decodeIfPresent([Settings].self, forKey: .settings)
		banners = try values.decodeIfPresent([Banners].self, forKey: .banners)
	}

}
