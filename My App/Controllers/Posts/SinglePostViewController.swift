//
//  SinglePostViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import ImageSlideshow
import CoreData
import KMPopUp

class SinglePostViewController: UIViewController {
    
    @IBOutlet weak var favBTN: UIButton!
    @IBOutlet weak var shareBTN: UIButton!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    var settings: Settings!
    var banners: MyAppSettingResponse!
    var sliderList = [Slider]()
    var PostID: String = "-1"
    var PostName: String = ""
    var fromSide: Bool = false
    var RandomAdNumber: Int!
    var ProductModel: ProductResponseModel?
    let fetchRequestFav  : NSFetchRequest<FavEntityMo> = FavEntityMo.fetchRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        banners = MyAppHandller.shared.getMyAppSettings()
        settings = MyAppSettings.data!.settings![0]
        SetupView()
        getProductData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: PostName, backBtn: !fromSide, LargTitle: false)
    }
    
    //MARK:- HelperMethods
    func SetupView() {
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        shareBTN.backgroundColor = UIColor(hexString: SharedData.AppColor)
        if AppDelegate.Static.isThereFavorate {
            if SharedData.IsFavorate(ID: PostID) {
                print("PostID \(PostID)")
                favBTN.tintColor = UIColor.white
            } else {
                favBTN.tintColor = UIColor.gray
            }
            favBTN.isHidden = false
        } else {
            favBTN.isHidden = true
        }
       
    }
    
    func setupNibs() {
        var nibName = UINib()
        if ProductModel?.data?.event?[0].kind == "3" {
            nibName = UINib(nibName: "YoutubeTableViewCell", bundle: nil)
        } else {
            nibName = UINib(nibName: "SliderMainScreenTableViewCell", bundle: nil)
        }
        tableView.register(nibName, forCellReuseIdentifier: "sliderCelll")
    }
    
    
    //MARK:- Request Online
    func getProductData() {
        if PostID != "-1" {
            let param = ["method":"event",
                         "id":"\(PostID)",
                "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
            print(param)
            indicator.startAnimating()
            APIManager.sharedInstance.postReques(Parameters: param) { (response) in
                if response.error == nil {
                    do {
                        self.ProductModel = try JSONDecoder().decode(ProductResponseModel.self, from: response.data!)
                        self.sliderList = self.ProductModel!.data!.slider!
                        guard let eventModName = self.ProductModel?.data?.event?[0].name else { return }
                        self.navigationItem.title = eventModName
                        self.setupNibs()
                        self.tableView.reloadData()
                    } catch {
                        MyAppHandller.shared.handleErrors(from: self, error: error)
                    }
                } else {
                    MyAppHandller.shared.handleErrors(from: self, error: response.error!)
                }
                self.indicator.stopAnimating()
            }
        } else {
            KMPopUp.ShowScreenPopUp(self, message: "BugReport".localized, image: "warning", withAlpha: 0.7)
        }
    }
    
    @IBAction func shareButton(_ sender: Any) {
        guard let details = ProductModel?.data?.event?[0].details else { return }
        let activityShare = UIActivityViewController(activityItems: ["\(PostName) \n \(details)\n Download for Android \n\n Download for IOS"], applicationActivities: nil)
        activityShare.popoverPresentationController?.sourceView = self.view
        
        self.present(activityShare, animated: true, completion: nil)
    }
    
    @IBAction func favBTNAction(_ sender: Any) {
        ActionInCoreData(fromFav: true)
    }
    
    func ActionInCoreData(fromFav: Bool) {
        let predicateID = NSPredicate(format: "id == %@", PostID)
        let predicateFav = NSPredicate(format: "idForFav == \(fromFav)")
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicateID,predicateFav])
        print(predicateCompound)
        fetchRequestFav.predicate = predicateCompound
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequestFav)
            if test.count > 0
            {
                PresistanceServce.context.delete(test[0])
                do {
                    try PresistanceServce.context.save() // <- remember to put this :)
                    if fromFav {
                        self.favBTN.tintColor = UIColor.gray
                    }
                } catch {
                    // Do something... fatalerror
                }
            } else {
                let subject = FavEntityMo(context: PresistanceServce.context)
                subject.id = PostID
                subject.idForFav = fromFav
                subject.cartQ = 1
                PresistanceServce.saveContext()
                if fromFav {
                    self.favBTN.tintColor = UIColor.white
                }
            }
        }
        catch
        {
            print(error)
        }
    }
    
}
