//
//  NativeNaveViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/11/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class NativeNaveViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let settingsModel = MyAppHandller.shared.getMyAppSettings()
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: settingsModel.data!.settings![0].menu_name!,backBtn: false)
    }
    
}
