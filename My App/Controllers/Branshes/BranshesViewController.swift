//
//  BranshesViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/8/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import GoogleMaps
import MapKit
import NVActivityIndicatorView
import KMPopUp

class BranshesViewController: UIViewController ,IndicatorInfoProvider ,GMSMapViewDelegate , CLLocationManagerDelegate{

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var MyMapView: UIView!
    var locManager = CLLocationManager()
    var lat: CLLocationDegrees!
    var long: CLLocationDegrees!
    var mapView : GMSMapView!
    var LoactionModel : BranshesModelResponse?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        getBranches()
        
    }
    

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Locations".localized)
    }
    
    //MARK:- Helper Methods
    func isAuthorizedtoGetUserLocation() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            locManager.requestWhenInUseAuthorization()
        }
    }
    
    func MapSetup(lat: String , long: String) {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!, zoom: 8)
        
        let f: CGRect = MyMapView.frame
        let mapFrame = CGRect(x: f.origin.x, y: f.origin.y, width: f.size.width, height: f.size.height)
        mapView = GMSMapView.map(withFrame: mapFrame, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.isMyLocationEnabled = true
        
        MyMapView.addSubview(mapView)
        mapView.delegate = self
        MapMarkerSetup()
    }
    
    func MapMarkerSetup() {
        if let count = LoactionModel?.data?.count {
            for index in 0..<count {
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(LoactionModel!.data![index].location_lat!)!, longitude: CLLocationDegrees(LoactionModel!.data![index].location_lng!)!)
                print(LoactionModel!.data![index].location_lng! , " : " , LoactionModel!.data![index].location_lng!)
                marker.title = LoactionModel!.data![index].title_en!
                marker.snippet = LoactionModel!.data![index].address_en!
//                marker.iconView?.tintColor = UIColor(hexString: SharedData.AppColor)
                
                let markerView = UIImageView(image: #imageLiteral(resourceName: "marker123"))
                markerView.tintColor = UIColor(hexString: SharedData.AppColor)
                marker.iconView = markerView
                
//                marker.icon = UIImage(named: "marker123")
                marker.map = mapView
            }
        }
        
    }
    
    //MARK:- Requests Online
    func getBranches() {
        let param = ["method":"branches" ,
                     "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.LoactionModel = try JSONDecoder().decode(BranshesModelResponse.self, from: response.data!)
                    switch self.LoactionModel!.state {
                    case ResponseState.sucess.rawValue :
                        if self.LoactionModel!.data!.count > 0 {
                            self.MapSetup(lat: String(self.LoactionModel!.data![0].location_lat!), long: String(self.LoactionModel!.data![0].location_lng!))
                        } else {
                            self.MapSetup(lat: "24.494001", long: "47.489755")
                        }
                        self.tableView.reloadData()
                    case ResponseState.empty.rawValue:
                        print("empty data")
                    case ResponseState.failed.rawValue:
                        print("Faild Request")
                    default:
                        print("No State in Reponse")
                    }
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
        }
    }

}

extension BranshesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = LoactionModel?.data?.count {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BranchesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BranchesTableViewCell
        if (indexPath.row + 1) % 2 == 0 {
            cell.backgroundColor = UIColor(hexString: "#EFEFF4")
        }
        cell.arrowImage.tintColor = UIColor(hexString: SharedData.AppColor)
        cell.branchName.text = LoactionModel!.data![indexPath.row].title_en!
        cell.branchAddress.text = LoactionModel!.data![indexPath.row].address_en!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NAVSingleBranchViewController") as! UINavigationController
        let MainVC = vc.topViewController as! SingleBranchViewController
        guard let bransdata = LoactionModel?.data?[indexPath.row] else { return }
        MainVC.branchData = bransdata
        vc.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
}
