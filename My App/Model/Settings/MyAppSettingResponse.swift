//  My App
//
//  Created by Kirollos Maged on 12/6/18.
//  Copyright © 2018 apple. All rights reserved.
//
import Foundation
struct MyAppSettingResponse : Codable {
    var state : String?
    var data : MyAppSettingsData?
    
    init() {
        self.state = "0"
        self.data = nil
    }
    
    enum CodingKeys: String, CodingKey {
        case state = "state"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        data = try values.decodeIfPresent(MyAppSettingsData.self, forKey: .data)
    }


}
