//
//  IconTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/17/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class IconTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    let settings = MyAppSettings.data!.settings![0]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        collectionView.backgroundColor = UIColor(hexString: "#\(settings.background_color_icon!)")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
