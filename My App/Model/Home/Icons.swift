

import Foundation
struct Icons : Codable {
	let title : String?
	let ext_url : String?
	let image : String?
	let type : String?
	let target_id : String?
	let kind : String?
    let style_type: String?

	enum CodingKeys: String, CodingKey {

		case title = "title"
		case ext_url = "ext_url"
		case image = "image"
		case type = "type"
		case target_id = "target_id"
		case kind = "kind"
        case style_type = "style_type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		ext_url = try values.decodeIfPresent(String.self, forKey: .ext_url)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		target_id = try values.decodeIfPresent(String.self, forKey: .target_id)
		kind = try values.decodeIfPresent(String.self, forKey: .kind)
        style_type = try values.decodeIfPresent(String.self, forKey: .style_type)
	}

}
