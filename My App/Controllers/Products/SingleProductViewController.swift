//
//  SingleProductViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/24/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import ImageSlideshow
import CoreData
import KMPopUp

class SingleProductViewController: UIViewController {
    
    @IBOutlet weak var favBTN: UIButton!
    @IBOutlet weak var CartBTN: UIButton!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var banners: MyAppSettingResponse!
    var settings: Settings!
    var sliderList = [Slider]()
    var ProductID: String = "-1"
    var ProductName: String = ""
    var fromSide: Bool = false
    var RandomAdNumber: Int!
    var ProductModel: ProductResponseModel?
    let fetchRequestCart  : NSFetchRequest<CartEntityMo> = CartEntityMo.fetchRequest()
    let fetchRequestFav  : NSFetchRequest<FavEntityMo> = FavEntityMo.fetchRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateCartCount), name: Notification.Name("updateCartCount"), object: nil)
        banners = MyAppHandller.shared.getMyAppSettings()
        settings = MyAppSettings.data!.settings![0]
        SetupView()
        getProductData()
    }
    
    @objc func updateCartCount() {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: ProductName, backBtn: !fromSide, LargTitle: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: ProductName, backBtn: !fromSide, LargTitle: false)
    }
    
    //MARK:- HelperMethods
    func SetupView() {
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        CartBTN.backgroundColor = UIColor(hexString: SharedData.AppColor)
        let nibName = UINib(nibName: "SliderMainScreenTableViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "sliderCell")
        if AppDelegate.Static.isThereFavorate {
            if SharedData.IsFavorate(ID: ProductID) {
                favBTN.tintColor = UIColor.white
            } else {
                favBTN.tintColor = UIColor.gray
            }
            favBTN.isHidden = false
        } else {
            favBTN.isHidden = true
        }
    }
    
    //MARK:- Request Online
    func getProductData() {
        if ProductID != "-1" {
            let param = ["method":"event",
                         "id":"\(ProductID)",
                "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
            print(param)
            indicator.startAnimating()
            APIManager.sharedInstance.postReques(Parameters: param) { (response) in
                if response.error == nil {
                    do {
                        self.ProductModel = try JSONDecoder().decode(ProductResponseModel.self, from: response.data!)
                        self.sliderList = self.ProductModel!.data!.slider!
                        guard let eventModName = self.ProductModel?.data?.event?[0].name else { return }
                        self.navigationItem.title = eventModName
                        self.tableView.reloadData()
                    } catch {
                        MyAppHandller.shared.handleErrors(from: self, error: error)
                    }
                } else {
                    MyAppHandller.shared.handleErrors(from: self, error: response.error!)
                }
                self.indicator.stopAnimating()
            }
        } else {
            KMPopUp.ShowScreenPopUp(self, message: "BugReport".localized, image: "warning", withAlpha: 0.7)
        }
    }
    
    //MARK:- Actions
    
    @IBAction func cartAction(_ sender: Any) {
        if ProductModel?.data?.event?[0].sold == "0" {
            ActionInCoreDataCart(fromFav: false)
        } else if ProductModel?.data?.event?[0].sold == "1" {
            KMPopUp.ShowMessage(self, message: "Out of stock".localized, image: "warning", withAlpha: 0.7)
        }
    }
    
    @IBAction func FavBTNAction(_ sender: Any) {
        ActionInCoreDataFav(fromFav: true)
    }
    
    
    @IBAction func shareBtnPressed(_ sender: UIButton) {
        var items : [Any] = ["name: \(ProductName)","price: \(ProductModel?.data?.event?[0].price_after_disc ?? "")"]
        if let url = URL(string: ProductModel?.data?.event?[0].post_url ?? "") {
            items.append(url)
        }
        
            let vc = UIActivityViewController(activityItems: items, applicationActivities: [])
            vc.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
            present(vc, animated: true)
        }
    
    func ActionInCoreDataCart(fromFav: Bool) {
        let predicateID = NSPredicate(format: "id == %@", ProductID)
        let predicateFav = NSPredicate(format: "idForFav == \(fromFav)")
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicateID,predicateFav])
        print(predicateCompound)
        fetchRequestCart.predicate = predicateCompound
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequestCart)
            if test.count > 0
            {
                PresistanceServce.context.delete(test[0])
                do {
                    try PresistanceServce.context.save() // <- remember to put this :)
                    if fromFav {
                        self.favBTN.tintColor = UIColor.gray
                    }
                } catch {
                    // Do something... fatalerror
                }
            } else {
                let subject = CartEntityMo(context: PresistanceServce.context)
                subject.id = ProductID
                subject.idForFav = fromFav
                subject.cartQ = 1
                PresistanceServce.saveContext()
                if fromFav {
                    self.favBTN.tintColor = UIColor.white
                } else {
                    KMPopUp.ShowMessageWithDuration(self, message: "", image: "empty-cart", duration: 2.0, withAlpha: 0.7)
                }
            }
            NotificationCenter.default.post(name: Notification.Name("updateCartCount"), object: nil)
        }
        catch
        {
            print(error)
        }
    }
    
    func ActionInCoreDataFav(fromFav: Bool) {
        let predicateID = NSPredicate(format: "id == %@", ProductID)
        let predicateFav = NSPredicate(format: "idForFav == \(fromFav)")
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicateID,predicateFav])
        print(predicateCompound)
        fetchRequestFav.predicate = predicateCompound
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequestFav)
            if test.count > 0
            {
                PresistanceServce.context.delete(test[0])
                do {
                    try PresistanceServce.context.save() // <- remember to put this :)
                    if fromFav {
                        self.favBTN.tintColor = UIColor.gray
                    }
                } catch {
                    // Do something... fatalerror
                }
            } else {
                let subject = FavEntityMo(context: PresistanceServce.context)
                subject.id = ProductID
                subject.idForFav = fromFav
                subject.cartQ = 1
                PresistanceServce.saveContext()
                if fromFav {
                    self.favBTN.tintColor = UIColor.white
                } else {
                    KMPopUp.ShowMessageWithDuration(self, message: "", image: "empty-cart", duration: 2.0, withAlpha: 0.7)
                }
            }
        }
        catch
        {
            print(error)
        }
    }
    
}



