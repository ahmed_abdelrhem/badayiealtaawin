//  My App
//
//  Created by Kirollos Maged on 12/6/18.
//  Copyright © 2018 apple. All rights reserved.
//
import Foundation
struct Banners : Codable {
    let id : String?
    let title : String?
    let details : String?
    let icon : String?
    let link : String?
    let type : String?
    let type_id : String?
    let style_type : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case title = "title"
        case details = "details"
        case icon = "icon"
        case link = "link"
        case type = "type"
        case type_id = "type_id"
        case style_type = "style_type"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        details = try values.decodeIfPresent(String.self, forKey: .details)
        icon = try values.decodeIfPresent(String.self, forKey: .icon)
        link = try values.decodeIfPresent(String.self, forKey: .link)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        type_id = try values.decodeIfPresent(String.self, forKey: .type_id)
        style_type = try values.decodeIfPresent(String.self, forKey: .style_type)
    }
    
    static func setBannerObject(settingObject:[Banners]){
        SharedData.setObjectData(anyObject: settingObject, key: "Banner")
    }
    
    static func getBannerObject() -> [Banners]{
        if let myBanner = SharedData.getObjectData(key: "Banner") as? Banners {
            return [myBanner]
        }
        return []
    }
    
}
