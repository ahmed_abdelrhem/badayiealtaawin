//
//  AllSingleBranchTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 1/14/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class AllSingleBranchTableViewCell: UITableViewCell {

    @IBOutlet weak var branceName: UILabel!
    @IBOutlet weak var branchAddress: UILabel!
    @IBOutlet weak var BranchPhone: UILabel!
    @IBOutlet weak var BranchMobile: UILabel!
    @IBOutlet weak var BranchStart: UILabel!
    @IBOutlet weak var BranchEnd: UILabel!
    @IBOutlet weak var whatsApp1: UIButton!
    @IBOutlet weak var whatsApp2: UIButton!
    @IBOutlet weak var phone1: UIButton!
    @IBOutlet weak var phone2: UIButton!
    
    var delegate : SingleBranchViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func whatsappBTN(_ sender: UIButton) {
        delegate?.whatsAppCall(sender: sender)
    }
    
    @IBAction func phoneCallBTN(_ sender: UIButton) {
        delegate?.phoneCall(sender: sender)
    }
    
    
}
