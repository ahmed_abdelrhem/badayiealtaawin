//
//  TeamMemberCollectionViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/27/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class TeamMemberCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var memberImage: RoundImage!
    @IBOutlet weak var memberName: UILabel!
    @IBOutlet weak var memberDetails: UILabel!
}
