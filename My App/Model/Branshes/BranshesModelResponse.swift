//
//  BranshesModelResponse.swift
//  My App
//
//  Created by Kirollos Maged on 1/8/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
struct BranshesModelResponse : Codable {
    let state : String?
    let data : [BransheData]?
    
    enum CodingKeys: String, CodingKey {
        case state = "state"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        data = try values.decodeIfPresent([BransheData].self, forKey: .data)
    }
    
}

