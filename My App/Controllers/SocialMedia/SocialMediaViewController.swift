//
//  SocialMediaViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/30/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import KMPopUp


class SocialMediaViewController: UIViewController {
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var type_style: String = "0" // Screen Style
    var type_id: String = "0" // contecntID
    var settings: Settings!
    var fromSide: Bool = false
    var socialModel: SocialMediaModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settings = MyAppSettings.data!.settings![0]
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        tableView.alpha = 0
        getTeamData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "Social Media".localized, backBtn: !fromSide, LargTitle: false)
    }
    
    
    //MARK:- Request Online
    func getTeamData() {
        let param = ["method":"social_media_items" ,
                     "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.socialModel = try JSONDecoder().decode(SocialMediaModel.self, from: response.data!)
                    switch self.socialModel!.state {
                    case ResponseState.sucess.rawValue :
                        print(self.socialModel?.data?.count)
                        self.tableView.fadeIn()
                        self.tableView.reloadData()
                    case ResponseState.empty.rawValue:
                        print("empty data")
                    case ResponseState.failed.rawValue:
                        print("Faild Request")
                    default:
                        print("No State in Reponse")
                    }
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
        }
        
    }
    
    
}

extension SocialMediaViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = socialModel?.data?.count {
            return count + 1  // +1 for header + list of social
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = SocialMediaTableViewCell()
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "cell0", for: indexPath) as! SocialMediaTableViewCell
            ImageTools.setImage(url: settings.social_header!, imageView: cell.HeaderImage, placeHolder: "myappIcon")
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! SocialMediaTableViewCell
            cell.socialName.text = socialModel!.data![indexPath.row - 1].title!
            cell.socialurl.text = socialModel!.data![indexPath.row - 1].link!
            ImageTools.setImage(url: socialModel!.data![indexPath.row - 1].icon!, imageView: cell.socialImage, placeHolder: "myappIcon")
        }
        cell.selectionStyle = .blue
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row != 0 {
            if socialModel?.data?[indexPath.row - 1].kind_link == 1 { // go to link
                if let url = URL(string: "\(socialModel!.data![indexPath.row - 1].link!)") {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            }
            else if socialModel?.data?[indexPath.row - 1].kind_link ==  2 { // make call
                if let phoneNum = socialModel?.data?[indexPath.row - 1].link {
                    let url:NSURL = NSURL(string: "tel://\(phoneNum)")!
                    if UIApplication.shared.canOpenURL(url as URL) {
                        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                    }
                }
            }
            else if socialModel?.data?[indexPath.row - 1].kind_link ==  3 { // go to whats app
                
                if let phoneNum = socialModel?.data?[indexPath.row - 1].link
                {
                    let msg = "Hello :"
                    let urlWhats = "whatsapp://send?phone=\(phoneNum)&text=\(msg)"
                    if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                        if let whatsappURL = NSURL(string: urlString) {
                            if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                                UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                            } else {
                                KMPopUp.ShowMessage(self, message: "whatsAppError".localized, image: "warning")
                            }
                        }
                    }
                }
            }
        }
    }
}
