//
//  URLHandller.swift
//  My App
//
//  Created by Kirollos Maged on 2/4/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation

enum MyAppUrl: String {
    case badayiealtaawin = "http://badayiealtaawin.com"
}

enum MyAppMapKey: String {
    case MyAppKey = "AIzaSyCVLys5ki64X4I1t9ExJcCDA9McIIrhLAE"
}

struct URLHandller {
    static let WS_Endpoint = "/web_service/go_ios.php"
    static let youtubeBaseUrl = "https://www.youtube.com/embed/"
    static let MapKey = MyAppMapKey.MyAppKey.rawValue
    //MARK:stagingURL
    //TODO: add here your new enum MyAppUrl
    static let stagingURL: MyAppUrl = .badayiealtaawin
    static let baseURL = "\(stagingURL.rawValue)\(WS_Endpoint)"
}
