//
//  APIManager.swift
//  My App
//
//  Created by Kirollos Maged on 12/9/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import Alamofire
import MOLH


class APIManager {
    //MARK: Singlton
    class var sharedInstance : APIManager {
        struct Singlton {
            static let instance = APIManager()
        }
        return Singlton.instance
    }
    
    // MARK: general requests
    func getReques(_ url : String ,completionHandler :@escaping (AFDataResponse<Any>) -> Void) {
        let MyUrl = URLHandller.baseURL + url
        print(">>Url",MyUrl)
        print("Header",["lang":"\(MOLHLanguage.currentAppleLanguage())"])
        _ = AF.request(MyUrl,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["lang":"\(MOLHLanguage.currentAppleLanguage())"]).responseJSON { response in

    
            completionHandler(response)
        }
    }
    
    func postReques(Parameters : [String: Any],completionHandler :@escaping (AFDataResponse<Any>) -> Void) {
        let MyUrl = URLHandller.baseURL
        print(">>Url",MyUrl)
        print(">>Parameters",Parameters)
        print("Header",["lang":"\(MOLHLanguage.currentAppleLanguage())"])
        _ = AF.request(MyUrl,method: .post, parameters: Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            print("My Response ==>>",response)
            completionHandler(response)
        }
    }
}
