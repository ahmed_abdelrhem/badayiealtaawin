//
//  ImagePopUpViewController.swift
//  My App
//
//  Created by Kirollos Maged on 3/24/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ImagePopUpViewController: UIViewController {
    
    @IBOutlet weak var popUpImage: UIImageView!
    var image: UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addTapGesture(tapNumber: 1, target: self, action: #selector(TabAction))
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        popUpImage.image = image
        showAnimate()
    }
    
    @objc func TabAction() {
        removeAnimation()
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }

}
