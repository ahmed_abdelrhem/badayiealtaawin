//
//  CustomProductTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/23/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import CoreData

class CustomProductTableViewCell: UITableViewCell {

    @IBOutlet weak var ProductImage: UIImageView!
    @IBOutlet weak var cartBTN: UIButton!
    @IBOutlet weak var ProuductName: UILabel!
    @IBOutlet weak var favBTN: UIButton!
    @IBOutlet weak var DiscountPriceView: UIView!
    @IBOutlet weak var DiscountPrice: UILabel!
    @IBOutlet weak var PriceView: UIView!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var DiscountLine: UIView!
    @IBOutlet weak var SoldProduct: UIView!
    @IBOutlet weak var ProductDescription: UILabel!     // Only used in panorama View
    @IBOutlet weak var PriceBackground: UIView!         // Only used in galary View
    var ProductID = "-1"
    let fetchRequestCart  : NSFetchRequest<CartEntityMo> = CartEntityMo.fetchRequest()
    let fetchRequestFav  : NSFetchRequest<FavEntityMo> = FavEntityMo.fetchRequest()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        SetupCell()
    }
    
    func SetupCell() {
//        cartBTN.tintColor = UIColor(hexString: SharedData.AppColor)
        Price.textColor = UIColor(hexString: SharedData.AppColor)
        DiscountPrice.textColor = UIColor(hexString: SharedData.AppColor)
        DiscountLine.backgroundColor = UIColor(hexString: SharedData.AppColor)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func addToCart(_ sender: Any) {
        ActionInCoreDataCart(fromFav: false)
    }
    @IBAction func favAction(_ sender: Any) {
        ActionInCoreDataFav(fromFav: true)
    }
    
    func ActionInCoreDataCart(fromFav: Bool) {
        let predicateID = NSPredicate(format: "id == %@", ProductID)
        let predicateFav = NSPredicate(format: "idForFav == \(fromFav)")
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicateID,predicateFav])
        print(predicateCompound)
        fetchRequestCart.predicate = predicateCompound
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequestCart)
            if test.count > 0
            {
                PresistanceServce.context.delete(test[0])
                do {
                    try PresistanceServce.context.save() // <- remember to put this :)
                    if fromFav {
                        self.favBTN.tintColor = UIColor(hexString: "#CCCCCC")
                    } else {
                        self.cartBTN.tintColor = UIColor(hexString: "#CCCCCC")
                    }
                    
                } catch {
                    // Do something... fatalerror
                }
            } else {
                let subject = CartEntityMo(context: PresistanceServce.context)
                subject.id = ProductID
                subject.idForFav = fromFav
                subject.cartQ = 1
                PresistanceServce.saveContext()
                if fromFav {
                    self.favBTN.tintColor = UIColor(hexString: SharedData.AppColor)
                } else {
                    self.cartBTN.tintColor  = UIColor(hexString: SharedData.AppColor)
                }
            }
            NotificationCenter.default.post(name: Notification.Name("updateCartCount"), object: nil)
        }
        catch
        {
            print(error)
        }
    }
    
    func ActionInCoreDataFav(fromFav: Bool) {
        let predicateID = NSPredicate(format: "id == %@", ProductID)
        let predicateFav = NSPredicate(format: "idForFav == \(fromFav)")
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicateID,predicateFav])
        print(predicateCompound)
        fetchRequestFav.predicate = predicateCompound
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequestFav)
            if test.count > 0
            {
                PresistanceServce.context.delete(test[0])
                do {
                    try PresistanceServce.context.save() // <- remember to put this :)
                    if fromFav {
                        self.favBTN.tintColor = UIColor(hexString: "#CCCCCC")
                    } else {
                        self.cartBTN.tintColor = UIColor(hexString: "#CCCCCC")
                    }
                    
                } catch {
                    // Do something... fatalerror
                }
            } else {
                let subject = FavEntityMo(context: PresistanceServce.context)
                subject.id = ProductID
                subject.idForFav = fromFav
                subject.cartQ = 1
                PresistanceServce.saveContext()
                if fromFav {
                    self.favBTN.tintColor = UIColor(hexString: SharedData.AppColor)
                } else {
                    self.cartBTN.tintColor  = UIColor(hexString: SharedData.AppColor)
                }
            }
        }
        catch
        {
            print(error)
        }
    }
}
