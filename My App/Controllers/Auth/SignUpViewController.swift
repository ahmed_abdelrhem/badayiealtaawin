//
//  SignUpViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/28/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import KMPopUp
import SwiftyJSON

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var ProfileImageBTN: UIButton!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var mailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var confirmPassText: UITextField!
    
    @IBOutlet weak var signUp: UIButton!
    
    var settings : Settings!
    var RequestWithImage : Bool = false
    var logo_data : UIImage!
    var VerificationCode : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //MARK:- Helper Methods
    func setupView() {
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        settings = MyAppSettings.data!.settings![0]
        // as per Mohamed Atef
        if settings.register_mobile_active == "1" {
            mailText.isHidden = true
        } else if settings.register_mobile_active == "0" {
            mailText.isHidden = false
        }
        signUp.backgroundColor = UIColor(hexString: SharedData.AppColor)
    }
    
    func setStrinImage(toNSString image: UIImage) -> String {
        let data: Data? = image.jpegData(compressionQuality: 0.7)
        return data?.base64EncodedString(options: .endLineWithLineFeed) ?? ""
    }
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
    
    @IBAction func ProfileImage(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func signupBTN(_ sender: Any) {
        
        var txtFArray: [UITextField] = [nameText,phoneText,passwordText,confirmPassText]
        if settings.register_mobile_active == "0" {
            txtFArray.append(mailText)
        }
        if MyAppHandller.shared.isValidAction(textFeilds: txtFArray) {
                if UITextField.compare_confirm_pass(testStr: passwordText, passconfirm: confirmPassText) {
                    var ActivationStatus: String  {
                        if settings.email_activation == "1" {
                            return "0"
                        } else if settings.email_activation == "0" {
                            return "1"
                        }
                        return "0"
                    }
                    let imageName = "\(randomString(length: 16))"
                    var param = ["method":"register" ,
                                 "name":"\(nameText.text!)" ,
                        "phone":"\(phoneText.text!)" ,
                        "password":"\(passwordText.text!)",
                        "device_id":"\(DEVICE_ID)" ,
                        "active":"\(ActivationStatus)" ,
                        "google_id":"\(MyAppHandller.shared.Value(of: DEVICE_TOKEN))" ] // as per Mohamed Atef
                    if settings.register_mobile_active == "0" {
                        param["email"] = "\(mailText.text!)"
                    }
                    
                    if RequestWithImage{
                        param["file_data"] = "\(setStrinImage(toNSString: logo_data))"
                        param["file_name"] = "\(imageName)"
                        param["photo"] = "\(imageName)"
                    }
                    
                    print(param)
                    if settings.email_activation == "1" {
                        sendCode(registrationParameters: param)
                    } else {
                        Register(param, loginType: APP_LOGIN)
                    }
                } else {
                    KMPopUp.ShowMessage(self, message: PASSWORD_ERROR_MESSAGE, image: "warning", withAlpha: 0.7)
                }
          
        } else {
            KMPopUp.ShowMessage(self, message: COMPLETE_DATA_ERROR, image: "warning", withAlpha: 0.7)
        }
    }
    
    //MARK:- Requests Online
    func sendCode(registrationParameters: [String:Any]) {
        VerificationCode = MyAppHandller.shared.generateRandomDigits(6)
        if !mailText.isHidden {
            let param = ["method":"send_code",
                         "code":"\(VerificationCode)",
                "mail":"\(mailText.text!)"]
            APIManager.sharedInstance.postReques(Parameters: param) { (res) in
                if res.error == nil {
                    let json = JSON(res.value!)
                    guard let state = json["state"].string else { return }
                    if state == ResponseState.sucess.rawValue {
                        let alertController = UIAlertController(title: "Enter Code", message: "code was sent to your e-mail please check your mail and enter code here.".localized, preferredStyle: .alert)
                        
                        alertController.addTextField { (textField : UITextField!) -> Void in
                            textField.placeholder = "Verification code"
                        }
                        let saveAction = UIAlertAction(title: "Send", style: .default, handler: { alert -> Void in
                            let codeText = alertController.textFields![0] as UITextField
                            if MyAppHandller.shared.isValidAction(textFeilds: [codeText]) {
                                print(codeText.text!)
                                if codeText.text == self.VerificationCode {
                                    self.Register(registrationParameters, loginType: APP_LOGIN)
                                }
                            }
                            
                        })
                        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                            (action : UIAlertAction!) -> Void in })
                        
                        alertController.addAction(saveAction)
                        alertController.addAction(cancelAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    } else if state == ResponseState.failed.rawValue {
                        KMPopUp.ShowMessage(self, message: "", image: "warning", withAlpha: 0.7)
                    }
                    
                } else {
                    MyAppHandller.shared.handleErrors(from: self, error: res.error!)
                }
            }
        } else {
            KMPopUp.ShowMessage(self, message: "BugReport".localized, image: "warning", withAlpha: 0.7)
        }
    }
    
    func Register(_ param: [String:Any],loginType: String) {
        print("#Register")
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                let json = JSON(response.value!)
                guard let status = json["state"].string else { return }
                if status == ResponseState.sucess.rawValue {
                    guard let data = json["data"].array else { return }
                    guard let user = data[0].dictionary else {return}
                    print("#user ==>>",user)
                    guard let id = user["id"]?.int else { print("#1 ==>>")
                        return }
                    guard let name = user["name"]?.string else { print("#2 ==>>")
                        return }
                    guard let email = user["email"]?.string else { print("#3 ==>>")
                        return }
                    guard let phone = user["phone"]?.string else { print("#4 ==>>")
                        return }
                    guard let active = user["active"]?.int else { print("#5 ==>>")
                        return }
                


                    MyAppHandller.shared.setValue(loginType, forKey: LOGIN_STATE)
                    UserDefaults.standard.setValue(String(id), forKey: "DEFuserid")
                    UserDefaults.standard.setValue(email, forKey: "DEFemail")
                    UserDefaults.standard.set(name, forKey: "DEFname")
                    UserDefaults.standard.set(phone, forKey: "DEFuserphone")
                    UserDefaults.standard.set(active, forKey: "DEFuseractive")
                    UserDefaults.standard.setValue(id, forKey: "DEFuserid")


                    
                    NotificationCenter.default.post(name: LOGIN_NOTIFCATION, object: nil)
                }  else if status == ResponseState.failed.rawValue {
                    KMPopUp.ShowMessage(self, message: GENERAL_UNKHOWN_ERROR, image: "warning")
                } else if status == ResponseState.used_mail.rawValue {
                    KMPopUp.ShowMessage(self, message: "this E-mail is already used", image: "warning")
                } else if status == ResponseState.wrong_mail.rawValue {
                    KMPopUp.ShowMessage(self, message: "Phone or e-mail not found", image: "warning")
                }
            } else {
                KMPopUp.ShowMessage(self, message: response.error!.localizedDescription, image: "warning")
            }
            self.indicator.stopAnimating()
        }
    }
    
    
}

extension SignUpViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        logo_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        ProfileImageBTN.setImage(logo_data, for: .normal)
        RequestWithImage = true
        dismiss(animated: true, completion: nil)
        
    }
    
}

