//
//  SideMenuViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/11/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import ExpyTableView
import MOLH

class SideMenuViewController: UIViewController ,ExpyTableViewDelegate ,ExpyTableViewDataSource{
    //MARK:- Outlets
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var MenuTableView: ExpyTableView!
    // header Objects
    @IBOutlet weak var HeaderMainView: UIView!
    @IBOutlet weak var HeaderMainImage: UIImageView!
    @IBOutlet weak var HeaderIcon: UIImageView!
    @IBOutlet weak var HeaderText: UILabel!
    @IBOutlet weak var grandImage: UIImageView!
    @IBOutlet weak var grandViewHeight: NSLayoutConstraint!
    
    
    //MARK:- SideMenu Properties
    var MenuTopBgColor: String?     // color for HeaderMainView
    var MenuTopTextColor: String?   // color HeaderText
    var ArrowColor: String?         // color for expand arrow
    var MenuTextColor: String?      // color for cells text
    var MenuLineColor: String?      // color for separator
    
    var MyAppModel = MyAppHandller.shared.getMyAppSettings()   // setting model used in all controllers in MyApp with the same singletone access
    var menuList = [Menu]()
    var List = [Section]()
    var log = ""
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewConfigurations()
        MappingMenuListToSections()
        ViewSetup()
        setupHeader()
    }
    
    //MARK:- helper Methods
    
    // general Configurations
    func viewConfigurations() {
        MenuTableView.dataSource = self
        MenuTableView.delegate = self
        
        MenuTableView.rowHeight = UITableView.automaticDimension
        MenuTableView.estimatedRowHeight = 44
        menuList = MyAppModel.data!.menu!
        //Alter the animations as you want
        MenuTableView.expandingAnimation = .middle
        MenuTableView.collapsingAnimation = .middle
    }
    
    //Method for filter menulist from list to sections
    func MappingMenuListToSections() {
        for index in 0..<menuList.count {
            if menuList[index].menu_id == "0" {
                let filtered = menuList.filter( { return $0.menu_id == menuList[index].id! } ) // return array of sub cells when we have expand cell
                self.List.append(Section(genre: menuList[index].name!,
                                         category: filtered,
                                         expanded: false,
                                         id: menuList[index].id!,
                                         name: menuList[index].name!,
                                         type: menuList[index].type!,
                                         type_id: menuList[index].type_id!,
                                         icon: menuList[index].icon!,
                                         style_type: menuList[index].style_type!,
                                         menu_id: menuList[index].menu_id!))
            }
        }
    }
    
    //Method for setup MyApp side Menu colors and Main View
    func ViewSetup() {
        guard let settings = MyAppModel.data?.settings?[0] else { return }
        let menuBgColor = settings.menu_bgcolor!
        MainView.backgroundColor = UIColor(hexString: "#" + menuBgColor)
        if let menuTextColor = settings.menu_text_color {
            if settings.copyright == "1" {
                grandViewHeight.constant = 40.0
                grandImage.tintColor = UIColor(hexString: "#" + menuTextColor)
            } else if settings.copyright == "0" {
                grandViewHeight.constant = 0.0
            }
        }
        
        // TODO: offline cell alwayes = -1
        // passing id = -1 for any offline new cell
        // image read from offline url only when check if id == -1 -- used for login cell for example
        if settings.members != "0" || settings.member_admin != "0" { // check if we have login cell or nop
            // Login Cell
            if MyAppHandller.shared.appLogin() {
                var profile = "Profile"
                if MOLHLanguage.isRTLLanguage() {
                    log = "تسجيل خروج"
                    profile = "حسابي"
                }else {
                    log = "Logout"
                    profile = "profile"
                }
                self.List.insert(Section(genre: "Profile", category: [], expanded: false, id: "-1", name: profile, type: "201", type_id: "201", icon: "Profile", style_type: "201", menu_id: "201"), at: List.count - 1)
//            self.List.append()
            
            } else {
                if MOLHLanguage.isRTLLanguage() {
                    log = "تسجيل دخول"
                }else {
                    log = "Login"
                }
            }
            
            self.List.append(Section(genre: "Login", category: [], expanded: false, id: "-1", name: log, type: "100", type_id: "100", icon: "login", style_type: "100", menu_id: "100"))
            
            NotificationCenter.default.addObserver(forName: LOGIN_NOTIFCATION, object: nil, queue: nil) { notification in
                if MyAppHandller.shared.appLogin() {
                    var profile = "Profile"
                    if MOLHLanguage.isRTLLanguage() {
                        self.log = "تسجيل خروج"
                        profile = "حسابي"
                    }else {
                        self.log = "Logout"
                        profile = "profile"
                    }
                    self.List.insert(Section(genre: "Profile", category: [], expanded: false, id: "-1", name: profile, type: "201", type_id: "201", icon: "Profile", style_type: "201", menu_id: "201"), at: self.List.count - 1)

                    
                } else {
                    if MOLHLanguage.isRTLLanguage() {
                        self.log = "تسجيل دخول"
                    } else {
                        self.log = "Login"
                    }
                }
                for index in 0..<self.List.count {     // to reload side table view
                    if self.List[index].type == "100" {
                        self.List[index] = Section(genre: "Login", category: [], expanded: false, id: "-1", name: self.log, type: "201", type_id: "201", icon: "login", style_type: "201", menu_id: "201")
                    }
                }
                self.MenuTableView.reloadData()
            }
        }
    }
    
   
    
    func setupHeader() {
        guard let settings = MyAppModel.data?.settings?[0] else { return }
       


        
        HeaderMainView.backgroundColor = UIColor(hexString: "#\(settings.menutop_bgcolor!)")
        if settings.menutop_logo_check == "0" {
            HeaderText.isHidden = true
            HeaderIcon.isHidden = true
        } else {
            HeaderText.isHidden = false
            HeaderIcon.isHidden = false
        }
        HeaderText.textColor = UIColor(hexString: "#\(settings.menutop_text_color!)")
        HeaderText.text = settings.menu_name!
        print(settings.menutop_logo!)
        print(settings.menutop_image!)
        ImageTools.setImage(url: settings.menutop_logo!, imageView: HeaderIcon, placeHolder: "myappIcon")
        if settings.menutop_image_check == "1" {
             ImageTools.setImage(url: settings.menutop_image!, imageView: HeaderMainImage, placeHolder: "myappIcon")
        }
    }
    
    //MARK:- Actions
  
    //MARK: By Grand Action
    
    @IBAction func ByGrand(_ sender: Any) {
        let popup : ByGrandViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ByGrandViewController") as! ByGrandViewController
        self.presentOnRoot(with: popup)
    }
}

