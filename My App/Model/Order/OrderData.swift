//
//  OrderData.swift
//  My App
//
//  Created by apple on 12/28/20.
//  Copyright © 2020 apple. All rights reserved.
//

struct OrderData : Codable {
    
    let id : Int?
    let total_price : Int?
    let created : String?
    let current_case : String?
    let comments : String?
    let items : String?
    let count : String?
    let status_name : String?
    let status_color : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case total_price = "total_price"
        case created = "created"
        case current_case = "current_case"
        case comments = "comments"
        case items = "items"
        case count = "count"
        case status_name = "status_name"
        case status_color = "status_color"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        total_price = try values.decodeIfPresent(Int.self, forKey: .total_price)
        created = try values.decodeIfPresent(String.self, forKey: .created)
        current_case = try values.decodeIfPresent(String.self, forKey: .current_case)
        comments = try values.decodeIfPresent(String.self, forKey: .comments)
        items = try values.decodeIfPresent(String.self, forKey: .items)
        count = try values.decodeIfPresent(String.self, forKey: .count)
        status_name = try values.decodeIfPresent(String.self, forKey: .status_name)
        status_color = try values.decodeIfPresent(String.self, forKey: .status_color)
    }

}
