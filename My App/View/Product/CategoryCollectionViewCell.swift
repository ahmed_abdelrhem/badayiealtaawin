//
//  CategoryCollectionViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
        
        @IBOutlet weak var relatedProductImage: UIImageView!
        
        @IBOutlet weak var relatedProductName: UILabel!
 
}
