

import Foundation
struct NotificationData : Codable {
	let id : Int?
	let title : String?
	let details : String?
	let image : String?
	let created : String?
	let youtube : String?
	let voice : String?
	let type : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case title = "title"
		case details = "details"
		case image = "image"
		case created = "created"
		case youtube = "youtube"
		case voice = "voice"
		case type = "type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		details = try values.decodeIfPresent(String.self, forKey: .details)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		created = try values.decodeIfPresent(String.self, forKey: .created)
		youtube = try values.decodeIfPresent(String.self, forKey: .youtube)
		voice = try values.decodeIfPresent(String.self, forKey: .voice)
		type = try values.decodeIfPresent(Int.self, forKey: .type)
	}

}

