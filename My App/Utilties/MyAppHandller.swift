//
//  SharedHandller.swift
//  My App
//
//  Created by Kirollos Maged on 12/9/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import MOLH
import Kingfisher
import KMPopUp
import NVActivityIndicatorView
import CoreData

class MyAppHandller {
    //MARK:- shared instatnce
    static let shared = MyAppHandller()
    var globalController: UIViewController!
    
    //MARK:- Side Menu
    var sideButton: UIBarButtonItem!
    func setSideImage()  {
        let image = UIImage(named: "menu_bar")
        
        sideButton = UIBarButtonItem(image: image,  style: .plain , target: self, action: Selector(("didTapEditButton:")))
    }
    @objc func sideMenus()
    {
        setSideImage()
        if globalController.revealViewController() != nil
        {
            if MOLHLanguage.isRTLLanguage() {
                // right setting
                sideButton.target = globalController.revealViewController()
                sideButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
                globalController.navigationItem.leftBarButtonItem = sideButton
                globalController.revealViewController().rearViewRevealWidth = UIScreen.main.bounds.width - 50
                
            }
            else {
                // left setting
                sideButton.target = globalController.revealViewController()
                sideButton.action = #selector(SWRevealViewController.revealToggle(_:))
                globalController.navigationItem.leftBarButtonItem = sideButton
                globalController.revealViewController().rightViewRevealWidth = UIScreen.main.bounds.width - 50
            }
            globalController.revealViewController().frontViewController.view.addGestureRecognizer(globalController.revealViewController().tapGestureRecognizer())
            globalController.view.addGestureRecognizer(globalController.revealViewController().panGestureRecognizer())
        }
    }
    
    //MARK: - MYAPP Navigation bar items setup from dashboard (BED)
    /// - parameter controller:           Repsented as delegate for currunt VC what we need to setup its navigations icons
    /// - parameter items:                This designed for using for count in cart items count -still not in use-
    /// - parameter headerName:           This paramter contain navigation title
    /// - parameter backBtn:              Boolian param just for setup we have back button or side menu button -back button when true-
    /// - parameter LargTitle:            Boolian param designd for using in large navigation bar style -still not in use-
    /// - parameter isSideButton:         Boolian param just for setup we have back button or side menu button -side menu when true-
    func setupMyAppNavigationBar(controller: UIViewController,
                                 items: String,
                                 headerName:String,
                                 backBtn:Bool = false,
                                 LargTitle: Bool = false,
                                 isSideButton:Bool = true) {
        var BTNsArray = [UIBarButtonItem]()
        let MyAppModel = MyAppHandller.shared.getMyAppSettings()
        let setting = MyAppModel.data!.settings![0]
        print("#Nav Icons ==>>",setting)
        
        if AppDelegate.Static.isThereLangues {
            let newBTN = UIButton(type: UIButton.ButtonType.system)
            newBTN.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            newBTN.addTarget(self, action: #selector(lang), for: .touchUpInside)
            setImage(url: setting.actionbar_language_icon!, UIButton: newBTN, placeHolder: "notif_bar")
            let newBTNItem = UIBarButtonItem(customView: newBTN)
            BTNsArray.append(newBTNItem)
        }
        let F = controller as? FavorateViewController
        if AppDelegate.Static.isThereFavorate && F == nil {
            let newBTN = UIButton(type: UIButton.ButtonType.system)
            newBTN.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            newBTN.addTarget(self, action: #selector(fav), for: .touchUpInside)
            setImage(url: setting.actionbar_fav_icon!, UIButton: newBTN, placeHolder: "heart_bar")
            let newBTNItem = UIBarButtonItem(customView: newBTN)
            BTNsArray.append(newBTNItem)
        }
        let N = controller as? NotificationViewController
        if AppDelegate.Static.isThereNotification && N == nil {
            let newBTN = UIButton(type: UIButton.ButtonType.system)
            newBTN.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            newBTN.addTarget(self, action: #selector(notification), for: .touchUpInside)
            setImage(url: setting.actionbar_notification_icon!, UIButton: newBTN, placeHolder: "bell")
            let newBTNItem = UIBarButtonItem(customView: newBTN)
            BTNsArray.append(newBTNItem)
        }
      
        let CH = controller as? ChatViewController
        if AppDelegate.Static.isThereChat && CH == nil {
            let newBTN = UIButton(type: UIButton.ButtonType.system)
            newBTN.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
             newBTN.addTarget(self, action: #selector(chat), for: .touchUpInside)
            setImage(url: setting.actionbar_chat_icon!, UIButton: newBTN, placeHolder: "speech-bubbles-comment-option")
            let newBTNItem = UIBarButtonItem(customView: newBTN)
            BTNsArray.append(newBTNItem)
        }
        let searchh = controller as? SearchViewController
        if AppDelegate.Static.isThereSearch && searchh == nil  {
            let newBTN = UIButton(type: UIButton.ButtonType.system)
            newBTN.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            newBTN.addTarget(self, action: #selector(search), for: .touchUpInside)
            setImage(url: setting.actionbar_search_icon!, UIButton: newBTN, placeHolder: "magnifier")
            let newBTNItem = UIBarButtonItem(customView: newBTN)
            BTNsArray.append(newBTNItem)
        }
        let C = controller as? CartViewController
        if AppDelegate.Static.isTherecart && C == nil {
            let newBTN = UIButton(type: UIButton.ButtonType.system)
            newBTN.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            setImage(url: setting.actionbar_cart_icon!, UIButton: newBTN, placeHolder: "shopping-cart (2)")
            newBTN.addTarget(self, action: #selector(cart), for: .touchUpInside)
            let newBTNItem = UIBarButtonItem(customView: newBTN)
            let countCart = getCount()
            if countCart != 0 {
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                label.layer.cornerRadius = label.bounds.size.height / 2
                label.textAlignment = .center
                label.layer.masksToBounds = true
                label.font = UIFont.systemFont(ofSize: 12)
                label.textColor = .white
                label.backgroundColor = .red
                label.text = "\(countCart)"
                newBTN.addSubview(label)
            }
            BTNsArray.append(newBTNItem)
        }
        
        globalController = controller
        controller.navigationItem.title = headerName
        let tlabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        tlabel.text = controller.navigationItem.title
        tlabel.textColor = UIColor.white
        //        tlabel.font = UIFont(name: "Helvetica-Bold", size: 30.0)
        tlabel.backgroundColor = UIColor.clear
        tlabel.adjustsFontSizeToFitWidth = true
        tlabel.textAlignment = .center;
        if !LargTitle {
            controller.navigationItem.titleView = tlabel
        }
        
        controller.navigationController?.navigationBar.prefersLargeTitles = LargTitle
        
        //MARK: Navigation Setup
        controller.navigationController?.navigationBar.isTranslucent = false
        controller.navigationController?.navigationBar.barTintColor = UIColor(hexString: "\(SharedData.AppColor)")
        controller.navigationController?.navigationBar.tintColor = UIColor.white
        controller.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        controller.navigationItem.rightBarButtonItems = BTNsArray
        if !backBtn {
            if isSideButton {
                sideMenus()
            }
        }
    }
    
    func getCount() -> Int {
        let fetchRequest  : NSFetchRequest<CartEntityMo> = CartEntityMo.fetchRequest()
        var predicate : NSPredicate!
        predicate = NSPredicate(format: "idForFav == \(false)")
        fetchRequest.predicate = predicate
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequest)
            if test.count > 0 {
                var count = 0
                test.forEach { (row) in
                    print(row.cartQ)
                    count += Int(row.cartQ)
                }
                return count
            }
        }
        catch {
            print(error)
            return 0
        }
        return 0
    }
    
    @objc func fav() {
        globalController.navigationController?.navigate(to: "FavorateViewController")
    }
    @objc func search() {
        globalController.navigationController?.navigate(to: "SearchViewController")
    }
    @objc func cart() {
        globalController.navigationController?.navigate(to: "CartViewController")
    }
    @objc func lang() {
        let alert = UIAlertController(title: "Select Your Language", message: "After selecting your language, the application will close to reset the settings.", preferredStyle: .alert)
        let ar = UIAlertAction(title: "عربي", style: .default) { [weak self] (action) in
            MOLH.setLanguageTo("ar")
                          exit(0)

        }
        let en = UIAlertAction(title: "English", style: .default) { [weak self] (action) in
                   MOLH.setLanguageTo("en")
                                exit(0)

               }
        let cancel = UIAlertAction(title: MOLHLanguage.currentAppleLanguage() == "en" ? "Cancel" : "إلغاء", style: .destructive) { [weak self] (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(ar)
        alert.addAction(en)
        alert.addAction(cancel)
        globalController.present(alert, animated: true, completion: nil)
      
    }
    @objc func notification() {
        globalController.navigationController?.navigate(to: "NotificationViewController")
    }
    @objc func chat() {
        globalController.navigationController?.navigate(to: "ChatViewController")
    }
    
    func setImage(url : String, UIButton: UIButton,placeHolder: String) {
        UIButton.imageView?.contentMode = .scaleAspectFit
        UIButton.imageView!.kf.setImage(with: URL(string: url), placeholder: UIImage(named: placeHolder), options: nil, progressBlock: nil) { (image, error, cashe, url) in
            if error != nil {
                UIButton.setImage(UIImage(named: placeHolder), for: .normal)
            }
        }
    }
    
    //MARK:- setup Setting File for My App
    func CashMyAppSettings(from decoderVar:MyAppSettingResponse)  {
        UserDefaults.standard.set(try? JSONEncoder().encode(decoderVar), forKey:MY_APP_SETTINGS)
    }
    
    func isFirstLaunch() -> Bool {
        if let _ = UserDefaults.standard.value(forKey: "fLaunch") {
            return false
        }
        return true
    }
    
    func appLogin() -> Bool {
        if self.Value(of: LOGIN_STATE) == APP_LOGIN {
            return true
        }
        return false
    }
    
    func setValue(_ Value : String, forKey : String)  {
        UserDefaults.standard.set(Value, forKey: forKey)
        UserDefaults.standard.synchronize()
        
    }
    
    func Value(of userDeafult : String) -> String {
        let y = UserDefaults.standard.object(forKey: userDeafult) as? String ?? "out"
        return y
    }
    
    func getMyAppSettings() -> MyAppSettingResponse {
        var res : MyAppSettingResponse!
        res = MyAppSettingResponse()
        do {
            if let data = UserDefaults.standard.value(forKey:MY_APP_SETTINGS) as? Data {
                res = try JSONDecoder().decode(MyAppSettingResponse.self, from: data)
                
                return res
            }
        } catch {
            print(error.localizedDescription)
        }
        return res
    }
    
    // MARK: return currunt language
    func getMyAppCurruntLang() -> String {
        let language = MOLHLanguage.currentAppleLanguage()
        return language
    }
    
    // Handle Errors if network issue or server
    func handleErrors(from control: UIViewController,error: Error) {
        if let err = error as? URLError, err.code  == URLError.Code.notConnectedToInternet || err.code  == URLError.Code.networkConnectionLost
        {
            // No internet
            print(err.failureURLString!)
            KMPopUp.ShowMessage(control, message: error.localizedDescription, image: "no-wifi")
        }
        else
        {
            // Other errors
            KMPopUp.ShowMessage(control, message: error.localizedDescription, image: "warning")
        }
    }
    
    //Genrate Random Number Code
    func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }
    
    //MARK:- handle indicator
    func IndicatorSetup(indcator: NVActivityIndicatorView) {
        let MyAppModel = MyAppHandller.shared.getMyAppSettings()
        let setting = MyAppModel.data!.settings![0]
        indcator.color = UIColor(hexString: SharedData.AppColor)
        print(setting.loading!)
        if setting.loading == "0"  {
            indcator.type = .ballPulse
        }
        if setting.loading == "1"  {
            indcator.type = .ballGridPulse
        }
        if setting.loading == "2"  {
            indcator.type = .ballClipRotate
        }
        if setting.loading == "3"  {
            indcator.type = .ballClipRotatePulse
        }
        if setting.loading == "4"  {
            indcator.type = .squareSpin
        }
        if setting.loading == "5"  {
            indcator.type = .ballClipRotateMultiple
        }
        if setting.loading == "6"  {
            indcator.type = .ballPulseRise
        }
        if setting.loading == "7"  {
            indcator.type = .ballRotate
        }
        if setting.loading == "8"  {
            indcator.type = .cubeTransition
        }
        if setting.loading == "9"  {
            indcator.type = .ballZigZag
        }
        if setting.loading == "10" {
            indcator.type = .ballZigZagDeflect
        }
        if setting.loading == "11" {
            indcator.type = .ballTrianglePath
        }
        if setting.loading == "12" {
            indcator.type = .ballScale
        }
        if setting.loading == "13" {
            indcator.type = .lineScale
        }
        if setting.loading == "14" {
            indcator.type = .lineScaleParty
        }
        if setting.loading == "15" {
            indcator.type = .ballScaleMultiple
        }
        if setting.loading == "16" {
           indcator.type = .ballPulseSync
        }
        if setting.loading == "17" {
            indcator.type = .ballBeat
        }
        if setting.loading == "18" {
            indcator.type = .lineScalePulseOut
        }
        if setting.loading == "19" {
            indcator.type = .lineScalePulseOutRapid
        }
        if setting.loading == "20" {
            indcator.type = .ballScaleRipple
        }
        if setting.loading == "21" {
            indcator.type = .ballScaleRippleMultiple
        }
        if setting.loading == "22" {
            indcator.type = .ballSpinFadeLoader
        }
        if setting.loading == "23" {
            indcator.type = .lineSpinFadeLoader
        }
        if setting.loading == "24" {
            indcator.type = .triangleSkewSpin
        }
        if setting.loading == "25" {
            indcator.type = .pacman
        }
        if setting.loading == "26" {
            indcator.type = .ballGridBeat
        }
        if setting.loading == "27" {
            indcator.type = .semiCircleSpin
        }
        if setting.loading == "28" {
            indcator.type = .ballRotateChase
        }
        if setting.loading == "29" {
            indcator.type = .orbit
        }
        if setting.loading == "30" {
            indcator.type = .audioEqualizer
        }
        if setting.loading == "31" {
            indcator.type = .circleStrokeSpin
        }
    }
    
    //MARK:- validation
    func isValidAction(textFeilds: [UITextField]) -> Bool {
        
        for myTextFeild in textFeilds {
            if (myTextFeild.text!.isEmpty) {
                return false
            }
        }
        return true
    }
    
    //MARK:- Logout action
    @objc func endProcess()
    {
        DispatchQueue.main.async {
            UserDefaults.standard.setValue(nil, forKey: "DEFemail")
            UserDefaults.standard.set(nil, forKey: "DEFuserid")
            UserDefaults.standard.set(nil, forKey: "DEFname")
            UserDefaults.standard.set(nil, forKey: "DEFuserphone")
            UserDefaults.standard.set(nil, forKey: "DEFuseractive")
            UserDefaults.standard.set(nil, forKey: "DEFPWD")
            UserDefaults.standard.set("out", forKey: "login_State")
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "GIFViewController")
            UIApplication.shared.keyWindow?.rootViewController = vc
        }
    }
    
    
    // MARK:- Navigation method from icon selection
    /// - parameter Type:               this parameter for check what screen we will go
    /// - parameter typeStyle:          this parameter for make a dicesion what style in cells we will use in products and posts
    /// - parameter typeID:             this parameter contain the content id which we will make a request with
    /// - parameter ViewController:     this parameter founded to be a delegate from currunt VC
    func NavigateTo(Type: String,
                    typeStyle: String,
                    typeID: String,
                    ViewController: UIViewController) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        switch Type {
        case ScreenType.allProductsOrEvents.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "AllProductsViewController") as! AllProductsViewController
            vc.type_id = typeID
            vc.type_style = typeStyle
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.teamWork.rawValue :
            let vc = storyBoard.instantiateViewController(withIdentifier: "TeamWorkViewController") as! TeamWorkViewController
            vc.type_id = typeID
            vc.type_style = typeStyle
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.CustomPost.rawValue :
            let vc = storyBoard.instantiateViewController(withIdentifier: "SinglePostViewController") as! SinglePostViewController
            vc.PostID = typeID
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.CustomEvent.rawValue :
            let vc = storyBoard.instantiateViewController(withIdentifier: "SinglePostViewController") as! SinglePostViewController
            vc.PostID = typeID
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.CustomProduct.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
            vc.ProductID = typeID
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.item.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
            vc.ProductID = typeID
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.socialMedia.rawValue:
            print("SocialMedia")
            
            let vc = storyBoard.instantiateViewController(withIdentifier: "SocialMediaViewController") as! SocialMediaViewController
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.Parteners.rawValue :
            let vc = storyBoard.instantiateViewController(withIdentifier: "PartenersViewController") as! PartenersViewController
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.faqs.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.saidAboutUs.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "saidAboutUsViewController") as! saidAboutUsViewController
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.search.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.search.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "FavorateViewController") as! FavorateViewController
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.contactUs.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.cart.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
            ViewController.navigationController?.pushViewController(vc, animated: true)
        case ScreenType.MyOrders.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyOrdersViewController") as! MyOrdersViewController
            ViewController.navigationController?.pushViewController(vc, animated: true)
            
        case ScreenType.CustomForm.rawValue:
            handleFormsIconNavigation(Type: Type,
                                      typeStyle: typeStyle,
                                      typeID: typeID,
                                      ViewController: ViewController)
            
        default:
            print("No Scense for this type \(Type)")
        }
    }
    
    // MARK:- Navigation method from side selection
    /// - parameter Type:               this parameter for check what screen we will go
    /// - parameter typeStyle:          this parameter for make a dicesion what style in cells we will use in products and posts
    /// - parameter typeID:             this parameter contain the content id which we will make a request with
    /// - parameter ViewController:     this parameter founded to be a delegate from sideMenuVC
    func NavigateFromSide(Title: String,
                          Type: String,
                          typeStyle: String,
                          typeID: String,
                          ViewController: SWRevealViewController) {
        let sideMenuAnimation: SWRevealToggleAnimationType = .easeOut
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        switch Type {
        case ScreenType.MainScreen.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVMainScreenViewController") as!  UINavigationController
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc,animated:true)
            
        case ScreenType.allProductsOrEvents.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVAllProductsViewController") as!  UINavigationController
            let single = vc.topViewController as! AllProductsViewController
            single.pageTitle = Title
            single.fromSide = true
            single.type_id = typeID
            single.type_style = typeStyle
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc,animated:true)
            
        case ScreenType.CustomPost.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVSinglePostViewController") as!  UINavigationController
            let single = vc.topViewController as! SinglePostViewController
            single.fromSide = true
            single.PostID = typeID
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc,animated:true)
            
        case ScreenType.CustomEvent.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVSinglePostViewController") as!  UINavigationController
            let single = vc.topViewController as! SinglePostViewController
            single.fromSide = true
            single.PostID = typeID
            ViewController.pushFrontViewController(vc,animated:true)
            
        case ScreenType.CustomProduct.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVSingleProductViewController") as!  UINavigationController
            let single = vc.topViewController as! SingleProductViewController
            single.fromSide = true
            single.ProductID = typeID
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc,animated:true)
            
        case ScreenType.item.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVSingleProductViewController") as!  UINavigationController
            let single = vc.topViewController as! SingleProductViewController
            single.fromSide = true
            single.ProductID = typeID
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc,animated:true)
            
        case ScreenType.teamWork.rawValue :
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVTeamWorkViewController") as!  UINavigationController
            let single = vc.topViewController as! TeamWorkViewController
            single.fromSide = true
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc,animated:true)
            
        case ScreenType.socialMedia.rawValue :
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVSocialMediaViewController") as!  UINavigationController
            let single = vc.topViewController as! SocialMediaViewController
            single.fromSide = true
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc,animated:true)
            
        case ScreenType.faqs.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVFAQViewController") as!  UINavigationController
            let single = vc.topViewController as! FAQViewController
            single.fromSide = true
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc,animated:true)
            
        case ScreenType.Parteners.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVPartenersViewController") as!  UINavigationController
            let single = vc.topViewController as! PartenersViewController
            single.fromSide = true
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc,animated:true)
            
        case ScreenType.saidAboutUs.rawValue :
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVsaidAboutUsViewController") as! UINavigationController
            let single = vc.topViewController as! saidAboutUsViewController
            single.fromSide = true
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc, animated: true)
            
        case ScreenType.search.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVSearchViewController") as! UINavigationController
            let single = vc.topViewController as! SearchViewController
            single.fromSide = true
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc, animated: true)
            
        case ScreenType.contactUs.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVContactUsViewController") as! UINavigationController
            let single = vc.topViewController as! ContactUsViewController
            single.fromSide = true
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc, animated: true)
            
        case ScreenType.Favorite.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVFavorateViewController") as! UINavigationController
            let single = vc.topViewController as! FavorateViewController
            single.fromSide = true
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc, animated: true)
            
        case ScreenType.ShareApp.rawValue:
            let activityShare = UIActivityViewController(activityItems: ["Download for Android \n\n Download for IOS"], applicationActivities: nil)
            activityShare.popoverPresentationController?.sourceView = ViewController.view
            
            ViewController.present(activityShare, animated: true, completion: nil)
            
        case ScreenType.cart.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVCartViewController") as! UINavigationController
            let single = vc.topViewController as! CartViewController
            single.fromSide = true
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc, animated: true)
            
        case ScreenType.login.rawValue:
            if MyAppHandller.shared.appLogin() {
                endProcess()
            } else {
                let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
                vc.modalPresentationStyle = .overFullScreen
                ViewController.present(vc, animated: true, completion: nil)
                let HomeVC = storyBoard.instantiateViewController(withIdentifier: "NAVMainScreenViewController") as!  UINavigationController
                ViewController.toggleAnimationType = sideMenuAnimation
                ViewController.pushFrontViewController(HomeVC,animated:true)
            }
        case ScreenType.profile.rawValue:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NAVProfileViewController") as! UINavigationController
            let single = vc.topViewController as! ProfileViewController
            single.fromSide = true
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc, animated: true)
        case ScreenType.MyOrders.rawValue:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVMyOrdersViewController") as! UINavigationController
            let single = vc.topViewController as! MyOrdersViewController
            single.fromSide = true
            ViewController.toggleAnimationType = sideMenuAnimation
            ViewController.pushFrontViewController(vc, animated: true)
        case ScreenType.CustomForm.rawValue:
            handleFormsSideNavigation(Type: Type,
                                      typeStyle: typeStyle,
                                      typeID: typeID,
                                      ViewController: ViewController)
            
        default:
            print("")
        }
    }
    
    
    // MARK: New form Method
    // use this methods when application contains custom forms
    // jsut we need to know typeID from backend developer (BED)
    func handleFormsSideNavigation(Type: String,
                                   typeStyle: String,
                                   typeID: String,
                                   ViewController: SWRevealViewController) {
//        let sideMenuAnimation: SWRevealToggleAnimationType = .easeOut
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        if typeID == "6" {
//            let vc = storyBoard.instantiateViewController(withIdentifier: "NAVSuggestionViewController") as! UINavigationController
//            let single = vc.topViewController as! suggestionsViewController
//            single.fromSide = true
//            ViewController.toggleAnimationType = sideMenuAnimation
//            ViewController.pushFrontViewController(vc, animated: true)
//        }
    }
    
    func handleFormsIconNavigation(Type: String,
                                   typeStyle: String,
                                   typeID: String,
                                   ViewController: UIViewController) {
//        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//        if typeID == "6" {
//            let vc = storyBoard.instantiateViewController(withIdentifier: "SuggestionViewController") as! suggestionsViewController
//            ViewController.navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    
    
}


// MARK:- scrrens enum
// this enum define the screens types
// every screen has tybe ,we've to know it to create our success navigation
// if you need to add new screen >> just you need to know it's number from backend developer (BED)
// IMPORTANT :- Don't change this value because its static in Database
enum ScreenType: String {
    case CustomEvent = "1"
    case Parteners = "2"
    case teamWork = "3"
    case allProductsOrEvents = "4"
    case CustomPost = "5"
    case CustomProduct = "6"
    case faqs = "7"
    case CustomForm = "8"
    case contactUs = "9"
    case socialMedia = "12"
    case item = "13"
    case search = "15"
    case chat = "16"
    case MyOrders = "17"
    case saidAboutUs = "20"
    case cart = "21"
    case Favorite = "22"
    case MainScreen = "23"
    case ShareApp = "24"
    case login = "100"
    case profile = "201"

}
