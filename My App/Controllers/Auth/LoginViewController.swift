//
//  LoginViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/28/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import KMPopUp
import SwiftyJSON
import MOLH

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var cancelBTN: UIBarButtonItem!
    @IBOutlet weak var loginBTN: UIButton!
    @IBOutlet weak var newAccount: UIButton!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var MailText: UITextField!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var forgetPassword: UIButton!

    var settings : Settings!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        NotificationCenter.default.addObserver(forName: NEW_ACCOUNT, object: nil, queue: nil) { notification in
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        NotificationCenter.default.addObserver(forName: LOGIN_NOTIFCATION, object: nil, queue: nil) { [weak self] notification in
            print("#789")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            if MOLHLanguage.isArabic(){
                let vc = storyBoard.instantiateViewController(withIdentifier: "SWRevealViewController1ar") as!  SWRevealViewController
                vc.modalPresentationStyle = .overFullScreen
                self!.present(vc, animated: true, completion: nil)

            }else{
                let vc = storyBoard.instantiateViewController(withIdentifier: "SWRevealViewController1") as!  SWRevealViewController
                vc.modalPresentationStyle = .overFullScreen
                self!.present(vc, animated: true, completion: nil)


            }
          
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: LOGIN_NOTIFCATION, object: nil)
    }
    
    //MARK:- Helper Methods
    func setupView() {
        settings = MyAppSettings.data!.settings![0]
        navigationController?.navigationBar.barTintColor = UIColor(hexString: SharedData.AppColor)
        navigationController!.navigationBar.isTranslucent = false
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        self.navigationItem.title = "Login".localized
        loginBTN.backgroundColor = UIColor(hexString: SharedData.AppColor)
        newAccount.setTitleColor(UIColor(hexString: SharedData.AppColor), for: .normal)
        forgetPassword.setTitleColor(UIColor(hexString: SharedData.AppColor), for: .normal)
        if settings.register_mobile_active == "0" {
            MailText.placeholder = "Phone / Mail".localized
            MailText.keyboardType = .emailAddress
        } else {
            MailText.placeholder = "Phone Number".localized
            MailText.keyboardType = .phonePad
        }
        if settings.member_app == "1" {
            self.navigationItem.leftBarButtonItem = nil
        }
    }
    
    //MARK:- Actions
    @IBAction func loginButton(_ sender: Any) {
        if MyAppHandller.shared.isValidAction(textFeilds: [MailText,passText]) {
                let param = ["method":"login" ,
                             "mail_phone":"\(MailText.text!)" ,
                    "password":"\(passText.text!)",
                    "google_id":"\(MyAppHandller.shared.Value(of: DEVICE_TOKEN))",
                    "device_id":"\(DEVICE_ID)"]
                login(param, loginType: APP_LOGIN)
        } else {
            KMPopUp.ShowMessage(self, message: COMPLETE_DATA_ERROR, image: "warning")
        }
    }

    @IBAction func CancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func NewAccount(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Requests Online
    func login(_ param: [String:Any],loginType: String) {
        print("mypara",param)
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                let json = JSON(response.value!)
                print(json)
                guard let status = json["state"].string else { return }
                if status == ResponseState.sucess.rawValue {
                    guard let data = json["data"].array else { return }
                    let user = data[0].dictionary!
                    print(user)
                    guard let id = user["id"]?.int else { return }
                    guard let name = user["name"]?.string else { return }
                    guard let email = user["email"]?.string else { return }
                    guard let phone = user["phone"]?.string else { return }
                    guard let active = user["active"]?.int else { return }
                    
                    
                    MyAppHandller.shared.setValue(loginType, forKey: LOGIN_STATE)
                    UserDefaults.standard.setValue("\(id)", forKey: User_id)

                    UserDefaults.standard.setValue(self.passText.text!, forKey: "DEFPWD")
                    UserDefaults.standard.setValue(email, forKey: "DEFemail")
                    UserDefaults.standard.set(name, forKey: "DEFname")
                    
                    UserDefaults.standard.set(active, forKey: "DEFuseractive")
                    UserDefaults.standard.set(phone, forKey: "DEFuserphone")


                    
                    NotificationCenter.default.post(name: LOGIN_NOTIFCATION, object: nil)
                    
                } else if status == ResponseState.failed.rawValue {
                    KMPopUp.ShowMessage(self, message: GENERAL_UNKHOWN_ERROR, image: "warning")
                } else if status == ResponseState.used_mail.rawValue {
                    KMPopUp.ShowMessage(self, message: "this E-mail is already used".localized, image: "warning")
                } else if status == ResponseState.wrong_mail.rawValue {
                    KMPopUp.ShowMessage(self, message: "Phone or e-mail not found".localized, image: "warning")
                }
            } else {
                KMPopUp.ShowMessage(self, message: response.error!.localizedDescription, image: "warning")
            }
            self.indicator.stopAnimating()
        }
    }

}
