//
//  ChangePasswordViewController.swift
//  My App
//
//  Created by iMac on 12/27/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import KMPopUp
import SwiftyJSON

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var loginBTN: UIButton!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var CodeText: UITextField!
    @IBOutlet weak var PasswordText: UITextField!

    var settings : Settings!
    
    var code: Int?
    var mailPhone: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //MARK:- Helper Methods
    func setupView() {
        settings = MyAppSettings.data!.settings![0]
        navigationController?.navigationBar.barTintColor = UIColor(hexString: SharedData.AppColor)
        navigationController!.navigationBar.isTranslucent = false
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        loginBTN.backgroundColor = UIColor(hexString: SharedData.AppColor)
        if settings.member_app == "1" {
            self.navigationItem.leftBarButtonItem = nil
        }
    }
    
    //MARK:- Actions
    @IBAction func loginButton(_ sender: Any) {
        if MyAppHandller.shared.isValidAction(textFeilds: [CodeText,PasswordText]) {
                let param = ["method":"update_password" ,
                             "password":"\(PasswordText.text!)",
                             "mail_phone":"\(mailPhone!)"]
                login(param, loginType: APP_LOGIN)
        } else {
            KMPopUp.ShowMessage(self, message: COMPLETE_DATA_ERROR, image: "warning")
        }
    }
    
    //MARK:- Requests Online
    func login(_ param: [String:Any],loginType: String) {
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                let json = JSON(response.value!)
                print(json)
                guard let status = json["state"].string else { return }
                if status == ResponseState.sucess.rawValue {
                    self.navigationController?.popToRootViewController(animated: true)
                } else if status == ResponseState.failed.rawValue {
                    KMPopUp.ShowMessage(self, message: GENERAL_UNKHOWN_ERROR, image: "warning")
                } else if status == ResponseState.wrong_mail.rawValue {
                    KMPopUp.ShowMessage(self, message: "Phone or e-mail not found".localized, image: "warning")
                }
            } else {
                KMPopUp.ShowMessage(self, message: response.error!.localizedDescription, image: "warning")
            }
            self.indicator.stopAnimating()
        }
    }

}
