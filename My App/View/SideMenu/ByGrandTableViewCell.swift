//
//  ByGrandTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 1/17/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ByGrandTableViewCell: UITableViewCell {
    
    var MyAppModel = MyAppHandller.shared.getMyAppSettings()
    
    @IBOutlet weak var byText: UILabel!
    @IBOutlet weak var GRANDText: UILabel!
    @IBOutlet weak var grandImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupView() {
        guard let settings = MyAppModel.data?.settings?[0] else { return }
        if let menuTextColor = settings.menu_text_color {
            byText.textColor = UIColor(hexString: "#" + menuTextColor)
            GRANDText.textColor = UIColor(hexString: "#" + menuTextColor)
            grandImage.tintColor = UIColor(hexString: "#" + menuTextColor)
        }
    }

}
