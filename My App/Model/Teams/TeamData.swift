
import Foundation
struct TeamData : Codable {
	let id : String?
	let name : String?
	let position : String?
	let details : String?
	let image : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case position = "position"
		case details = "details"
		case image = "image"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		position = try values.decodeIfPresent(String.self, forKey: .position)
		details = try values.decodeIfPresent(String.self, forKey: .details)
		image = try values.decodeIfPresent(String.self, forKey: .image)
	}

}
