//
//  SingleBranchTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 1/10/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import GoogleMaps

class SingleBranchMapTableViewCell: UITableViewCell ,GMSMapViewDelegate{

    @IBOutlet weak var MyMapView: UIView!
    var mapView: GMSMapView!
    var delegate: SingleBranchViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func navigationAction(_ sender: Any) {
        delegate?.NavigateToLocation()
    }
    //MARK:- helperMethods
    func MapSetup(lat: String , long: String) {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!, zoom: 15)
        
        let f: CGRect = MyMapView.frame
        let mapFrame = CGRect(x: f.origin.x, y: f.origin.y, width: UIScreen.main.bounds.width, height: f.size.height)
        mapView = GMSMapView.map(withFrame: mapFrame, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.isMyLocationEnabled = true
        
        MyMapView.addSubview(mapView)
        mapView.delegate = self
        MapMarkerSetup(lat: lat, long: long)
    }
    
    func MapMarkerSetup(lat: String , long: String) {
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!)
                print(lat , " : " , long)
                marker.title = ""
                marker.snippet = ""
                let markerView = UIImageView(image: #imageLiteral(resourceName: "marker123"))
                markerView.tintColor = UIColor(hexString: SharedData.AppColor)
                marker.iconView = markerView
                marker.map = mapView
        
    }
}
