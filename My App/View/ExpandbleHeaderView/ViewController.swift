//
//  ViewController.swift
//  Youya
//
//  Created by apple on 3/4/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UITableViewDelegate ,UITableViewDataSource, ExpabandableHeaderViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var sections = [Section]()
    
//
//    var sections = [Section(genre: "ملابس", category: ["بنطلون","بنطلون","بنطلون","بنطلون","بنطلون"], expanded: false)
//    ,Section(genre: "أحذية", category: ["بنطلون","بنطلون","بنطلون","بنطلون","بنطلون"], expanded: false)
//    ,Section(genre: "جينز", category: ["بنطلون","بنطلون","بنطلون","بنطلون","بنطلون"], expanded: false)
//    ,Section(genre: "اخري", category: ["بنطلون","بنطلون","بنطلون","بنطلون","بنطلون"], expanded: false)]
//
    
    var images = [#imageLiteral(resourceName: "maxresdefault") , #imageLiteral(resourceName: "1") , #imageLiteral(resourceName: "2") , #imageLiteral(resourceName: "maxresdefault")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].category.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if sections[indexPath.section].expanded {
            return 60
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 4
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = ExpabandableHeaderView()
       // header.customInit(image:images[section] , section: section, delegate: self)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "lableCell") as! customTableViewCell
        
        cell.textLable?.text = sections[indexPath.section].category[indexPath.row].name
        
        return cell
    }
    
    func toggleSection(header: ExpabandableHeaderView, section: Int) {
        sections[section].expanded = !sections[section].expanded
        
        tableView.beginUpdates()
        for i in 0..<sections[section].category.count {
            tableView.reloadRows(at: [IndexPath(row: i, section: section )], with: .automatic )
        }
        tableView.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "catViewController") as! catViewController
//        print(sections[indexPath.section].category[indexPath.row])
//        vc.ttt = sections[indexPath.section].category[indexPath.row].name!
//        
//        navigationController?.pushViewController(vc, animated: true)
//        
        
    }
    
}

