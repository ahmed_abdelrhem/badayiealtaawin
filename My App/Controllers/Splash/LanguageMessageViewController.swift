//
//  LanguageMessageViewController.swift
//  My App
//
//  Created by Kirollos Maged on 2/5/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class LanguageMessageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        showAnimate()
    }
    
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    @IBAction func ColseApp(_ sender: Any) {
        exit(0)
    }
}
