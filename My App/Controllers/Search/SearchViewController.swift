//
//  SearchViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/2/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    var spinner = UIActivityIndicatorView()
    var fromSide: Bool = false
    var settings: Settings!
    var itemModelResponse: SearchModelResponse!
    var ItemList = [ItemsModel]()
    var type_style = ""
    var queryWord = ""
    var pagenation = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.shadowImage = UIImage()
        settings = MyAppSettings.data!.settings![0]
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        setupSpinner()
        setupSearchBar()
    }
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "Search".localized, backBtn: !fromSide, LargTitle: false)
        
    }
    
    //MARK:- Helper Methods
    func setupSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.stopAnimating()
        spinner.hidesWhenStopped = true
        spinner.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60)
        spinner.color = UIColor(hexString: SharedData.AppColor)
        tableView.tableFooterView = spinner
    }
    //MARK: search bar setup
    func setupSearchBar() {
        searchbar.backgroundColor = UIColor.white
        searchbar.tintColor = UIColor.white
        searchbar.barStyle = .default
        searchbar.barTintColor = UIColor(hexString: SharedData.AppColor)
        searchbar.placeholder = "Search".localized
        searchbar.showsCancelButton = false
    }
    
    //MARK:- Requests Online
    func getAllProducts(_ param: [String: Any]) {
        print(param)
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.itemModelResponse = nil
                    self.tableView.reloadData()
                    self.itemModelResponse = try JSONDecoder().decode(SearchModelResponse.self, from: response.data!)
                    switch self.itemModelResponse!.state {
                    case ResponseState.sucess.rawValue :
                        self.itemModelResponse.data!.items!.forEach({ (newitem) in
                            self.ItemList.append(newitem)
                        })
                    case ResponseState.empty.rawValue:
                        print("empty data")
                    case ResponseState.failed.rawValue:
                        print("Faild Request")
                    default:
                        print("No State in Reponse")
                    }
                    self.indicator.stopAnimating()
                    self.tableView.reloadData()
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
            self.spinner.stopAnimating()
        }
    }
    
    
    //MARK:- Helper Methods
    //MARK: handle Product table View
    func HandleProductsCell(tableView: UITableView, indexPath: IndexPath) -> CustomProductTableViewCell {
        var ProductCell : CustomProductTableViewCell!
        switch settings.item_style {
        case "1":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! CustomProductTableViewCell)
        case "2":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "galaryCell", for: indexPath) as! CustomProductTableViewCell)
            ProductCell.PriceBackground.backgroundColor = UIColor(hexString: SharedData.AppColor)
            ProductCell.Price.textColor = UIColor.white
            ProductCell.DiscountPrice.textColor = UIColor.white
            ProductCell.DiscountLine.backgroundColor = UIColor.white
        case "3":
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "panoramaCell", for: indexPath) as! CustomProductTableViewCell)
            ProductCell.ProductDescription.text = ItemList[indexPath.row].details!
        default:
            ProductCell = (tableView.dequeueReusableCell(withIdentifier: "panoramaCell", for: indexPath) as! CustomProductTableViewCell)
            ProductCell.ProductDescription.text = ItemList[indexPath.row].details!
        }
        ImageTools.setImage(url: ItemList[indexPath.row].image!,
                            imageView: ProductCell.ProductImage, placeHolder: "myappIcon")
        if ItemList[indexPath.row].price_after_disc != "0.00" {
            ProductCell.DiscountPrice.text = "\(ItemList[indexPath.row].price_after_disc!) \(settings.currency!)"
            ProductCell.DiscountPriceView.isHidden = false
            ProductCell.DiscountLine.isHidden = false
        } else {
            ProductCell.DiscountPriceView.isHidden = true
            ProductCell.DiscountLine.isHidden = true
        }
        if ItemList[indexPath.row].sold == "1" {
            ProductCell.SoldProduct.isHidden = false
        }
        ProductCell.Price.text = "\(ItemList[indexPath.row].price!) \(settings.currency!)"
        ProductCell.ProuductName.text = ItemList[indexPath.row].name!
        ProductCell.ProductID = ItemList[indexPath.row].id!
        if AppDelegate.Static.isThereFavorate {
            ProductCell.favBTN.isHidden = false
            if SharedData.IsFavorate(ID: ItemList[indexPath.row].id!) {
                ProductCell.favBTN.tintColor = UIColor(hexString: SharedData.AppColor)
            } else {
                ProductCell.favBTN.tintColor = UIColor(hexString: "#CCCCCC")
            }
        } else {
            ProductCell.favBTN.isHidden = true
        }
        if AppDelegate.Static.isTherecart {
            if SharedData.IsFavorate(ID: ItemList[indexPath.row].id!,forFav: false) {
                ProductCell.cartBTN.tintColor = UIColor(hexString: SharedData.AppColor)
            } else {
                ProductCell.cartBTN.tintColor = UIColor(hexString: "#CCCCCC")
            }
            ProductCell.cartBTN.isHidden = false
        } else {
            ProductCell.cartBTN.isHidden = true
        }
        return ProductCell
    }
    
    //MARK: handle Post table View
    func HandlePostsCell(tableView: UITableView, indexPath: IndexPath) -> CustomPostTableViewCell {
        var Postcell : CustomPostTableViewCell!
        switch settings.post_style {
        case "1":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventnewsCell", for: indexPath) as! CustomPostTableViewCell)
        case "2":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventgalaryCell", for: indexPath) as! CustomPostTableViewCell)
        case "3":
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventpanoramaCell", for: indexPath) as! CustomPostTableViewCell)
            Postcell.descriptionInPanorama.text = ItemList[indexPath.row].details!
        default:
            Postcell = (tableView.dequeueReusableCell(withIdentifier: "EventpanoramaCell", for: indexPath) as! CustomPostTableViewCell)
            Postcell.descriptionInPanorama.text = ItemList[indexPath.row].details!
        }
        ImageTools.setImage(url: ItemList[indexPath.row].image!,
                            imageView: Postcell.postImage, placeHolder: "myappIcon")
        Postcell.PostTitle.text = ItemList[indexPath.row].name!
        Postcell.postDate.text = ItemList[indexPath.row].created!
        Postcell.PostID = ItemList[indexPath.row].id!
        if AppDelegate.Static.isThereFavorate {
            Postcell.postLike.isHidden = false
            if SharedData.IsFavorate(ID: ItemList[indexPath.row].id!) {
                Postcell.postLike.tintColor = UIColor(hexString: SharedData.AppColor)
            } else {
                Postcell.postLike.tintColor = UIColor(hexString: "#CCCCCC")
            }
        } else {
            Postcell.postLike.isHidden = true
        }
        return Postcell
    }

}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            return
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard searchBar.text != "" else {
            return
        }
        ItemList.removeAll()
        queryWord = searchBar.text!
        let param = ["method":"search" , "query":"\(queryWord)" , "from":"\(pagenation)",
            "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        getAllProducts(param)
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    
}

extension SearchViewController :UITableViewDelegate , UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ItemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ItemList[indexPath.row].type == "1" {
            let cell = HandleProductsCell(tableView: tableView, indexPath: indexPath)
            return cell
        } else {
            let cell = HandlePostsCell(tableView: tableView, indexPath: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ItemList[indexPath.row].type == "1" {
            switch settings.item_style {
            case "1": return 210
            case "2": return 228
            case "3": return 159
            default: return 159
            }
        } else if ItemList[indexPath.row].type == "0" {
            switch settings.post_style {
            case "1": return 200
            case "2": return 228
            case "3": return 144
            default: return 144
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ItemList[indexPath.row].type == "1" {
            tableView.deselectRow(at: indexPath, animated: true)
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
            vc.ProductID = ItemList[indexPath.row].id!
            vc.ProductName = ItemList[indexPath.row].name!
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SinglePostViewController") as! SinglePostViewController
            vc.PostID = ItemList[indexPath.row].id!
            vc.PostName = ItemList[indexPath.row].name!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(
            withDuration: 0.2,
            delay: 0.02 * Double(indexPath.row),
            animations: {
                cell.alpha = 1
        })
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offset = tableView.contentOffset
        let bounds = tableView.bounds
        let size = tableView.contentSize
        let inset = tableView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        let reloadDistance = CGFloat(30.0)
        if y > h + reloadDistance {
            guard let itemsCount = self.itemModelResponse?.data?.items?.count else { return }
            if itemsCount > 19 {
                spinner.startAnimating()
                if queryWord != "" {
                    let param = ["method":"search" , "query":"\(queryWord)" , "from":"\(pagenation + 20)",
                        "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
                    print(param)
                    getAllProducts(param)
                }
            }
        }
    }
}
