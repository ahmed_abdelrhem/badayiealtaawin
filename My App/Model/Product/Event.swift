
import Foundation
struct Event : Codable {
	let id : String?
	let category_id : String?
	let name : String?
	let type : String?
	let details : String?
	let sold : String?
	let kind : String?
	let created : String?
	let image : String?
	let price : String?
	let price_after_disc : String?
	let comments_active : String?
	let likes_num : String?
	let comments_num : String?
	let image_show : String?
	let file_text : String?
	let url_text : String?
	let post_url : String?
	let location_active : String?
	let location_text : String?
	let post_file : String?
	let location_lat : String?
	let location_lng : String?
	let audio : String?
    let youtube : String?
    

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case category_id = "category_id"
		case name = "name"
		case type = "type"
		case details = "details"
		case sold = "sold"
		case kind = "kind"
		case created = "created"
		case image = "image"
		case price = "price"
		case price_after_disc = "price_after_disc"
		case comments_active = "comments_active"
		case likes_num = "likes_num"
		case comments_num = "comments_num"
		case image_show = "image_show"
		case file_text = "file_text"
		case url_text = "url_text"
		case post_url = "post_url"
		case location_active = "location_active"
		case location_text = "location_text"
		case post_file = "post_file"
		case location_lat = "location_lat"
		case location_lng = "location_lng"
		case audio = "audio"
        case youtube = "youtube"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		details = try values.decodeIfPresent(String.self, forKey: .details)
		sold = try values.decodeIfPresent(String.self, forKey: .sold)
		kind = try values.decodeIfPresent(String.self, forKey: .kind)
		created = try values.decodeIfPresent(String.self, forKey: .created)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		price = try values.decodeIfPresent(String.self, forKey: .price)
		price_after_disc = try values.decodeIfPresent(String.self, forKey: .price_after_disc)
		comments_active = try values.decodeIfPresent(String.self, forKey: .comments_active)
		likes_num = try values.decodeIfPresent(String.self, forKey: .likes_num)
		comments_num = try values.decodeIfPresent(String.self, forKey: .comments_num)
		image_show = try values.decodeIfPresent(String.self, forKey: .image_show)
		file_text = try values.decodeIfPresent(String.self, forKey: .file_text)
		url_text = try values.decodeIfPresent(String.self, forKey: .url_text)
		post_url = try values.decodeIfPresent(String.self, forKey: .post_url)
		location_active = try values.decodeIfPresent(String.self, forKey: .location_active)
		location_text = try values.decodeIfPresent(String.self, forKey: .location_text)
		post_file = try values.decodeIfPresent(String.self, forKey: .post_file)
		location_lat = try values.decodeIfPresent(String.self, forKey: .location_lat)
		location_lng = try values.decodeIfPresent(String.self, forKey: .location_lng)
		audio = try values.decodeIfPresent(String.self, forKey: .audio)
        youtube = try values.decodeIfPresent(String.self, forKey: .youtube)
	}

}
