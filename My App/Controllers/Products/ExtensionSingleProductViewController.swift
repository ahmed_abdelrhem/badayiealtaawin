//
//  File.swift
//  My App
//
//  Created by Kirollos Maged on 12/25/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import ImageSlideshow

extension SingleProductViewController : UITableViewDelegate, UITableViewDataSource , UIScrollViewDelegate {
    // Tableview Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = ProductModel?.data {
            return 6
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell", for: indexPath) as! SliderMainScreenTableViewCell
            if sliderList.count > 0 {
                var arr = [KingfisherSource]()
                sliderList.forEach { (icon) in
                    arr.append(KingfisherSource(urlString: icon.image!)!)
                }
                cell.sliderShow.setImageInputs(arr)
            }
            
            return cell
        }
        if indexPath.row == 1 {
            let cell: ProductNameAndCommentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProductNameAndCommentTableViewCell", for: indexPath) as! ProductNameAndCommentTableViewCell
            cell.comments.tintColor = UIColor(hexString: SharedData.AppColor)
            cell.ProductName.text = ProductName
            if ProductModel!.data!.event![0].comments_active == "1" {
                let commentsNumber = ProductModel!.data!.event![0].comments_num!
                var commentName = "\(commentsNumber) \("Comment".localized)"
                if Int(commentsNumber)! > 1 {
                    commentName = "\(commentsNumber) \("Comments".localized)"
                }
                cell.comments.setTitle(commentName, for: .normal)
                cell.comments.isHidden = false
            } else {
                cell.comments.isHidden = true
            }
            return cell
            
        }
        if indexPath.row == 2 {
            let cell : PriceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PriceTableViewCell", for: indexPath) as! PriceTableViewCell
            cell.Price.text = ProductModel!.data!.event![0].price!
            cell.PriceTitleLable.textColor = UIColor(hexString: SharedData.AppColor)
            if ProductModel!.data!.event![0].price_after_disc != "0.00" {
                cell.lineInDiscount.isHidden = false
                cell.discountView.isHidden = false
                cell.dicountLable.text = ProductModel!.data!.event![0].price_after_disc!
            } else {
                cell.lineInDiscount.isHidden = true
                cell.discountView.isHidden = true
            }
            return cell
            
        }
        if indexPath.row == 3 {
            let cell : DetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "DetailsTableViewCell", for: indexPath) as! DetailsTableViewCell
            cell.detailsTitleLable?.textColor = UIColor(hexString: SharedData.AppColor)
            cell.productDetails.text = ProductModel!.data!.event![0].details!
            return cell
        }
        if indexPath.row == 4 {
            let cell: RelatedTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RelatedTableViewCell", for: indexPath) as! RelatedTableViewCell
            cell.relatedTitleLable.textColor = UIColor(hexString: SharedData.AppColor)
            cell.collectionView.reloadData()
            return cell
        }
        if indexPath.row == 5 {
            let cell : AdsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdsTableViewCell", for: indexPath) as! AdsTableViewCell
            
            if let C = banners.data?.banners?.count {
                if C > 0 {
                    let number = Int.random(in: 0 ..< C)
                    RandomAdNumber = number
                    print(banners.data!.banners![number].icon!)
                    ImageTools.setImage(url: banners.data!.banners![RandomAdNumber].icon! , imageView: cell.AdImage, placeHolder: "myappIcon")
                }
            }
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 5 { //Ads
            
            if let N = RandomAdNumber {
                if banners.data!.banners![N].link == "" {
                    MyAppHandller.shared.NavigateTo(Type: banners.data!.banners![N].type!,
                                                    typeStyle: banners.data!.banners![N].style_type!,
                                                    typeID: banners.data!.banners![N].type_id!,
                                                    ViewController: self)
                } else {
                    let url = URL(string: banners.data!.banners![N].link!)
                    if UIApplication.shared.canOpenURL(url!) {
                        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 275
        }
        if indexPath.row == 5 {
            if let C = banners.data?.banners?.count {
                if C > 0 {
                    return 225
                }
            }
            return 0
        }
        return UITableView.automaticDimension
    }
}

extension SingleProductViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = ProductModel?.data?.random?.count {
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : RelatedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RelatedCollectionViewCell", for: indexPath) as! RelatedCollectionViewCell
        cell.relatedProductName.text = ProductModel!.data!.random![indexPath.row].name!
        ImageTools.setImage(url: ProductModel!.data!.random![indexPath.row].image!, imageView: cell.relatedProductImage, placeHolder: "myappIcon")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SingleProductViewController") as! SingleProductViewController
        vc.ProductID = ProductModel!.data!.random![indexPath.row].id!
        vc.ProductName = ProductModel!.data!.random![indexPath.row].name!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
