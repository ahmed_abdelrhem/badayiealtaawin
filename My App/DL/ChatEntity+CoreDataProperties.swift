//
//  ChatEntity+CoreDataProperties.swift
//  
//
//  Created by Kirollos Maged on 2/21/19.
//
//

import Foundation
import CoreData


extension ChatEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ChatEntity> {
        return NSFetchRequest<ChatEntity>(entityName: "ChatEntity")
    }

    @NSManaged public var image: String?
    @NSManaged public var location: String?
    @NSManaged public var message: String?
    @NSManaged public var messageTime: String?
    @NSManaged public var name: String?
    @NSManaged public var profile_image: String?
    @NSManaged public var recieverId: String?
    @NSManaged public var seenStatus: Int64
    @NSManaged public var senderId: String?
    @NSManaged public var voice: String?

}
