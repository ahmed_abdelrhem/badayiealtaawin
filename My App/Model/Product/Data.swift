
import Foundation
struct ProductData : Codable {
	let event : [Event]?
	let random : [Random]?
	let slider : [Slider]?

	enum CodingKeys: String, CodingKey {

		case event = "event"
		case random = "random"
		case slider = "slider"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		event = try values.decodeIfPresent([Event].self, forKey: .event)
		random = try values.decodeIfPresent([Random].self, forKey: .random)
		slider = try values.decodeIfPresent([Slider].self, forKey: .slider)
	}

}
