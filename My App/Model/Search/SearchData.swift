
//
//  SearchData.swift
//  My App
//
//  Created by Kirollos Maged on 1/2/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
struct SearchData : Codable {
    let items : [ItemsModel]?
    
    enum CodingKeys: String, CodingKey {
        case items = "items"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        items = try values.decodeIfPresent([ItemsModel].self, forKey: .items)
    }
    
}
