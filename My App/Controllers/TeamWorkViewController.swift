//
//  TeamWorkViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/27/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class TeamWorkViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    
    var type_style: String = "0" // Screen Style
    var type_id: String = "0" // contecntID
    var settings: Settings!
    var teamsModelResponse: TeamsModelResponse?
    var teamList = [TeamData]()
    var fromSide: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settings = MyAppSettings.data!.settings![0]
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        tableView.alpha = 0
        getTeamData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "Team Work".localized, backBtn: !fromSide, LargTitle: false)
    }
    
    //MARK:- Request Online
    func getTeamData() {
        let param = ["method":"teams" ,
                     "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.teamsModelResponse = try JSONDecoder().decode(TeamsModelResponse.self, from: response.data!)
                    switch self.teamsModelResponse!.state {
                    case ResponseState.sucess.rawValue :
                        self.teamList = self.teamsModelResponse!.data!
                        self.tableView.fadeIn()
                        self.tableView.reloadData()
                    case ResponseState.empty.rawValue:
                        print("empty data")
                    case ResponseState.failed.rawValue:
                        print("Faild Request")
                    default:
                        print("No State in Reponse")
                    }
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
        }
        
    }

}



extension TeamWorkViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath) as! TeamHeaderTableViewCell
        if indexPath.row == 0 {
            ImageTools.setImage(url: settings.team_header!, imageView: cell.HEaderImage, placeHolder: "myappIcon")
            cell.headerLine.backgroundColor = UIColor(hexString: SharedData.AppColor)
        }
        if indexPath.row == 1 {
            cell.TeamShortNote.text = settings.team_description!
        }
        if indexPath.row == 2 {
            cell.collectionView.reloadData()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 178
        }
        if indexPath.row == 1 {
            return tableView.estimatedRowHeight
        }
        if indexPath.row == 2 {
            let width = screenSize.width
            let cellWidth = (width / 2) - 15
            let x : Double = Double(teamList.count) / 2.0
                let isInteger = floor(x) == x
                if isInteger {
                    print(CGFloat((Int(x)) * Int(cellWidth * 1.5)))
                    return CGFloat(((Int(x)) * Int(cellWidth * 1.5)) + 32)
                } else {
                    print(CGFloat((Int(x) + 1) * Int(cellWidth * 1.5)))
                    return CGFloat(((Int(x) + 1) * Int(cellWidth * 1.5)) + 32)
                }
        }
        return 0
    }
    
}

extension TeamWorkViewController: UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return teamList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : TeamMemberCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeamMemberCollectionViewCell", for: indexPath) as! TeamMemberCollectionViewCell
        
        let MemberModel = teamsModelResponse!.data![indexPath.row]
        cell.memberName.text = MemberModel.name!
        cell.memberDetails.text = MemberModel.details!
        ImageTools.setImage(url: MemberModel.image!, imageView: cell.memberImage, placeHolder: "UIHere")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let CellWidth = (screenSize.width / 2) - 15
        return CGSize(width: CellWidth, height: (CellWidth * 1.5))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MemberPopUpViewController") as! MemberPopUpViewController
        popUpVC.name = teamList[indexPath.row].name!
        popUpVC.details = teamList[indexPath.row].details!
        self.addChild(popUpVC)
        self.view.addSubview(popUpVC.view)
        popUpVC.didMove(toParent: self)
    }
}
