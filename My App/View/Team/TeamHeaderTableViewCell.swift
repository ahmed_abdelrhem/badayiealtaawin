//
//  TeamHeaderTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/27/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class TeamHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var HEaderImage: UIImageView!
    @IBOutlet weak var TeamShortNote: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
