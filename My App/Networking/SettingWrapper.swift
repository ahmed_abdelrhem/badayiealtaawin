//
//  SettingRabber.swift
//  My App
//
//  Created by Kirollos Maged on 12/9/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import SwiftyJSON
class SettingWrapper {
    
    class func setupMyAppFromSetting() {
        let myAppSettings = MyAppHandller.shared.getMyAppSettings()
        if let setting = myAppSettings.data?.settings?[0] {
            if let  cart_admin = setting.cart_admin {
                if cart_admin != "1" {
                    AppDelegate.Static.isTherecart = false
                }
            }
            if let carts = setting.carts {
                if carts != "1"{
                    AppDelegate.Static.isTherecart = false
                }
            }
            if let cartICON = setting.cart_icon {
                if cartICON != "1"{
                    AppDelegate.Static.isTherecart = false
                }
            }
            if let  languageAdmin = setting.lang {
                if languageAdmin == "3" {
                    AppDelegate.Static.isThereLangues = true
                    if let d_lang = setting.d_lang {
                        if d_lang == "1" {
                            AppDelegate.Static.isArabic = true
                        }
                    }
                }
                if languageAdmin == "1" {
                    AppDelegate.Static.isThereLangues = false
                    AppDelegate.Static.isArabic = true
                }
                if languageAdmin == "2" {
                    AppDelegate.Static.isThereLangues = false
                    AppDelegate.Static.isArabic = false
                }
            }
            if let notificationAdmin = setting.notification_icon {
                if notificationAdmin != "1" {
                    AppDelegate.Static.isThereNotification = false
                }
            }
            if let favorateAdmin = setting.fav_admin {
                if favorateAdmin != "1" {
                    AppDelegate.Static.isThereFavorate = false
                }
            }
            if let favorateIcon = setting.fav_icon {
                if favorateIcon != "1" {
                    AppDelegate.Static.isThereFavorate = false
                }
            }
            if let searchIcon = setting.search_icon {
                if searchIcon != "1" {
                    AppDelegate.Static.isThereSearch = false
                }
            }
            if let menuSparator = setting.menu_line_check {
                if menuSparator != "1" {
                    AppDelegate.Static.isThereSparatorInSideMenu = false
                }
            }
            var chatIcon: String {
                if let C = setting.chat_icon {
                    return C
                }
                return "0"
            }
            var chat: String {
                if let C = setting.chat {
                    return C
                }
                return "0"
            }
            var chatAdmin: String {
                if let C = setting.chat_admin {
                    return C
                }
                return "0"
            }
            if chatIcon == "1" && chat == "1" && chatAdmin == "1" {
                AppDelegate.Static.isThereChat = true
            } else {
                AppDelegate.Static.isThereChat = false
            }
            
        }
    }
}
