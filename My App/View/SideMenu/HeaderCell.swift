//
//  HeaderCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/13/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellText: UILabel!
    @IBOutlet weak var arrowBTN: UIButton!
    
    
    var MyAppModel = MyAppHandller.shared.getMyAppSettings()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCellView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImage(url : String) {
        DispatchQueue.global().async {
            if url != "" {
                let Myurl = URL(string: url)!
                DispatchQueue.main.async {
                    if UIApplication.shared.canOpenURL(Myurl) {
                        self.cellImage.kf.setImage(with: URL(string: url))
                    }
                }
            }
        }
    }
    
    func setupCellView() {
        guard let settings = MyAppModel.data?.settings?[0] else { return }
        if let arrowC = settings.arrow_color {
            arrowBTN.tintColor = UIColor(hexString: "#" + arrowC)
        }
        if let menuTextColor = settings.menu_text_color {
            cellText.textColor = UIColor(hexString: "#" + menuTextColor)
        }
    }
    
    func haveArrow(MainCell:Bool) {
        arrowBTN.isHidden = !MainCell
    }
    
}
