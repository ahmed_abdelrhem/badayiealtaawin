//
//  ChatPopupViewController.swift
//  My App
//
//  Created by Kirollos Maged on 3/25/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ChatPopupViewController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    

    @IBOutlet weak var femaleView: UIView!
    @IBOutlet weak var maleView: UIView!
    var gender: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    }
    @IBAction func onTapMen(_ sender: UIButton) {
        toggleGender(sender.tag)
    }
    
    @IBAction func onTapWomen(_ sender: UIButton) {
        toggleGender(sender.tag)
    }
    
    @IBAction func startChatAction(_ sender: Any) {
        if userName.hasText && gender != nil {
            UserDefaults.standard.set("1", forKey: "FTChat")
            removeAnimation()
        }
    }
    
    
    func toggleGender(_ senderTag: Int) {
        if senderTag == 1 {
            gender = "temp_boy.png"
            maleView.backgroundColor = UIColor(hexString: SharedData.AppColor)
            femaleView.backgroundColor = UIColor.white
        }
        if senderTag == 2 {
            gender = "temp_girl.png"
            femaleView.backgroundColor = UIColor(hexString: SharedData.AppColor)
            maleView.backgroundColor = UIColor.white
        }
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }
    
}
