//
//  SingleBranchViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/10/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import KMPopUp
import GoogleMaps
import MapKit

class SingleBranchViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var branchData : BransheData!
    var locManager = CLLocationManager()
    var lat: CLLocationDegrees!
    var long: CLLocationDegrees!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "", backBtn: false, LargTitle: false, isSideButton: false)
    }
    
    
    //MARK:- Actions
    @IBAction func DONEBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func whatsAppCall(sender: UIButton) {
        var phoneNum = ""
        if sender.tag == 0 {
            phoneNum = branchData.phone1!
        }
        else if sender.tag == 2 {
            phoneNum = branchData.phone2!
        }
        let msg = "Hello :"
        let urlWhats = "whatsapp://send?phone=\(phoneNum)&text=\(msg)"
        print(urlWhats)
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                }
            }
        }
    }
    
    func phoneCall(sender: UIButton) {
        var phoneNum = ""
        if sender.tag == 1 {
            phoneNum = branchData.phone1!
        } else if sender.tag == 3 {
            phoneNum = branchData.phone2!
        }
        let url:NSURL = NSURL(string: "tel://\(phoneNum)")!
        if UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    func NavigateToLocation() {
        
        if CLLocationManager.locationServicesEnabled(){
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways{
                locManager.startUpdatingLocation()
                lat = locManager.location!.coordinate.latitude
                long = locManager.location!.coordinate.longitude
                NavigateToGoogleMap(toLatitude: branchData.location_lat!, toLongitude: branchData.location_lng!)
            } else {
                locManager.requestWhenInUseAuthorization()
                KMPopUp.ShowMessage(self, message: "Please enable maps from settings".localized, image:"warning")
            }
            
        }
        
        
    }
    
    func NavigateToGoogleMap(toLatitude: String, toLongitude: String) {
        // if GoogleMap installed
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?saddr=\(self.lat!),\(self.long!)&daddr=\(branchData.location_lat!),\(branchData.location_lng!)&directionsmode=driving")! as URL)
            
        } else {
            // if GoogleMap App is not installed
            UIApplication.shared.openURL(NSURL(string:
                "https://www.google.co.in/maps/dir/?saddr=\(self.lat!),\(self.long!)&daddr=\(branchData.location_lat!),\(branchData.location_lng!)&directionsmode=driving")! as URL)
        }
    }
    
    //MARK:- helper Methods
    func mapSetup() {
        locManager = CLLocationManager()
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways{
                locManager.startUpdatingLocation()
                lat = locManager.location!.coordinate.latitude
                long = locManager.location!.coordinate.longitude
            } else {
                locManager.requestWhenInUseAuthorization()
            }
            
        }
    }
    
    
}


extension SingleBranchViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell!
        switch indexPath.row {
        case 0:
            let MapCell : SingleBranchMapTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath) as! SingleBranchMapTableViewCell
            MapCell.delegate = self
            MapCell.MapSetup(lat: branchData.location_lat!, long: branchData.location_lng!)
            return MapCell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath) as! AllSingleBranchTableViewCell
            cell.delegate = self
            cell.branceName.text = "BName".localized + branchData.title_en!
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath) as! AllSingleBranchTableViewCell
            cell.delegate = self
            cell.branchAddress.text = "BAddress".localized + branchData.address_en!
            
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath) as! AllSingleBranchTableViewCell
            cell.delegate = self
            if branchData.phone1_whats == "0" {
                cell.whatsApp1.isHidden = true
            } else {
                cell.whatsApp1.isHidden = false
            }
            cell.BranchPhone.text = "BPhone".localized + branchData.phone1!
            cell.phone1.tintColor = UIColor(hexString: SharedData.AppColor)
            cell.whatsApp1.tintColor = UIColor(hexString: SharedData.AppColor)
            
            
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath) as! AllSingleBranchTableViewCell
            cell.delegate = self
            if branchData.phone2_whats == "0" {
                cell.whatsApp2.isHidden = true
            } else {
                cell.whatsApp2.isHidden = false
            }
            cell.BranchMobile.text = "BMobile".localized + branchData.phone2!
            cell.phone2.tintColor = UIColor(hexString: SharedData.AppColor)
            cell.whatsApp2.tintColor = UIColor(hexString: SharedData.AppColor)
            
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath) as! AllSingleBranchTableViewCell
            cell.delegate = self
            cell.BranchStart.text =  "BStart at".localized + branchData.from_working!
            
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath) as! AllSingleBranchTableViewCell
            cell.delegate = self
            cell.BranchEnd.text =  "BEnd at".localized + branchData.to_working!
            
            return cell
        default:
            print("")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 250
        }
        if indexPath.row == 4 {
            if branchData.phone2 == "" {
                return 0
            }
        }
        return 56
    }
    
}
