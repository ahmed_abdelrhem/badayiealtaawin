//
//  NotificationViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/15/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class NotificationViewController: UIViewController {

    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    var notification: NotificationModelResponse?
    var NotList = [NotificationData]()
    var pagnation = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        getNotification()
    }
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "Notifications".localized, backBtn: true)
    }
    
    //MARK:- Requests Online
    func getNotification() {
        let param = [
            "method":"notifications" ,
            "from":"\(pagnation)" ,
            "send_to":"2",
            "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"
        ]
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.notification = nil
                    self.tableView.reloadData()
                    self.notification = try JSONDecoder().decode(NotificationModelResponse.self, from: response.data!)
                    switch self.notification!.state {
                    case ResponseState.sucess.rawValue :
                        self.notification!.data!.forEach({ (newitem) in
                            self.NotList.append(newitem)
                        })
                    case ResponseState.empty.rawValue:
                        print("empty data")
                    case ResponseState.failed.rawValue:
                        print("Faild Request")
                    default:
                        print("No State in Reponse")
                    }
                    self.indicator.stopAnimating()
                    self.tableView.reloadData()
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
        }
    }

}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = notification?.data?.count {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NotificationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NotificationTableViewCell
        cell.selectionStyle = .none
        let NotData = notification?.data![indexPath.row]
        cell.NotTitle.text = NotData?.title!
        cell.NotDes.text = NotData?.details!
        cell.NotDate.text = NotData?.created!
        
        ImageTools.setImage(url: (NotData?.image!)!, imageView: cell.NotImage, placeHolder: "myappIcon")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SingleNotificationViewController") as! SingleNotificationViewController
        vc.notificationData = NotList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
