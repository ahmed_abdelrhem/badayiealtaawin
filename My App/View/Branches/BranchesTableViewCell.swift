//
//  BranchesTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 1/10/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class BranchesTableViewCell: UITableViewCell {

    @IBOutlet weak var branchName: UILabel!
    @IBOutlet weak var branchAddress: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
