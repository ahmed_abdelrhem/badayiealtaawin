//
//  FAQData.swift
//  My App
//
//  Created by Kirollos Maged on 1/1/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
struct FAQData : Codable {
    let id : Int?
    let question : String?
    let answer : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case question = "question"
        case answer = "answer"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        question = try values.decodeIfPresent(String.self, forKey: .question)
        answer = try values.decodeIfPresent(String.self, forKey: .answer)
    }
    
}

