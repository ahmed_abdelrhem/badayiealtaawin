
import Foundation
struct ItemsModel : Codable {
	let id : String?
	let category_id : String?
	let name : String?
	let type : String?
	let details : String?
	let sold : String?
	let kind : String?
	let created : String?
	let image : String?
	let price : String?
	let price_after_disc : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case category_id = "category_id"
		case name = "name"
		case type = "type"
		case details = "details"
		case sold = "sold"
		case kind = "kind"
		case created = "created"
		case image = "image"
		case price = "price"
		case price_after_disc = "price_after_disc"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		details = try values.decodeIfPresent(String.self, forKey: .details)
		sold = try values.decodeIfPresent(String.self, forKey: .sold)
		kind = try values.decodeIfPresent(String.self, forKey: .kind)
		created = try values.decodeIfPresent(String.self, forKey: .created)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		price = try values.decodeIfPresent(String.self, forKey: .price)
		price_after_disc = try values.decodeIfPresent(String.self, forKey: .price_after_disc)
	}

}
