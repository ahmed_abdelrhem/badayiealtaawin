//
//  OrderModelResponse.swift
//  My App
//
//  Created by apple on 12/28/20.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation

struct OrderModelResponse : Codable {
    
    let state : String?
    let data : [OrderData]?

    enum CodingKeys: String, CodingKey {
        case state = "state"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        data = try values.decodeIfPresent([OrderData].self, forKey: .data)
    }

}
