//
//  File.swift
//  Youya
//
//  Created by apple on 3/4/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation

struct Section {
    var genre: String!
    var category: [Menu]!
    //var index: Int!
    var expanded : Bool!
    var id : String?
    var name : String?
    var type : String?
    var type_id : String?
    var icon : String?
    var style_type : String?
    var menu_id : String?
    
    init(genre: String,category: [Menu],expanded : Bool,id : String,name : String,type : String,type_id : String,icon : String,style_type : String,menu_id : String) {
        self.genre = genre
        self.category = category
        self.expanded = expanded
        self.id = id
        self.name = name
        self.type_id = type_id
        self.type = type
        self.style_type = style_type
        self.icon = icon
        self.menu_id = menu_id
    }
}
