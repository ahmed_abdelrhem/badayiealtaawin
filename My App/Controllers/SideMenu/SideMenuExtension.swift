//
//  SideMenuExtension.swift
//  My App
//
//  Created by Kirollos Maged on 12/16/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import ExpyTableView



extension SideMenuViewController {
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SideMenuTableViewCell.self)) as! SideMenuTableViewCell
            //Make your customizations here.
            cell.cellText.text = List[section].name!
        var offlineUrl: Bool = false
        if List[section].id == "-1" {
            offlineUrl = true
        } else {
            offlineUrl = false
        }
        cell.setImage(url: List[section].icon!, offlineUrl: offlineUrl)
            if List[section].category.count > 0 {
                cell.haveArrow(MainCell: true)
            } else {
                cell.haveArrow(MainCell: false)
        }
            return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
}

extension SideMenuViewController {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        switch state {
        case .willExpand:
            print("WILL EXPAND")
        case .willCollapse:
            print("WILL COLLAPSE")
        case .didExpand:
            print("DID EXPAND")
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        print("list",List)
        return List.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(List[section].category.count)
        print("list section",List[section])

        return List[section].category.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SideMenuTableViewCell2.self)) as! SideMenuTableViewCell2
        
        cell.cellText.text = List[indexPath.section].category[indexPath.row - 1].name!
        if List[indexPath.section].category[indexPath.row - 1].id == "-1" {    // check if loading image cell from assets or from online url
            cell.setImage(url: List[indexPath.section].category[indexPath.row - 1].icon!, offlineUrl: true)
        } else {
            cell.setImage(url: List[indexPath.section].category[indexPath.row - 1].icon!, offlineUrl: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)","Type: \(List[indexPath.section].type!)")
        if indexPath.row == 0 {
            print("Row")
            print("-type>>",List[indexPath.section].type!,"-style>>",List[indexPath.section].style_type!,"-id>>",List[indexPath.section].type_id!)
            
            let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
            MyAppHandller.shared.NavigateFromSide(Title: List[indexPath.section].name!, Type: List[indexPath.section].type!, typeStyle: List[indexPath.section].style_type!, typeID:
                List[indexPath.section].type_id!, ViewController: rvc)
        } else {
            print("InRow")
            let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
            print(List[indexPath.section].category[indexPath.row - 1].type!)
            print(List[indexPath.section].category[indexPath.row - 1].style_type!)
            print(List[indexPath.section].category[indexPath.row - 1].type_id!)
            MyAppHandller.shared.NavigateFromSide(Title: List[indexPath.section].category[indexPath.row - 1].name!, Type: List[indexPath.section].category[indexPath.row - 1].type!, typeStyle: List[indexPath.section].category[indexPath.row - 1].style_type!, typeID:
                List[indexPath.section].category[indexPath.row - 1].type_id!, ViewController: rvc)
        }
    }
}


