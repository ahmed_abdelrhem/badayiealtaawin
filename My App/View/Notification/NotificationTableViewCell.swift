//
//  NotificationTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 1/15/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var NotImage: UIImageView!
    @IBOutlet weak var NotTitle: UILabel!
    @IBOutlet weak var NotDes: UILabel!
    @IBOutlet weak var NotDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
