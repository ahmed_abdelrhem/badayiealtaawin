//
//  exViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/13/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import ExpyTableView

class exViewController: UIViewController {
    
    @IBOutlet weak var expandableTableView: ExpyTableView!

    var MyAppModel = MyAppHandller.shared.getMyAppSettings()
    var menuList = [Menu]()
    var List = [Section]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        expandableTableView.dataSource = self
        expandableTableView.delegate = self
        
        expandableTableView.rowHeight = UITableView.automaticDimension
        expandableTableView.estimatedRowHeight = 44
        
        //Alter the animations as you want
        expandableTableView.expandingAnimation = .fade
        expandableTableView.collapsingAnimation = .fade
        // Do any additional setup after loading the view.
        menuList = MyAppModel.data!.menu!
        MappingMenuListToSections()
    
        
    }
    
    func MappingMenuListToSections() {
        for index in 0..<menuList.count {
            if menuList[index].menu_id == "0" {
                let filtered = menuList.filter({return $0.menu_id == menuList[index].id!})
                self.List.append(Section(genre: menuList[index].name!,
                                         category: filtered,
                                         expanded: false,
                                         id: menuList[index].id!,
                                         name: menuList[index].name!,
                                         type: menuList[index].type!,
                                         type_id: menuList[index].type_id!,
                                         icon: menuList[index].icon!,
                                         style_type: menuList[index].style_type!,
                                         menu_id: menuList[index].menu_id!))
            }
           
        }
        
        print(List.count)
        self.List.forEach { (item) in
            //                    print(item)
            print("cat count :" + "\(item.category.count)")
        }
    }
    

}

extension exViewController: ExpyTableViewDelegate ,ExpyTableViewDataSource{
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SideMenuTableViewCell.self)) as! SideMenuTableViewCell
        //Make your customizations here.
        cell.cellText.text = List[section].name! //"Section: \(section) Row: 0"
        if List[section].category.count > 0 {
            cell.haveArrow(MainCell: true)
        }
        cell.showSeparator()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
}

extension exViewController {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            
        case .willCollapse:
            print("WILL COLLAPSE")
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        print(List.count)
        return List.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(List[section].category.count)
        return List[section].category.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SideMenuTableViewCell2.self)) as! SideMenuTableViewCell2

        print(List[indexPath.section].id)
        print(List[indexPath.section].category[indexPath.row - 1].name)
        
        
        cell.cellText.text = List[indexPath.section].category[indexPath.row - 1].name! /// "KK Section: \(indexPath.section) Row: \(indexPath.row)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        //This solution obviously has side effects, you can implement your own solution from the given link.
        //This is not a bug of ExpyTableView hence, I think, you should solve it with the proper way for your implementation.
        //If you have a generic solution for this, please submit a pull request or open an issue.
        
        print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")
    }
    
}

