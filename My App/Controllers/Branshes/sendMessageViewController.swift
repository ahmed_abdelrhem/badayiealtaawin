//
//  sendMessageViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/8/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import KMPopUp
import NVActivityIndicatorView
import SwiftyJSON

class sendMessageViewController: UIViewController, IndicatorInfoProvider {

    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var mail: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var message: DesignabtextView!
    @IBOutlet weak var sendBTN: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        setupView()
    }
    
    func setupView() {
        sendBTN.backgroundColor = UIColor(hexString: SharedData.AppColor)
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Send Message".localized)
    }

    @IBAction func sendAction(_ sender: Any) {
        if MyAppHandller.shared.isValidAction(textFeilds: [name,mail,phoneNumber]) {
            sendMessage()
        } else {
            KMPopUp.ShowMessage(self, message: "", image: "warning", withAlpha: 0.7)
        }
    }
    
    //MARK:- request Online
    func sendMessage() {
        indicator.startAnimating()
        let param = ["method":"send_message" ,
                     "email":"\(mail.text!)" ,
                     "message":"\(message.text!)" ,
                     "subject":"\(name.text!)" ,
                     "phone":"\(phoneNumber.text!)"]
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                let json = JSON(response.value!)
                if let state = json["state"].string {
                    switch state {
                    case ResponseState.sucess.rawValue:
                        self.name.text = ""
                        self.mail.text = ""
                        self.phoneNumber.text = ""
                        self.message.text = ""
                        KMPopUp.ShowMessage(self, message: "Message sent successfully".localized, image: "like", withAlpha: 0.7)
                    case ResponseState.failed.rawValue:
                        KMPopUp.ShowMessage(self, message: GENERAL_UNKHOWN_ERROR, image: "warning", withAlpha: 0.7)
                    default:
                        KMPopUp.ShowMessage(self, message: GENERAL_UNKHOWN_ERROR, image: "warning", withAlpha: 0.7)
                    }
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
        }
    }
}
