

import Foundation
struct TeamsModelResponse : Codable {
	let state : String?
	let data : [TeamData]?

	enum CodingKeys: String, CodingKey {

		case state = "state"
		case data = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		state = try values.decodeIfPresent(String.self, forKey: .state)
		data = try values.decodeIfPresent([TeamData].self, forKey: .data)
	}

}
