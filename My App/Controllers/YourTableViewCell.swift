//
//  YourTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 12/13/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import ExpyTableView

class YourTableViewCell: UITableViewCell, ExpyTableViewHeaderCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            
        case .willCollapse:
            print("WILL COLLAPSE")
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }


}
