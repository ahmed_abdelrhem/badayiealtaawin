//
//  FAQViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/1/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class FAQViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    var FAQModel : FAQModelResponse?
    var fromSide: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        getTeamData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "FAQ".localized, backBtn: !fromSide, LargTitle: false)
    }
    
    //MARK:- Online Requests
    func getTeamData() {
        let param = ["method":"faqs" ,
                     "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.FAQModel = try JSONDecoder().decode(FAQModelResponse.self, from: response.data!)
                    switch self.FAQModel!.state {
                    case ResponseState.sucess.rawValue :
                        self.tableView.fadeIn()
                        self.tableView.reloadData()
                    case ResponseState.empty.rawValue:
                        print("empty data")
                    case ResponseState.failed.rawValue:
                        print("Faild Request")
                    default:
                        print("No State in Reponse")
                    }
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
        }
    }
}


extension FAQViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = FAQModel?.data?.count {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : FAQTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FAQTableViewCell
        
        cell.selectionStyle = .none
        cell.QView.backgroundColor = UIColor(hexString: SharedData.AppColor)
        cell.QLable.text = FAQModel!.data![indexPath.row].question!
        cell.ALable.text = FAQModel!.data![indexPath.row].answer!
        
        return cell
    }
}
