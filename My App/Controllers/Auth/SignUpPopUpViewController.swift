//
//  SignUpPopUpViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/27/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class SignUpPopUpViewController: UIViewController {
    
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var NewAccountBTN: UIButton!
    @IBOutlet weak var haveAccountBTN: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        setupView()
        showAnimate()
        self.view.addTapGesture(tapNumber: 1, target: self, action: #selector(TabAction))
        // Do any additional setup after loading the view.
    }
    
    func setupView() {
        BGView.backgroundColor = UIColor(hexString: SharedData.AppColor)
        NewAccountBTN.setTitleColor(UIColor(hexString: SharedData.AppColor), for: .normal)
        haveAccountBTN.setTitleColor(UIColor(hexString: SharedData.AppColor), for: .normal)
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }
    
    @objc func TabAction() {
        removeAnimation()
    }
    
    @IBAction func loginButton(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        removeAnimation()
    }
    @IBAction func newAccount(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        perform(#selector(createObserver), with: nil, afterDelay: 0.01)
        removeAnimation()
    }
    
    @objc func createObserver() {
        NotificationCenter.default.post(name: NEW_ACCOUNT, object: nil)
    }


}
