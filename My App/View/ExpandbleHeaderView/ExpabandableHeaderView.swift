//
//  ExpabandableHeaderView.swift
//  Youya
//
//  Created by apple on 3/4/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

protocol ExpabandableHeaderViewDelegate  {
    func toggleSection(header: ExpabandableHeaderView, section: Int)
}

class ExpabandableHeaderView: UITableViewHeaderFooterView {
    
    var delegate: ExpabandableHeaderViewDelegate?
    var section: Int!
    var image =  UIImageView()
    var sectionModel: Section!
    var MyAppModel = MyAppHandller.shared.getMyAppSettings()
    var bv: UIView!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectHeaderAction)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func selectHeaderAction(gestureRecognizer: UITapGestureRecognizer)  {
        let cell = gestureRecognizer.view as! ExpabandableHeaderView
        delegate?.toggleSection(header: self, section: cell.section)
        self.image.image = ImageTools.imageRotatedByDegrees(oldImage: image.image!, deg: 180)
    }
    
    func customInit(sectionModel:Section ,title: String, section : Int, delegate: ExpabandableHeaderViewDelegate) {
        self.textLabel?.text = title
        self.sectionModel = sectionModel
        self.section = section
        self.delegate = delegate
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let settings = MyAppModel.data?.settings?[0] else { return }
        let menuBgColor = settings.menu_bgcolor!
//        addHeader()
        image.image = UIImage(named: "down-arrow")
        image.frame = CGRect(x: screenSize.width - 150, y: 25, width: 20, height: 20)
        self.addSubview(image)
        
        if let arrowC = settings.arrow_color {
            image.tintColor = UIColor(hexString: "#" + arrowC)
        }
        self.textLabel?.textAlignment = .left
        if let menuTextColor = settings.menu_text_color {
            self.textLabel?.textColor = UIColor(hexString: "#" + menuTextColor)
        }
        self.contentView.backgroundColor = UIColor.init(hexString: "#" + menuBgColor)
    }
    
    
    
    
    func addHeader() {
        let width = screenSize.width
        let mybv = SideCell(frame: CGRect(x: 0, y: 0, width: 600, height: 64))
        mybv.frame = CGRect(x:0, y:-2, width:width, height:66)
        mybv.layer.borderColor = UIColor.clear.cgColor
        print("")
        mybv.cellText.text = "sdf" //sectionModel.name!
//        mybv.setImage(url: sectionModel.icon!)
        bv = UIView()
        bv.addSubview(mybv)
        bv.clipsToBounds = true
        self.addSubview(bv)
    }
    
}
