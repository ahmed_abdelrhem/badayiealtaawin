
import Foundation
struct CatRandom : Codable {
	let name : String?
	let category_id : String?
	let style_type : String?
	let type : String?
	let image : String?
    

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case category_id = "category_id"
		case style_type = "style_type"
		case type = "type"
		case image = "image"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
		style_type = try values.decodeIfPresent(String.self, forKey: .style_type)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		image = try values.decodeIfPresent(String.self, forKey: .image)
	}

}
