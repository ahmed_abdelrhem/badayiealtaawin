
import Foundation
struct ItemData : Codable {
	let random : [CatRandom]?
	let items : [ItemsModel]?

	enum CodingKeys: String, CodingKey {

		case random = "random"
		case items = "items"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		random = try values.decodeIfPresent([CatRandom].self, forKey: .random)
		items = try values.decodeIfPresent([ItemsModel].self, forKey: .items)
	}

}
