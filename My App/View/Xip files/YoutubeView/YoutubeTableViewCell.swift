//
//  YoutubeTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 1/15/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import WebKit

class YoutubeTableViewCell: UITableViewCell {

    @IBOutlet weak var webView: WKWebView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func PlayYoutubeVideo(videoCode: String) {
        webView.load(URLRequest(url: URL(string: "\(URLHandller.youtubeBaseUrl)\(videoCode)")!))
    }
    
}
