//
//  SharedData.swift
//  My App
//
//  Created by Kirollos Maged on 12/6/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import CoreData

class SharedData {
    //save the whole object in NSUserDefakuts
    //params : an object that inherents from NSOject and a string key
    //return void
    class func setObjectData(anyObject anyObj:Codable , key:String){
        let userDefaults = UserDefaults.standard
        userDefaults.set(NSKeyedArchiver.archivedData(withRootObject: anyObj), forKey: key)
        userDefaults.synchronize()
    }
    
    //retreive an object that already saved in NSUserDefaults
    //params: a key as string value that belong to the object
    // return an NSObject that will be cast to the class needed
    class func getObjectData(key:String) -> Codable {
        var retreivedObj: Codable!
        if let data = UserDefaults.standard.object(forKey: key) as? Data {
            retreivedObj = (NSKeyedUnarchiver.unarchiveObject(with: data) as! Codable)
        }
        return retreivedObj
    }
    
    //remove an object that saved in NSUserDefaults
    //params: a string key that belong to the object
    //return void
    class func removeUserData(key:String){
        UserDefaults.standard.removeObject(forKey: key)
    }
    
    
    static var AppColor: String {
        let setting = MyAppHandller.shared.getMyAppSettings()
        let appC = setting.data!.settings![0].app_color!
        print("#" + appC)
        return "#" + appC
    }
    
    static var IsActiveUser: Bool {
        let Active_User = MyAppHandller.shared.Value(of: "DEFuseractive")
        let setting = MyAppSettings.data!.settings![0]
        if setting.member_admin == "1" {
            if Active_User == "0" {
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    static var userId: String {
        return MyAppHandller.shared.Value(of: "DEFuserid")
    }
    
    static func IsFavorate(ID: String, forFav: Bool = true) -> Bool {
        let fetchRequest  : NSFetchRequest<FavEntityMo> = FavEntityMo.fetchRequest()
        let predicateID = NSPredicate(format: "id == %@", ID)
        let predicateFav = NSPredicate(format: "idForFav == \(forFav)")
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicateID,predicateFav])
        print(predicateCompound)
        fetchRequest.predicate = predicateCompound
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequest)
            if test.count > 0
            {
                return true
            } else {
                return false
            }
        }
        catch {
            print(error)
        }
        return false
    }
    
    static func IsCart(ID: String, forFav: Bool = true) -> Bool {
        let fetchRequest  : NSFetchRequest<CartEntityMo> = CartEntityMo.fetchRequest()
        let predicateID = NSPredicate(format: "id == %@", ID)
        let predicateFav = NSPredicate(format: "idForFav == \(forFav)")
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicateID,predicateFav])
        print(predicateCompound)
        fetchRequest.predicate = predicateCompound
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequest)
            if test.count > 0
            {
                return true
            } else {
                return false
            }
        }
        catch {
            print(error)
        }
        return false
    }
    
}
