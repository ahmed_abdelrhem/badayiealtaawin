//
//  BransheData.swift
//  My App
//
//  Created by Kirollos Maged on 1/8/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation

import Foundation
struct BransheData : Codable {
    let id : String?
    let title_en : String?
    let address_en : String?
    let phone1 : String?
    let phone2 : String?
    let location_lat : String?
    let location_lng : String?
    let email : String?
    let fax : String?
    let from_working : String?
    let to_working : String?
    let phone1_whats : String?
    let phone2_whats : String?
    let created : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case title_en = "title_en"
        case address_en = "address_en"
        case phone1 = "phone1"
        case phone2 = "phone2"
        case location_lat = "location_lat"
        case location_lng = "location_lng"
        case email = "email"
        case fax = "fax"
        case from_working = "from_working"
        case to_working = "to_working"
        case phone1_whats = "phone1_whats"
        case phone2_whats = "phone2_whats"
        case created = "created"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        title_en = try values.decodeIfPresent(String.self, forKey: .title_en)
        address_en = try values.decodeIfPresent(String.self, forKey: .address_en)
        phone1 = try values.decodeIfPresent(String.self, forKey: .phone1)
        phone2 = try values.decodeIfPresent(String.self, forKey: .phone2)
        location_lat = try values.decodeIfPresent(String.self, forKey: .location_lat)
        location_lng = try values.decodeIfPresent(String.self, forKey: .location_lng)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        fax = try values.decodeIfPresent(String.self, forKey: .fax)
        from_working = try values.decodeIfPresent(String.self, forKey: .from_working)
        to_working = try values.decodeIfPresent(String.self, forKey: .to_working)
        phone1_whats = try values.decodeIfPresent(String.self, forKey: .phone1_whats)
        phone2_whats = try values.decodeIfPresent(String.self, forKey: .phone2_whats)
        created = try values.decodeIfPresent(String.self, forKey: .created)
    }
    
}
