//
//  CartViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/22/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import CoreData
import NVActivityIndicatorView
import MOLH
import KMPopUp

class CartViewController: UIViewController {

    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var totalText: UILabel!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var BuyBTN: UIButton!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    let fetchRequestCart  : NSFetchRequest<CartEntityMo> = CartEntityMo.fetchRequest()
    var settings: Settings!
    var itemModelResponse: ItemModelResponse!
    var ItemList = [ItemsModel]()
    var QList = [CartEntityMo]()
    var fromSide :Bool = false
    var totalPrice = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settings = MyAppSettings.data!.settings![0]
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        setupView()
        let param = ["method":"baskets",
                     "basket":"\(getIDs())",
            "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        getAllCart(param)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "", backBtn: !fromSide, LargTitle: false)
    }
    
    func setupView() {
        NotificationCenter.default.addObserver(forName: END_PURCHASE, object: nil, queue: nil) { notification in
            let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NAVMyOrdersViewController") as! UINavigationController
            let single = vc.topViewController as! MyOrdersViewController
            single.fromSide = true
            rvc.toggleAnimationType = .easeOut
            rvc.pushFrontViewController(vc, animated: true)
        }
        totalView.backgroundColor = UIColor(hexString: SharedData.AppColor)
        BuyBTN.backgroundColor = UIColor(hexString: SharedData.AppColor)
    }
    
    func getIDs() -> String {
        var predicate : NSPredicate!
        predicate = NSPredicate(format: "idForFav == \(false)")
        fetchRequestCart.predicate = predicate
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequestCart)
            if test.count > 0
            {
                print(test.count)
                self.QList = test
                var stringIDs = ""
                test.forEach { (row) in
                    print(row.id!)
                    stringIDs.append("\(row.id!),")
                }
                return stringIDs
            }
        }
        catch {
            print(error)
            return ""
        }
        return ""
    }
    
    func getCount() -> String {
        var predicate : NSPredicate!
        predicate = NSPredicate(format: "idForFav == \(false)")
        fetchRequestCart.predicate = predicate
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequestCart)
            if test.count > 0
            {
                print(test.count)
                self.QList = test
                var stringIDs = ""
                test.forEach { (row) in
                    print(row.cartQ)
                    stringIDs.append("\(row.cartQ),")
                }
                return stringIDs
            }
        }
        catch {
            print(error)
            return ""
        }
        return ""
    }
    
    //MARK:- Requests Online
    func getAllCart(_ param: [String: Any]) {
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.itemModelResponse = try JSONDecoder().decode(ItemModelResponse.self, from: response.data!)
                    self.ItemList = self.itemModelResponse.data!.items!
                    self.getTotalPrice()
                    self.tableView.reloadData()
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
        }
    }
    
    
    //MARK:- Helper Methods
    func getTotalPrice() {
        var totalSum = 0.0
        var totalCount = 0
        for item in ItemList {
            for q in QList {
                if q.id == item.id {
                    totalCount += Int(q.cartQ)
                    if item.price_after_disc != "0.00"{
                        totalSum += Double(item.price_after_disc!)! * Double(q.cartQ)
                    }else {
                        totalSum += Double(item.price!)! * Double(q.cartQ)
                    }
                }
            }
        }
        countLbl.text = "(\(totalCount) \("Product".localized))"
        totalText.text = String(totalSum) + " \(self.settings.currency!)"
        totalPrice = totalSum
    }
    
    func GenerateParam() -> [String:Any] {
        let totalSection = tableView.numberOfSections
        var comments = ""
        for section in 0..<totalSection
        {
            let totalRows = tableView.numberOfRows(inSection: section)
            for row in 0..<totalRows
            {
                let cell = tableView.cellForRow(at: IndexPath(row: row, section: section))
                if let comment = cell?.viewWithTag(4) as? UITextField {
                    comments.append(comment.text! + ",")
                }
            }
        }
        let param = ["method":"buy_now",
        "items": "\(getIDs())",
        "count":"\(getCount())",
        "comments":"\(comments)",
        "total_price":"\(totalPrice)",
        "user_id":"\(SharedData.userId)",
        "last_notes":"notesnotes",
        "status":"2"]
        
        return param
    }
    
    //MARK:- Actions
    @IBAction func BuyNow(_ sender: Any) {
        if MyAppHandller.shared.appLogin() {
            if SharedData.IsActiveUser {
                if ItemList.count > 0 {
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddressViewController") as! AddressViewController
                    vc.delegate = self
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overFullScreen
                    present(vc, animated: true, completion: nil)
                }
            } else {
                KMPopUp.ShowMessage(self, message: "Please contact Application admins to active your account".localized, image: "warning", withAlpha: 0.7)
            }
            
        } else {
            let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpPopUpViewController") as! SignUpPopUpViewController
            self.addChild(popUpVC)
            self.view.addSubview(popUpVC.view)
            popUpVC.didMove(toParent: self)
        }
    }
    
    func ActionInCoreData(_ sender: UIStepper,cell: CartTableViewCell) {
        let indexPath = tableView.indexPath(for: cell)!
        let predicateID = NSPredicate(format: "id == %@", ItemList[indexPath.row].id!)
        let predicateFav = NSPredicate(format: "idForFav == \(false)")
        let predicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [predicateID,predicateFav])
        print(predicateCompound)
        fetchRequestCart.predicate = predicateCompound
        do
        {
            let test = try PresistanceServce.context.fetch(fetchRequestCart)
            if test.count > 0
            {
                if sender.value < 1 {
                    let alert = UIAlertController(title: "Delete".localized, message: "Do you want to delete this item from cart?".localized, preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: "Yes".localized, style: .destructive, handler: {(action:UIAlertAction!) in
                        PresistanceServce.context.delete(test[0])
                        do {
                            try PresistanceServce.context.save()
                            if sender.value < 1 {
                                let param = ["method":"baskets",
                                             "basket":"\(self.getIDs())",
                                    "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
                                self.getAllCart(param)
                            }
                        } catch {
                            // Do something... fatalerror
                        }
                    })
                    let noAction = UIAlertAction(title: "No".localized, style: .cancel, handler: nil)
                    alert.addAction(yesAction)
                    alert.addAction(noAction)
                    present(alert, animated: true, completion: nil)
                } else {
                    let objectUpdate = test[0] as NSManagedObject
                    objectUpdate.setValue(Int16(sender.value), forKey: "cartQ")
                    do {
                        try PresistanceServce.context.save()
                        self.getTotalPrice()
                        self.tableView.reloadData()
                    } catch {
                        // Do something... fatalerror
                    }
                }
            }
        }
        catch
        {
            print(error)
        }
    }
    

}

extension CartViewController: AddressViewControllerDelegate {
    
    func saveAction(comment: String?, address: String?) {
        let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EndPurchasesPopUpViewController") as! EndPurchasesPopUpViewController
        popUpVC.totalValue = totalText.text!
        popUpVC.param = GenerateParam()
        self.addChild(popUpVC)
        self.view.addSubview(popUpVC.view)
        popUpVC.didMove(toParent: self)
    }
    
}

extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ItemList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CartTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartTableViewCell
        cell.delegate = self
        cell.selectionStyle = .none
        cell.productName.text = ItemList[indexPath.row].name!
        if ItemList[indexPath.row].price_after_disc != "0.00" {
            cell.productPrice.text = ItemList[indexPath.row].price_after_disc
            cell.totalPrice.text = String(Double(ItemList[indexPath.row].price_after_disc!)! * 1.0) + " \(settings.currency!)"
            for q in QList {
                if q.id == ItemList[indexPath.row].id {
                    cell.stepper.value = Double(q.cartQ)
                    cell.Quantity.text = String(q.cartQ)
                    cell.totalPrice.text = String(Double(ItemList[indexPath.row].price_after_disc!)! * Double(q.cartQ)) + " \(settings.currency!)"
                }
            }
        }else {
            cell.productPrice.text = ItemList[indexPath.row].price
            cell.totalPrice.text = String(Double(ItemList[indexPath.row].price!)! * 1.0) + " \(settings.currency!)"
            for q in QList {
                if q.id == ItemList[indexPath.row].id {
                    cell.stepper.value = Double(q.cartQ)
                    cell.Quantity.text = String(q.cartQ)
                    cell.totalPrice.text = String(Double(ItemList[indexPath.row].price!)! * Double(q.cartQ)) + " \(settings.currency!)"
                }
            }
        }
        ImageTools.setImage(url: ItemList[indexPath.row].image!, imageView: cell.productImage, placeHolder: "myappIcon")
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 265
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(ItemList[indexPath.row].id!)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(
            withDuration: 0.2,
            delay: 0.02 * Double(indexPath.row),
            animations: {
                cell.alpha = 1
        })
    }

}
