//
//  OrderDetailsViewController.swift
//  My App
//
//  Created by apple on 4/8/21.
//  Copyright © 2021 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class OrderDetailsViewController: UIViewController {

    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var totalText: UILabel!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var settings: Settings!
    var itemModelResponse: ItemModelResponse!
    var ItemList = [ItemsModel]()
    var fromSide :Bool = false
    var items = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settings = MyAppSettings.data!.settings![0]
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        setupView()
        let param = ["method":"baskets",
                     "basket":items,
            "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        getAllCart(param)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "", backBtn: !fromSide, LargTitle: false)
    }
    
    func setupView() {
        totalView.backgroundColor = UIColor(hexString: SharedData.AppColor)
    }
    
    //MARK:- Requests Online
    func getAllCart(_ param: [String: Any]) {
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.itemModelResponse = try JSONDecoder().decode(ItemModelResponse.self, from: response.data!)
                    self.ItemList = self.itemModelResponse.data!.items!
//                    self.getTotalPrice()
                    self.tableView.reloadData()
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
        }
    }
        
}

extension OrderDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ItemList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CartTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartTableViewCell
        cell.selectionStyle = .none
        cell.productName.text = ItemList[indexPath.row].name!
        if ItemList[indexPath.row].price_after_disc != "0.00" {
            cell.productPrice.text = ItemList[indexPath.row].price_after_disc
            cell.totalPrice.text = String(Double(ItemList[indexPath.row].price_after_disc!)! * 1.0) + " \(settings.currency!)"
        }else {
            cell.productPrice.text = ItemList[indexPath.row].price
            cell.totalPrice.text = String(Double(ItemList[indexPath.row].price!)! * 1.0) + " \(settings.currency!)"
        }
        ImageTools.setImage(url: ItemList[indexPath.row].image!, imageView: cell.productImage, placeHolder: "myappIcon")
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 265
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(ItemList[indexPath.row].id!)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(
            withDuration: 0.2,
            delay: 0.02 * Double(indexPath.row),
            animations: {
                cell.alpha = 1
        })
    }

}
