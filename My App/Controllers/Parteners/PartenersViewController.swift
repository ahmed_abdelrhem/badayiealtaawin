//
//  PartenersViewController.swift
//  My App
//
//  Created by Kirollos Maged on 12/30/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class PartenersViewController: UIViewController {
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var type_style: String = "0" // Screen Style
    var type_id: String = "0" // contecntID
    var settings: Settings!
    var PartenerModel: PartenersModelResponse?
    var PartenersList = [PartenerData]()
    var fromSide: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        settings = MyAppSettings.data!.settings![0]
        MyAppHandller.shared.IndicatorSetup(indcator: indicator)
        tableView.alpha = 0
        getTeamData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "Parteners".localized, backBtn: !fromSide, LargTitle: false)
    }
    
    //MARK:- Request Online
    func getTeamData() {
        let param = ["method":"partners" ,
                     "language":"\(MyAppHandller.shared.getMyAppCurruntLang())"]
        indicator.startAnimating()
        APIManager.sharedInstance.postReques(Parameters: param) { (response) in
            if response.error == nil {
                do {
                    self.PartenerModel = try JSONDecoder().decode(PartenersModelResponse.self, from: response.data!)
                    switch self.PartenerModel!.state {
                    case ResponseState.sucess.rawValue :
                        self.PartenersList = self.PartenerModel!.data!
                        self.tableView.fadeIn()
                        self.tableView.reloadData()
                    case ResponseState.empty.rawValue:
                        print("empty data")
                    case ResponseState.failed.rawValue:
                        print("Faild Request")
                    default:
                        print("No State in Reponse")
                    }
                } catch {
                    MyAppHandller.shared.handleErrors(from: self, error: error)
                }
            } else {
                MyAppHandller.shared.handleErrors(from: self, error: response.error!)
            }
            self.indicator.stopAnimating()
        }
        
    }

}


extension PartenersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PartenerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath) as! PartenerTableViewCell
        if indexPath.row == 0 {
            ImageTools.setImage(url: settings.partner_header!, imageView: cell.partenerHeader, placeHolder: "myappIcon")
        }
        if indexPath.row == 1 {
            cell.PartenerDetails.text = settings.partners_description
        }
        if indexPath.row == 2 {
            cell.collectionView.reloadData()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 180.0
        }
        else if indexPath.row == 1 {
            return tableView.estimatedRowHeight
        }
        else if indexPath.row == 2 {
            let width = screenSize.width
            let cellWidth = (width / 2) - 15
            let x : Double = Double(PartenersList.count) / 2.0
            let isInteger = floor(x) == x
            if isInteger {
                print(CGFloat((Int(x)) * Int(cellWidth * 1.25)))
                return CGFloat(((Int(x)) * Int(cellWidth * 1.25)) + 32)
            } else {
                print(CGFloat((Int(x) + 1) * Int(cellWidth * 1.25)))
                return CGFloat(((Int(x) + 1) * Int(cellWidth * 1.25)) + 32)
            }
        }
        return 0
    }
}

extension PartenersViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PartenersList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PartenerCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PartenerCollectionViewCell", for: indexPath) as! PartenerCollectionViewCell
        
        cell.partName.text = PartenersList[indexPath.row].name!
        ImageTools.setImage(url: PartenersList[indexPath.row].logo!, imageView: cell.partImage, placeHolder: "UIHere")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let CellWidth = (screenSize.width / 2) - 30
        return CGSize(width: CellWidth, height: (CellWidth * 1.25))
    }
}
