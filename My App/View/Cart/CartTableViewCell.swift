//
//  CartTableViewCell.swift
//  My App
//
//  Created by Kirollos Maged on 1/23/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var Quantity: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    
    var delegate: CartViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func stepperAction(_ sender: UIStepper) {
//        Quantity.text = String(Int(sender.value))
        delegate?.ActionInCoreData(sender, cell: self)
    }
}
