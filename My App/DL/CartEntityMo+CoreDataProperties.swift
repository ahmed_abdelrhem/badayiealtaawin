//
//  CartEntityMo+CoreDataProperties.swift
//  My App
//
//  Created by apple on 3/7/21.
//  Copyright © 2021 apple. All rights reserved.
//

import Foundation
import CoreData


extension CartEntityMo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CartEntityMo> {
        return NSFetchRequest<CartEntityMo>(entityName: "CartEntity")
    }

    @NSManaged public var id: String?
    @NSManaged public var idForFav: Bool
    @NSManaged public var cartQ: Int16

}
