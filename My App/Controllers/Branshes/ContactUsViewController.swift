//
//  ContactUsViewController.swift
//  My App
//
//  Created by Kirollos Maged on 1/8/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ContactUsViewController: ButtonBarPagerTabStripViewController {
    
    var fromSide:Bool = false

    override func viewDidLoad() {
        XLPagerSetup()
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MyAppHandller.shared.setupMyAppNavigationBar(controller: self, items: "", headerName: "Contact Us".localized, backBtn: !fromSide, LargTitle: false)
    }
    
    func XLPagerSetup() {
        
        settings.style.buttonBarBackgroundColor = UIColor(hexString: SharedData.AppColor)
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor = UIColor(hexString: SharedData.AppColor)
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 16)
        settings.style.selectedBarHeight = 2
        settings.style.buttonBarMinimumLineSpacing = 0
//        settings.style.buttonBarItemTitleColor = UIColor.brown
//        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = {  (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor(hexString: SharedData.AppColor) //UIColor.white
            oldCell?.backgroundColor = UIColor.white //UIColor(hexString: SharedData.AppColor)
            newCell?.label.textColor = UIColor(hexString: SharedData.AppColor)
            newCell?.backgroundColor = UIColor.white
            oldCell?.layer.cornerRadius = 0
            newCell?.layer.cornerRadius = 0
            
            // oldCell?.imageView.isHighlighted = false
            // newCell?.imageView.isHighlighted = true
        }
    }
    
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BranshesViewController" )
        let child_2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sendMessageViewController" )
        
        return [child_1, child_2]
    }

}
