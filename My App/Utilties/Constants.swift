//
//  Constants.swift
//  My App
//
//  Created by Kirollos Maged on 12/6/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation

let MyAppSettings = MyAppHandller.shared.getMyAppSettings()
let screenSize: CGRect = UIScreen.main.bounds
let MY_APP_SETTINGS = "_*my_app_settings_"
let APP_COLOR = "_*app_color_"
let ICON_STYLE = "_*icon_style_"
let LOGIN_STATE = "login_State"
let APP_LOGIN = "AppLogin"
let DEVICE_TOKEN = "DeviceToken"
let User_id = "DEFuserid"


let DEVICE_ID = UIDevice.current.identifierForVendor!.uuidString

let END_PURCHASE = NSNotification.Name("End_purchase")
let LOGIN_NOTIFCATION  = NSNotification.Name("login_notification")
let NEW_ACCOUNT = NSNotification.Name("newAccount")
let MESSAGE_NOTIFY = NSNotification.Name("messageReloader")

let SHARE_APP_MESSAGE: String = "SHARE_APP_MESSAGE".localized
let PHONE_ERROR_MESSAGE: String =  "PHONE_ERROR_MESSAGE".localized
let MAIL_ERROR_MESSAGE: String = "MAIL_ERROR_MESSAGE".localized
let PASSWORD_ERROR_MESSAGE : String = "PASSWORD_ERROR_MESSAGE".localized
let PASSWORD_COUNT_ERROR_MESSAGE : String = "password must be more than 5 digits or characters".localized

let LOCATION_MESSAGE: String = "LOCATION_MESSAGE".localized
let COMPLETE_DATA_ERROR: String = "COMPLETE_DATA_ERROR".localized
let LOGIN_ERROR_MESSAGE: String = "LOGIN_ERROR_MESSAGE".localized
let GENERAL_UNKHOWN_ERROR : String = "GENERAL_UNKHOWN_ERROR".localized

enum ResponseState :String {
    case sucess = "101"
    case empty = "102"
    case wrong_mail = "103"
    case failed = "104"
    case used_mail = "105"
    case wrong_password = "106"
    case rent_before = "107"
    case deleted = "108"
}
